package org.jsi.elmis.model.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionHistoryDTO {
	private Integer ordinalNumber;
	private Integer txnId;
	private Integer productId;
	private Integer nodeId;
	private BigDecimal signedTxnQuantity;
	private Date txnTimestamp;
	private BigDecimal qtyBeforeTxn;
	private BigDecimal qtyAfterTxn;
}
