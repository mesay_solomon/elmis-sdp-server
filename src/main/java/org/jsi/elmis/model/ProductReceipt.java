package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductReceipt {
    private Integer id;

    private Integer transactionId;

    private String dispatchNumber;

    private Integer sourceId;
}