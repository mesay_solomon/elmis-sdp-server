package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EquipmentStatus {
    private Integer id;

    private Integer periodId;

    private Boolean functioning;

    private Equipment equipment;
    
    private Integer equipmentId;

    private Integer daysOut;

}