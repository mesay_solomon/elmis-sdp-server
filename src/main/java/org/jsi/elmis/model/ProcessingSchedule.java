package org.jsi.elmis.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessingSchedule {
    
    public static final String MONTHLY = "MONTHLY";
    public static final String WEEKLY = "WEEKLY";
    
	private Integer id;

    private String code;

    private String name;

    private String description;

    private Integer createdby;

    private Date createddate;

    private Integer modifiedby;

    private Date modifieddate;
    
}