package org.jsi.elmis.model;

import java.math.BigDecimal;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class FacilityType {
    private Integer id;

    private String code;

    private String name;

    private String description;

    @SerializedName("levelId")
    private Integer levelid;

    @SerializedName("nominalMaxMonth")
    private Integer nominalmaxmonth;

    @SerializedName("nominalEop")
    private BigDecimal nominaleop;

    @SerializedName("displayOrder")
    private Integer displayorder;

    private Boolean active;

    private Date createddate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getLevelid() {
        return levelid;
    }

    public void setLevelid(Integer levelid) {
        this.levelid = levelid;
    }

    public Integer getNominalmaxmonth() {
        return nominalmaxmonth;
    }

    public void setNominalmaxmonth(Integer nominalmaxmonth) {
        this.nominalmaxmonth = nominalmaxmonth;
    }

    public BigDecimal getNominaleop() {
        return nominaleop;
    }

    public void setNominaleop(BigDecimal nominaleop) {
        this.nominaleop = nominaleop;
    }

    public Integer getDisplayorder() {
        return displayorder;
    }

    public void setDisplayorder(Integer displayorder) {
        this.displayorder = displayorder;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
}