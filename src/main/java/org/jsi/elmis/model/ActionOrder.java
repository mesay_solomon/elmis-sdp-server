package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActionOrder {
	
	public static final String BEFORE = "BEFORE";
	public static final String AFTER = "AFTER";
	
	private Integer neignboringOrder;
	private String precedence;
}
