package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HIVTestProduct {
    private Integer productId;

    private Integer id;

    private String testType;

}