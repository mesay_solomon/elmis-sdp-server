package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReversalTransaction {
    private Integer id;

    private Integer reversedTransactionId;

    private Integer reversalTransactionId;
    
}