package org.jsi.elmis.model;

public class NodeProgram {
    private Integer id;

    private Integer nodeId;

    private Integer programId;

    private Boolean dispensingPoint;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public Boolean getDispensingPoint() {
        return dispensingPoint;
    }

    public void setDispensingPoint(Boolean dispensingPoint) {
        this.dispensingPoint = dispensingPoint;
    }
}