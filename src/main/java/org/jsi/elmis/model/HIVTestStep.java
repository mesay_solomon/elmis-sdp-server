package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HIVTestStep {
    private Integer id;

    private Integer transactionId;

    private String testType;

    private Integer hivTestId;

    private String testResult;

}