package org.jsi.elmis.model;

public class PhysicalCountAdjustment {
    private Integer id;

    private Integer physicalCountId;

    private Integer adjustmentId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPhysicalCountId() {
        return physicalCountId;
    }

    public void setPhysicalCountId(Integer physicalCountId) {
        this.physicalCountId = physicalCountId;
    }

    public Integer getAdjustmentId() {
        return adjustmentId;
    }

    public void setAdjustmentId(Integer adjustmentId) {
        this.adjustmentId = adjustmentId;
    }
}