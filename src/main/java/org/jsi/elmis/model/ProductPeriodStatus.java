package org.jsi.elmis.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPeriodStatus {
    private Integer id;

    private Integer periodId;

    private Integer productId;

    private BigDecimal beginningBalance;

    private BigDecimal quantityReceived;

    private BigDecimal quantityDispensed;

    private BigDecimal stockOnHand;

    private BigDecimal latestPhysicalCount;

    private Integer stockOutDays;

}