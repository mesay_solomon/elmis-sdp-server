package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportAndRequisition {
	
	public static final String SUBMITTED = "SUBMITTED";
	public static final String SKIPPED = "SKIPPED";
	
    private Integer id;

    private Integer periodId;

    private String status;

    private String remark;

    private Integer programId;
}