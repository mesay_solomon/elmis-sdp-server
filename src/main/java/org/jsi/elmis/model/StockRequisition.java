package org.jsi.elmis.model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockRequisition {
    private Integer id;

    private Integer requesterId;

    private Integer supplierId;

    private Date time;

    private String status;

    private Integer userId;
    
    private List<StockRequistionItem> stockRequisitionItems; 

}