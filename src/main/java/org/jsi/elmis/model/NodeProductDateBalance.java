package org.jsi.elmis.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NodeProductDateBalance {
	private BigDecimal balance;
	private BigDecimal usableBalance;
}
