package org.jsi.elmis.model;

import java.time.LocalDate;
import java.util.Date;


public class ProductSource {
	
	private String sourcename;
	private Date createddate;
	private String createdby ;
	private  Integer facilityid ;
	
	private Integer id ;
	
	public String getSourcename() {
		return sourcename;
	}
	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	
	public Integer getFacilityid() {
		return facilityid;
	}
	public void setFacilityid(Integer facilityid) {
		this.facilityid = facilityid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
    
}