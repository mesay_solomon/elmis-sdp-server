package org.jsi.elmis.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockRequistionItem {
    private Integer id;

    private Integer productId;

    private BigDecimal quantityRequested;

    private Integer stockRequisitionId;

   
}