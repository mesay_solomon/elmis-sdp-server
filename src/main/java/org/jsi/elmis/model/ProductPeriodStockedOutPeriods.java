package org.jsi.elmis.model;

import java.util.Date;

public class ProductPeriodStockedOutPeriods {
    private Integer id;

    private Integer productPeriodId;

    private Date startDate;

    private Date endDate;

    private Integer numberOfDays;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductPeriodId() {
        return productPeriodId;
    }

    public void setProductPeriodId(Integer productPeriodId) {
        this.productPeriodId = productPeriodId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
}