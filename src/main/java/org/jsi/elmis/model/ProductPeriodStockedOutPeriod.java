package org.jsi.elmis.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPeriodStockedOutPeriod {
    private Integer id;

    private Integer productPeriodId;

    private Date startDate;

    private Date endDate;

    private Integer numberOfDays;
}