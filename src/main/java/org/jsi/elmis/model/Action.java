package org.jsi.elmis.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Action {
	//TODO : should be inherited by transactions and physical counts
    private Integer id;

    private String type;

    private Date timestamp;

    private Integer ordinalNumber;

    private Integer userId;
}