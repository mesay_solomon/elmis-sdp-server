package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockRequisitionTransaction {
    private Integer id;

    private Integer stockRequisitionId;

    private Integer transactionId;

}