package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HIVTest {
    private Integer id;

    private Integer nodeId;

    private String finalResult;

    private String clientNumber;

    private String purpose;

}