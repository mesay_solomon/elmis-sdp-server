package org.jsi.elmis.model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionHistoryItem {

	private Date date;
	private String description;
	private List<Transaction> transactions;
	
}
