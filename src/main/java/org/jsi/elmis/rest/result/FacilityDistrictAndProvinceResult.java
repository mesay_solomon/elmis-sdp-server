package org.jsi.elmis.rest.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacilityDistrictAndProvinceResult {
	private String facilityName;
	private String districtName;
	private String provinceName;
}
