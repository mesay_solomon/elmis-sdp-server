package org.jsi.elmis.rest.result;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.jsi.elmis.model.Node;
import org.jsi.elmis.notification.ELMISNotificationType;
/**
 * 
 * @author Mekbib
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class StockRequisitionResult extends ELMISNotificationType {
	private Integer requestId;
	private Node requesterNode;
	private List<FacilityApprovedProductResult> requestedProducts;	
	private Date time;
}
