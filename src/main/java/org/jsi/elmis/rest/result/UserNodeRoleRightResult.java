package org.jsi.elmis.rest.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author Mekbib
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserNodeRoleRightResult {
	private Integer id;
	private Integer nodeId;
	private Integer roleId;
	private Integer userId;
	private String firstName;
	private String lastName;
	private String rightName;
}
