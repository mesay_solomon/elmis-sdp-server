package org.jsi.elmis.rest.result;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.model.TransactionHistoryItem;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReversibleTransactionsResult {
	
	List<TransactionHistoryItem> reversibleTxns;
	
	Integer noOfPages;

}
