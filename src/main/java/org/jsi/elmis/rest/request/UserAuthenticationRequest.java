package org.jsi.elmis.rest.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.model.User;
/**
 * 
 * @author Mekbib
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthenticationRequest {
	
	private User user;
	private Integer nodeId;

}
