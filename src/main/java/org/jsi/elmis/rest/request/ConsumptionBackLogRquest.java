package org.jsi.elmis.rest.request;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.TransactionProduct;

@Data
@AllArgsConstructor 
@NoArgsConstructor
public class ConsumptionBackLogRquest {
	Node node;
	List<TransactionProduct> txnProducts;
	String txnType;
	Date txnDate;
	Integer userId;
}
