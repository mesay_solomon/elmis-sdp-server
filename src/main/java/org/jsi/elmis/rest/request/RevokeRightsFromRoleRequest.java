package org.jsi.elmis.rest.request;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RevokeRightsFromRoleRequest {
	Integer roleId;
	ArrayList<String> rightNameList;
}
