/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jsi.elmis.notification.reader;

import java.util.Date;
import java.util.List;

import org.jsi.elmis.notification.ELMISNotificationReader;
import org.jsi.elmis.rest.result.StockRequisitionResult;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Mekbib
 */
@Component
public class StockRequisitionNotification implements ELMISNotificationReader<StockRequisitionResult> {

	@Autowired
	StoreManagementService storeMgmt;
	
	@Value("${notification.interval}")
	private Long checkTime;
	
    @Override
    public List<StockRequisitionResult> readNotifications(Integer nodeIdToBeNotified, Date afterTime) {
    	return storeMgmt.getStockRequestsBySupplierId(nodeIdToBeNotified, afterTime);
    }
    
    public Long getCheckTime(){
    	return this.checkTime;
    }
}
