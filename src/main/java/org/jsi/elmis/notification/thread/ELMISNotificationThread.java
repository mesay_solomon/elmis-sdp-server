/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jsi.elmis.notification.thread;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.notification.ELMISNotification;
import org.jsi.elmis.notification.ELMISNotificationType;
import org.jsi.elmis.notification.reader.StockRequisitionNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.async.DeferredResult;

/**
 *
 * @author Mekbib
 */
public class ELMISNotificationThread implements ELMISNotification<ELMISNotificationType>{
	private static final Logger logger = LoggerFactory.getLogger(ELMISNotificationThread.class);
	StockRequisitionNotification stockRequisitionNotification;
    DeferredResult<List<ELMISNotificationType>> deferredResult;
    Integer nodeToBeNotified;
    Date afterTime;
    
    
	public ELMISNotificationThread(
			StockRequisitionNotification stockRequisitionNotification,
			DeferredResult<List<ELMISNotificationType>> deferredResult,
			Integer nodeToBeNotified,
			Date afterTime) {
		super();
		this.stockRequisitionNotification = stockRequisitionNotification;
		this.deferredResult = deferredResult;
		this.nodeToBeNotified = nodeToBeNotified;
		this.afterTime = afterTime;
	}

	@Override
    public void run() {
        setResult(deferredResult);
    }

    @Override
    public void setResult(DeferredResult<List<ELMISNotificationType>> deferredResult) {
        if(deferredResult.isSetOrExpired()){
        	//logger.info("The request has expired");
        }else{
            boolean isResultSet = deferredResult.setResult(collectAllNotifications());
            if(isResultSet){
//            	logger.info("The result has been set successfully");
            }else{
//            	logger.error("An error occurred while trying to set the result");
            }
        }
    }

    @Override
    public List<ELMISNotificationType> collectAllNotifications() {
        List<ELMISNotificationType> allNotifications = new ArrayList<>();
        //stock requisition notifications
        boolean hasNotifications = stockRequisitionNotification.readNotifications(nodeToBeNotified, afterTime).size() > 0;
        while(!hasNotifications && !deferredResult.isSetOrExpired()){
        	hasNotifications = stockRequisitionNotification.readNotifications(nodeToBeNotified, afterTime).size() > 0;
        	try {
				Thread.sleep(stockRequisitionNotification.getCheckTime());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        allNotifications.addAll(stockRequisitionNotification.readNotifications(nodeToBeNotified, afterTime));
        //add other notification readers here
        return allNotifications;
    }


  
    
}
