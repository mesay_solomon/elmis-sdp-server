/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jsi.elmis.notification;

import org.springframework.web.context.request.async.DeferredResult;
import java.util.List;
/**
 * 
 * @author Mekbib
 */
public interface ELMISNotification<T> extends Runnable {
    abstract void setResult(DeferredResult<List<T>> deferredResult);
    abstract List<T> collectAllNotifications();
}
