/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.Action;
import org.jsi.elmis.model.TransactionHistoryItem;
import org.jsi.elmis.rest.request.ConsumptionBackLogRquest;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */

@Controller
@ComponentScan
public class GeneralTransacionController {
	
	@Autowired
	TransactionService txnService;
	@Autowired
	NodeService nodeService;
    
    @RequestMapping(value = "/rest-api/reversible-transactions/{nodeId}/{numberOfRecords}/{startingRecordNo}/{scheduleCode}")
	public @ResponseBody List<TransactionHistoryItem> getTransactionsForReversal( 
			@PathVariable Integer nodeId ,
			@PathVariable Integer numberOfRecords , 
			@PathVariable Integer startingRecordNo,
			@PathVariable String scheduleCode) {
		return txnService.getTransactionsToReverse(nodeId, numberOfRecords, startingRecordNo , txnService.getScheduleCodeForReversal());
	}
    
    @RequestMapping(value = "/rest-api/reversible-transactions/{nodeId}/{scheduleCode}/count")
	public @ResponseBody Integer getNoOfTransactionsForReversal( 
			@PathVariable Integer nodeId ,
			@PathVariable String scheduleCode) {
		return txnService.getTransactionsToReverseCount(nodeId , txnService.getScheduleCodeForReversal());
	}
    
    @RequestMapping(value = "/rest-api/reverse/{txnid}/{userid}")
	public @ResponseBody Integer reverseNonTransitiveTransactions( 
			@PathVariable Integer txnid ,
			@PathVariable Integer userid ) throws UnavailableNodeProductException, InsufficientNodeProductException {
		return txnService.reverseNonTransitiveTransaction(txnid, userid);
	}
    
	
    @RequestMapping(value = "/rest-api/transaction/consumption-backlog", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer registerConsumptionBacklog(@RequestBody ConsumptionBackLogRquest request) throws UnavailableNodeProductException, InsufficientNodeProductException {
    	 return txnService.performTransaction(request.getNode(),request.getTxnProducts(),request.getTxnType(),request.getTxnDate() , request.getUserId());
    }
    
    @RequestMapping(value = "/rest-api/transaction-timeline/{nodeId}/{numberOfRecords}/{startingRecordNo}/{scheduleCode}")
	public @ResponseBody List<TransactionHistoryItem> transactionTimeLine( 
			@PathVariable Integer nodeId ,
			@PathVariable Integer numberOfRecords , 
			@PathVariable Integer startingRecordNo,
			@PathVariable String scheduleCode) {
		return txnService.getTransactionsToReverse(nodeId, numberOfRecords, startingRecordNo , txnService.getScheduleCodeForReversal());
	}
    
    @RequestMapping(value = "/rest-api/action/{id}")
	public @ResponseBody Action getAction( 
			@PathVariable Integer id ) throws UnavailableNodeProductException, InsufficientNodeProductException {
		return txnService.getAction(id);
	}
    
    @RequestMapping(value = "/rest-api/txn/physicalcount/delete/{id}/{userId}")
	public @ResponseBody Boolean deletePhysicalCount(@PathVariable Integer id,  @PathVariable Integer userId) throws UnavailableNodeProductException, InsufficientNodeProductException {
		return nodeService.deletePhysicalCount(id, userId);
	}
    
    
}
