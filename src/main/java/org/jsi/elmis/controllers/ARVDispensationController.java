/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.DosageFrequency;
import org.jsi.elmis.model.DosageUnit;
import org.jsi.elmis.model.FacilityClient;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.dto.ARVActivityRegister;
import org.jsi.elmis.model.dto.RegimenCombinationProductDTO;
import org.jsi.elmis.rest.request.ARVClientSearchRequest;
import org.jsi.elmis.rest.request.ARVDispenseRequest;
import org.jsi.elmis.rest.result.ARVClientResult;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */

@Controller
@ComponentScan
public class ARVDispensationController {
	
	@Autowired
	ARVDispensingService arvDispensing;
	
	@Autowired
	PropertiesService perpertiesService;
	
	@RequestMapping(value = "/rest-api/arv/client/{artNo}/{nodeId}")
	public @ResponseBody ARVClientResult getARVClientByART(@PathVariable String artNo,@PathVariable Integer nodeId) {
		FacilityClient arvClient = arvDispensing.getARVClinetByARTNOByART(artNo);
		ARVClientResult result = new ARVClientResult();
		result.setClient(arvClient);
		if(arvClient.getRegimenId() != null){//if client is initiated on regimen
			result.setRegimenCombos(arvDispensing.getRegimenProductCombos(arvClient.getRegimenId()));
			result.setDefaultCombo(arvDispensing.getLastDispensedComboForClient(artNo));//previous combination dispensed to user. getLastDispensedComboForClient returns null if client is new or regimen has been changed 
			
			if(result.getDefaultCombo() != null && result.getDefaultCombo().getId() != null){
				result.setAppointmentDate(arvDispensing.getPreviousDispensationForCombination(result.getDefaultCombo().getId(), artNo).getNextVisitDate());
				result.setProducts(arvDispensing.getRegimenComboProducts(artNo, nodeId, result.getDefaultCombo().getId()));
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/rest-api/arv/client-by-nrc/{nrcNo}/{nodeId}")
	public @ResponseBody ARVClientResult getARVClientByNRC(@PathVariable String nrcNo,@PathVariable Integer nodeId) {
		FacilityClient arvClient = arvDispensing.getARVClinetByARTNOByNRC(nrcNo);
		ARVClientResult result = new ARVClientResult();
		result.setClient(arvClient);
		if(arvClient.getRegimenId() != null){//if client is initiated on regimen
			result.setRegimenCombos(arvDispensing.getRegimenProductCombos(arvClient.getRegimenId()));
			result.setDefaultCombo(arvDispensing.getLastDispensedComboForClient(arvClient.getArtNumber()));//previous combination dispensed to user. getLastDispensedComboForClient returns null if client is new or regimen has been changed 
			
			if(result.getDefaultCombo() != null && result.getDefaultCombo().getId() != null){
				result.setAppointmentDate(arvDispensing.getPreviousDispensationForCombination(result.getDefaultCombo().getId(), arvClient.getArtNumber()).getNextVisitDate());
				result.setProducts(arvDispensing.getRegimenComboProducts(arvClient.getArtNumber(), nodeId, result.getDefaultCombo().getId()));
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/rest-api/arv/client/expected")
	public @ResponseBody List<FacilityClient> getExpectedClients() {
		return arvDispensing.getARVClientsExpectedToday();
	}
	
	@RequestMapping(value = "/rest-api/arv/artprefix")
	public @ResponseBody String getARVDefaultPrefix() {
		return perpertiesService.getARTDefaultPrefix();
	}
	
	@RequestMapping(value = "/combination-products")
	public @ResponseBody List<RegimenCombinationProductDTO> getCombinationProducts(
			@RequestParam(value="artNo", required=true) String artNo , 
			@RequestParam(value="nodeId", required=true) Integer nodeId,
			@RequestParam(value="comboId", required=true) Integer comboId) {
		return arvDispensing.getRegimenComboProducts(artNo, nodeId, comboId);
	}
	
	
    @RequestMapping(value = "/dispense-arv", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer dispenseARV(@RequestBody ARVDispenseRequest request) throws UnavailableNodeProductException, InsufficientNodeProductException {
    	 return arvDispensing.dispenseARV(request.getNode(), request.getArvDispensationItems(), request.getDispensationDate(), request.getUserId(), request.getProductComboId() , request.getClientId() , request.getActionOrder());
    }
    
    @RequestMapping(value = "/register-arv-client", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer registerARVClient(@RequestBody FacilityClient arvClient) {
    	 return arvDispensing.registerARVClient(arvClient);
    }
    
    @RequestMapping(value = "/update-arv-client", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer updateARVClientInfo(@RequestBody FacilityClient arvClient) {
    	 return arvDispensing.updateARVClient(arvClient);
    }
    
    @RequestMapping(value = "/change-regimen", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer changeRegimen(@RequestBody FacilityClient client) {
    	 return arvDispensing.changeRegimen(client);
    }

	@RequestMapping(value = "/regimen-lines")
	public @ResponseBody List<RegimenLine> getRegimenLines() {
		return arvDispensing.getAllRegimenLines();
	}
	
	@RequestMapping(value = "/dosage-units")
	public @ResponseBody List<DosageUnit> getDosageUnits() {
		return arvDispensing.getDosageUnits();
	}
	
	@RequestMapping(value = "/dosage-frequencies")
	public @ResponseBody List<DosageFrequency> getDosageFrequencies() {
		return arvDispensing.getDosageFrequencies();
	}
	
	@RequestMapping(value = "/rest-api/arv/regimens/{lineId}")
	public @ResponseBody List<Regimen> getRegimensInLine(
			@PathVariable Integer lineId) {
		return arvDispensing.getRegimensByLineId(lineId);
	}
	
    @RequestMapping(value = "/advanced-arv-client-search", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody List<FacilityClient> getARVClientByProperyCombos(@RequestBody ARVClientSearchRequest request) {
    	 return arvDispensing.getARVClinetByARTNOByPropertyCombinations(request.getFrom(), request.getTo(), request.getFirstName(), request.getLastName(), request.getSex());
    }
    
    @RequestMapping(value = "/rest-api/arv/activity-register/{nodeId}/{from}/{to}")
	public @ResponseBody ARVActivityRegister generateARVACtivtyRegister( 
			@PathVariable Integer nodeId,
			@PathVariable Date from,
			@PathVariable Date to) {
		return arvDispensing.generateARVActivityRegister(nodeId , from, to);
	}
    
    @RequestMapping(value = "/rest-api/arv/reset-next-visit/{clientId}", method = RequestMethod.GET)
    public @ResponseBody Integer resetNextVisitDate(@PathVariable(value = "clientId") Integer clientId) {
    	 return arvDispensing.resetClientNextVisitDate(clientId);
    }
}
