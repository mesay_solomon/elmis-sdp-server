/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.NodeProductDateBalance;
import org.jsi.elmis.model.NodeProgram;
import org.jsi.elmis.model.NodeType;
import org.jsi.elmis.model.PhysicalCount;
import org.jsi.elmis.rest.request.AdjustmentRequest;
import org.jsi.elmis.rest.request.PhysicalCountRequest;
import org.jsi.elmis.service.interfaces.AdjustmentService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author Mesay S. Taye
 *
 */

@Controller
public class NodeController {
	
	@Autowired
	NodeService nodeService;
	@Autowired
	TransactionService txnService;
	@Autowired
	AdjustmentService adjService;
	
	@RequestMapping(value = "/get-nodes-by-type")
	public @ResponseBody List<Node> getNodesOfType(
			@RequestParam(value="type", required=true, defaultValue="") String type) {
		return nodeService.getNodesByType(type);
	}
	
	@RequestMapping(value = "/save-node")
	public @ResponseBody Integer saveNode(@RequestBody Node node) {
		return nodeService.saveNode(node);
	}
	
	@RequestMapping(value = "/update-node")
	public @ResponseBody Integer udpateNode(@RequestBody Node node) {
		return nodeService.updateNode(node);
	}
	
	@RequestMapping(value = "/delete-node")
	public @ResponseBody Integer deleteNode(@RequestParam(value="nodeId", required=true, defaultValue="") Integer nodeId) {
		return nodeService.deleteNode(nodeId);
	}
	
    @RequestMapping(value = "/rest-api/node/physical-count", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Boolean doPhysicalCount(@RequestBody PhysicalCountRequest request) {
    	 return nodeService.doPhysicalCount(request.getNodeId(), request.getPcProducts() , request.getDate() , request.getUserId());
    }
    
    @RequestMapping(value = "/rest-api/node/product/latest-physical-count/{nodeid}/{productid}", method = RequestMethod.GET)
    public @ResponseBody PhysicalCount getLatestPhysicalCount(@PathVariable(value="nodeid") Integer nodeId,@PathVariable(value="productid") Integer productId) throws UnavailablePhysicalCountException, UnavailableNodeProductException {
    	 return nodeService.getLatestPhysicalCount(nodeId , productId);
    }
    
    @RequestMapping(value = "/adjustment", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Boolean doAdjustment(@RequestBody AdjustmentRequest request) throws UnavailableNodeProductException, InsufficientNodeProductException {
    	 return adjService.doAdjustment(request.getNode(), request.getProducts(), request.getDate(), request.getUserId(),request.getActionOrder());
    }
	
    @RequestMapping(value = "/rest-api/node/get-all-nodetypes")
	public @ResponseBody List<NodeType> getNodeTypes() {
		return nodeService.getNodeTypes();
	}
	
	@RequestMapping(value = "/get-all-nodes")
	public @ResponseBody List<Node> getNodesOfType() {
		return nodeService.getAllNodes();
	}
	
	@RequestMapping(value = "/save-node-program")
	public @ResponseBody Boolean saveNode(@RequestBody String nodeProgramsJSON) {
		ArrayList<NodeProgram> nodePrograms = new Gson().fromJson(nodeProgramsJSON, new TypeToken<ArrayList<NodeProgram>>() {}.getType());
		return nodeService.saveNodePrograms(nodePrograms);
	}
	
	@RequestMapping(value = "/rest-api/node/update-node-programs")
	public @ResponseBody Boolean updateNodePrograms(@RequestBody String nodeProgramsJSON) {
		ArrayList<NodeProgram> nodePrograms = new Gson().fromJson(nodeProgramsJSON, new TypeToken<ArrayList<NodeProgram>>() {}.getType());
		return nodeService.updateNodePrograms(nodePrograms);
	}
	
	@RequestMapping(value = "/get-nodes-by-program/{programId}", method = RequestMethod.GET)
	public @ResponseBody List<Node> getNodesByProgram(@PathVariable( value="programId") Integer programId) {
		return nodeService.getNodesByProgram(programId);
	}
	
	@RequestMapping(value = "/get-node-programs-by-node/{nodeId}", method = RequestMethod.GET)
	public @ResponseBody List<NodeProgram> getNodeProgramsByNode(@PathVariable( value="nodeId") Integer nodeId) {
		return nodeService.getNodeProgramsByNode(nodeId);
	}
	
	@RequestMapping(value = "/delete-node-programs", method = RequestMethod.POST)
	public @ResponseBody Boolean deleteNodePrograms(@RequestBody String nodeProgramsJSON) {
		ArrayList<NodeProgram> nodePrograms = new Gson().fromJson(nodeProgramsJSON, new TypeToken<ArrayList<NodeProgram>>() {}.getType());
		return nodeService.deleteNodePrograms(nodePrograms);
	}
	
	@RequestMapping(value = "/rest-api/node/node-products-without-pc", method = RequestMethod.GET)
	public @ResponseBody ArrayList<NodeProduct> getNodeProductsWithoutPC(
			@RequestParam(value="periodid" , required=false) Integer periodId,
			@RequestParam(value="schedulecode" , required=false) String scheduleCode,
			@RequestParam(value="programcode" , required=false) String programCode) {
		ArrayList<NodeProduct> nodeProducts = (ArrayList<NodeProduct>) nodeService.getNodeProductsPCNotCompletedFor(periodId , scheduleCode ,  programCode);
		return nodeProducts;
	}
	
	@RequestMapping(value = "/rest-api/node/node/balance-on-date/{date}/{nodeId}/{productId}", method = RequestMethod.GET)
	public @ResponseBody  NodeProductDateBalance getBalanceOnDate(
			@PathVariable(value="date") Date date , 
			@PathVariable(value="nodeId") Integer nodeId , 
			@PathVariable(value="productId") Integer productId
			){
		return txnService.getBalanceOnDate(date, nodeId, productId);
	}
}

