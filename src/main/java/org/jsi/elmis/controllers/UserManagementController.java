package org.jsi.elmis.controllers;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.configuration.AssignUserNodeRoleRequestObject;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Right;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.RoleRight;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNodeRole;
import org.jsi.elmis.rest.request.RevokeRightsFromRoleRequest;
import org.jsi.elmis.rest.request.UserAuthenticationRequest;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;
import org.jsi.elmis.service.interfaces.UserManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class UserManagementController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserManagementController.class);
	
	@Autowired
	UserManagementService userManagementService;
	
	@RequestMapping(value = UserManagementUriConstants.AVAILABLE_USER_MGT_WS)
	public String process(){		
		return "TEST : Usermanagement web services available";
	}
	
	@RequestMapping(value = UserManagementUriConstants.CREATE_USER, method = RequestMethod.POST)
    public @ResponseBody Integer createUser(@RequestBody String newUserJson) {
		logger.info("*** Start insertion of a new user");
		Gson gson = new Gson();
		User user = gson.fromJson(newUserJson, User.class);
		return userManagementService.saveUserInfo(user);
	}
	@RequestMapping(value = UserManagementUriConstants.SELECT_BY_USER_ID, method = RequestMethod.GET)
	public @ResponseBody User selectUserById(@PathVariable("id") int id){
		logger.info(" *** retrieving user with ID "+id);
		User user = userManagementService.selectUserById(id);
		return user;
	}
	
	@RequestMapping(value = UserManagementUriConstants.SELECT_BY_USER_EMAIL, method = RequestMethod.GET)
	public @ResponseBody User selectUserById(@PathVariable("email") String email){
		logger.info(" *** retrieving user with email "+email);
		User user = userManagementService.selectUserByEmail(email);
		return user;
	}
	
	@RequestMapping(value = UserManagementUriConstants.UPDATE_USER, method = RequestMethod.POST)
	public @ResponseBody Integer updateUser(@RequestBody User user){
		logger.info(" *** updating user with ID "+user.getId());
		
		return userManagementService.updateUser(user);
	}
	
	@RequestMapping(value = UserManagementUriConstants.DELETE_USER, method = RequestMethod.GET)
	public @ResponseBody Integer deleteUser(@RequestParam(value="userId", required=true, defaultValue="") Integer userId){
		logger.info(" *** deleting user with ID " + userId);
		return userManagementService.deleteUser(userId);
	}
	
	@RequestMapping(value = UserManagementUriConstants.SAVE_ROLE, method = RequestMethod.POST)
	public @ResponseBody Integer saveRole(@RequestBody String newRoleJson){
		Role role = new Gson().fromJson(newRoleJson, Role.class);
		logger.info(" *** Saving Role "+role.getName());
		
		return userManagementService.saveRole(role);
	}
	
	@RequestMapping(value = UserManagementUriConstants.DELETE_ROLE, method = RequestMethod.GET)
	public @ResponseBody Integer deleteRole(@PathVariable(value="roleId") Integer roleId){
		logger.info(" *** deleting role with ID " + roleId);
		return userManagementService.deleteRole(roleId);
	}
	
/*	@RequestMapping(value = UserManagementUriConstants.ASSIGN_USER_NODE_ROLE, method = RequestMethod.POST)
	public @ResponseBody Integer assignUserNodeRoles(@RequestBody AssignUserNodeRoleRequestObject assignUserNodeRoleRequestObject){
		logger.info(" *** Assigning User Node Roles ");
		User user = assignUserNodeRoleRequestObject.getUser();
		Node node = assignUserNodeRoleRequestObject.getNode();
		List<Role> nodeRoles = assignUserNodeRoleRequestObject.getRoleList();
		
		return userManagementService.assignUserNodeRoles(user, node, nodeRoles);
	}*/
	
	@RequestMapping(value = UserManagementUriConstants.ASSIGN_USER_NODE_ROLE, method = RequestMethod.POST)
	public @ResponseBody Boolean assignUserNodeRoles(@RequestBody String jsonUserNodeRoles){
		List<UserNodeRole> userNodeRoles = new Gson().fromJson(jsonUserNodeRoles, new TypeToken<ArrayList<UserNodeRole>>() {}.getType());
		return userManagementService.insertUserNodeRoles(userNodeRoles);
	}
	
	@RequestMapping(value = UserManagementUriConstants.CREATE_ROLE_RIGHT, method = RequestMethod.POST)
	public @ResponseBody String saveRoleRight(@RequestBody RoleRight roleRight){
		userManagementService.saveRoleRight(roleRight);
		return "";
	}
	
	@RequestMapping(value = UserManagementUriConstants.SAVE_ROLE_RIGHTS, method = RequestMethod.POST)
	public @ResponseBody Integer saveRoleRights(@RequestBody String roleRightsJsonString){
		List<RoleRight> roleRights = new Gson().fromJson(roleRightsJsonString,
				new TypeToken<ArrayList<RoleRight>>() {
				}.getType());
		return userManagementService.saveRoleRights(roleRights);
	}
	
	@RequestMapping(value = UserManagementUriConstants.ROLES, method = RequestMethod.GET)
	public @ResponseBody List<Role> getRoles(){
		return userManagementService.getRoles();
	}
	
	@RequestMapping(value = UserManagementUriConstants.GET_ROLE_RIGHTS, method = RequestMethod.GET)
	public @ResponseBody List<RoleRight> getRoleRights(){
		return userManagementService.getRoleRights();
	}
	
	@RequestMapping(value = UserManagementUriConstants.GET_ROLE_RIGHTS_BY_ROLE_ID, method = RequestMethod.GET)
	public @ResponseBody List<RoleRight> getRoleRightsByRoleId(@RequestParam(value="roleId", required=true, defaultValue="") Integer roleId){
		return userManagementService.getRoleRightsByRoleId(roleId);
	}
	
	@RequestMapping(value = UserManagementUriConstants.UPDATE_ROLE, method = RequestMethod.POST)
	public @ResponseBody Integer updateRoles(@RequestBody Role role){
		
		return userManagementService.updateRole(role);
	}
	
	@RequestMapping(value = UserManagementUriConstants.UPDATE_ROLE_RIGHT, method = RequestMethod.POST)
	public @ResponseBody Integer updateRoleRights(@RequestBody RoleRight roleRight){
		
		return userManagementService.updateRoleRights(roleRight);
	}
	
	@RequestMapping(value = UserManagementUriConstants.UPATE_USER_NODE_ROLE, method = RequestMethod.POST)
	public @ResponseBody Integer updateUserNodeRoles(@RequestBody AssignUserNodeRoleRequestObject assignUserNodeRoleRequestObject){
		logger.info(" *** Updating User Node Roles ");
		User user = assignUserNodeRoleRequestObject.getUser();
		Node node = assignUserNodeRoleRequestObject.getNode();
		int id = assignUserNodeRoleRequestObject.getId();
		List<Role> nodeRoles = assignUserNodeRoleRequestObject.getRoleList();
		
		return userManagementService.updateUserNodeRoles(user, node, nodeRoles, id);
	}
	
	@RequestMapping(value = UserManagementUriConstants.USERS, method = RequestMethod.GET)
	public @ResponseBody List<User> getUsers(){
		return userManagementService.getUsers();
	}
	
	@RequestMapping(value = UserManagementUriConstants.RIGHTS, method = RequestMethod.GET)
	public @ResponseBody List<Right> getRights(){
		List<Right> rights = userManagementService.getRights();
		logger.debug("rights " + new Gson().toJson(rights));
		return rights;
	}
	
	@RequestMapping(value = UserManagementUriConstants.LOGIN, method = RequestMethod.POST)
	public @ResponseBody ArrayList<UserNodeRoleRightResult> authenticate(@RequestBody UserAuthenticationRequest userAuthenticationRequest){
		User userToAuthenticate = userAuthenticationRequest.getUser();
		ArrayList<UserNodeRoleRightResult> result = userManagementService.authenticate(userToAuthenticate);
		return result;
	}
	
	@RequestMapping(value = UserManagementUriConstants.GET_USER_ROLE_BY_NODE, method = RequestMethod.POST)
	public @ResponseBody ArrayList<UserNodeRoleRightResult> getUserNodeRoleByUserAndNode(@RequestBody UserNodeRole unr){
		return userManagementService.getUserNodeRoleRightsByUserAndNode(unr);
	}
	
	@RequestMapping(value = UserManagementUriConstants.GET_ALL_USER_NODE_ROLE_RIGHTS, method = RequestMethod.GET)
	public @ResponseBody ArrayList<UserNodeRoleRightResult> getUserNodeRoleByUserId(@RequestParam (value="userId") Integer userId){
		return userManagementService.getUserNodeRoleRightsByUserId(userId);
	}
	
	@RequestMapping(value = UserManagementUriConstants.REVOKE_RIGHTS_FROM_ROLE, method = RequestMethod.POST)
	public @ResponseBody Boolean revokeRightsFromRole(@RequestBody RevokeRightsFromRoleRequest request){
		return userManagementService.revokeRightsFromRole(request.getRoleId(), request.getRightNameList());
	}
	
	@RequestMapping(value = UserManagementUriConstants.REVOKE_ROLE_FROM_USER, method = RequestMethod.POST)
	public @ResponseBody Boolean revokeRolesFromUser(@RequestBody String requestJSON){
		ArrayList<UserNodeRole> userNodeRoles = new Gson().fromJson(requestJSON,
				new TypeToken<ArrayList<UserNodeRole>>() {
				}.getType());
		return userManagementService.revokeRolesFromUser(userNodeRoles);
	}
	
	@RequestMapping(value = UserManagementUriConstants.REVOKE_NODE_FROM_USER, method = RequestMethod.POST)
	public @ResponseBody Boolean revokeNodesFromUser(@RequestBody String requestJSON){
		ArrayList<UserNodeRole> userNodeRoles = new Gson().fromJson(requestJSON,
				new TypeToken<ArrayList<UserNodeRole>>() {
				}.getType());
		return userManagementService.revokeNodesFromUser(userNodeRoles);
	}
}
