package org.jsi.elmis.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.jsi.elmis.notification.ELMISNotificationType;
import org.jsi.elmis.notification.reader.StockRequisitionNotification;
import org.jsi.elmis.notification.thread.ELMISNotificationThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class NotificationsController {
	
	@Autowired
	StockRequisitionNotification stockRequisitionNotification;
	
	@RequestMapping(value = "/rest-api/notifications/all/{supplierNodeId}/{after-time}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON)
    public DeferredResult<List<ELMISNotificationType>>  getNotifications(@PathVariable(value="supplierNodeId") Integer nodeIdToBeNotified,
    		@PathVariable(value="after-time") Long afterTime, final HttpServletResponse response) {
		Date time = new Date();
		time.setTime(afterTime);
    	final DeferredResult<List<ELMISNotificationType>> result = new DeferredResult<>();
    	
    	result.onTimeout(new Runnable(){
    		@Override
    		public void run(){
    			result.setErrorResult("Request Timed out!");
    			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		}
    	});
    	
    	new Thread(new ELMISNotificationThread(stockRequisitionNotification, result, nodeIdToBeNotified, time)).start(); 
        return result;
    }

}
