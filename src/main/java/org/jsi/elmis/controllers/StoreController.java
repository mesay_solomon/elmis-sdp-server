/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.StockRequisition;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.rest.request.IssueProductsRequest;
import org.jsi.elmis.rest.request.IssueToFacilityRequest;
import org.jsi.elmis.rest.request.ReceiveProductsRequest;
import org.jsi.elmis.rest.request.StockControlCardRequest;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */

@Controller
public class StoreController {
	
	@Autowired
	StoreManagementService storeMgmt;
	
    @RequestMapping(value = "/rest-api/node/products/issue", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer issueProduts(@RequestBody IssueProductsRequest request) throws UnavailableNodeProductException, InsufficientNodeProductException {
    	 return storeMgmt.issue(request.getStore(), request.getDispensingPoint(), request.getProductsIssued(), request.getTxnDate(), request.getStockRequestId() , request.getUserId() , request.getActionOrder());
    }
    
    @RequestMapping(value = "/issue-products-to-facility", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer issueProdutsToFacility(@RequestBody IssueToFacilityRequest request) throws UnavailableNodeProductException, InsufficientNodeProductException {
    	 return storeMgmt.issueToOtherFacility(request.getStore(), request.getProductsIssued(), request.getTxnDate(), request.getUserId() , request.getActionOrder());
    }
	
    @RequestMapping(value = "/rest-api/store/receipt", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer receiveProduts(@RequestBody ReceiveProductsRequest request) throws UnavailableNodeProductException, InsufficientNodeProductException {
    	 return storeMgmt.receive(request.getStore(), request.getProductsReceived(), request.getTxnDate(), request.getUserId() , request.getDispatchNo() , request.getProductSourceId() , request.getActionOrder());
    }
	
    @RequestMapping(value = "/rest-api/node/stock-control-card", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody List<StockControlCard> generateStockControlCard(@RequestBody StockControlCardRequest request) {
        return storeMgmt.generateStockControlCard(request.getStore(), request.getFrom(), request.getTo(), request.getProductCode());
    }
    
    @RequestMapping(value = "/rest-api/node/latest-physical-count", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody List<StockControlCard> latestPhysicalCount(@RequestBody StockControlCardRequest request) {
        return storeMgmt.selectMostRecentPhysicalCount(request.getStore().getId(), request.getFrom(), request.getTo(), request.getProductCode());
    }
    
    @RequestMapping(value = "/rest-api/node/total-lna", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody BigDecimal getTotalLnA(@RequestBody StockControlCardRequest request) {
        return storeMgmt.getTotalLnA(request.getStore(), request.getFrom(), request.getTo(), request.getProductCode());
    }
    
    @RequestMapping(value = "/rest-api/node/total-receipts", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody BigDecimal getTotalReceipts(@RequestBody StockControlCardRequest request) {
        return storeMgmt.getTotalProductReceipts(request.getStore(), request.getFrom(), request.getTo(), request.getProductCode());
    }
    
    @RequestMapping(value = "/rest-api/store/stock-requisition", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer requestStock(@RequestBody StockRequisition stockRequisition) {
        return storeMgmt.requestProducts(stockRequisition.getRequesterId(), stockRequisition.getSupplierId(), 
        		stockRequisition.getStockRequisitionItems(), stockRequisition.getTime(), stockRequisition.getUserId());
    }

}
