/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.model.Equipment;
import org.jsi.elmis.model.EquipmentStatus;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.rest.request.UpdateEquipmentsStatusRequest;
import org.jsi.elmis.service.interfaces.LABService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */
@Controller
public class LABController {
	@Autowired
	LABService labService;
	@RequestMapping(value = "rest-api/lab/equipment-status/{periodId}", method = RequestMethod.GET)
	public @ResponseBody List<EquipmentStatus> getEquipmentsStatus(@PathVariable( value="periodId") Integer periodId) {
		return labService.getEquipmentsStatus(periodId);
	}
	
	@RequestMapping(value = "rest-api/lab/reagents/{equipmentId}", method = RequestMethod.GET)
	public @ResponseBody List<Product> getEquipmentReagents(@PathVariable( value="equipmentId") Integer equipmentId) {
		return labService.getEquipmentReagents(equipmentId);
	}
	
    @RequestMapping(value = "/rest-api/lab/equipment/status", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Boolean updateEquipmentInfo(@RequestBody UpdateEquipmentsStatusRequest request) {
    	 labService.updateEquipmentsStatus(request.getEquipmentsStatus());
    	 labService.updateLabTests(request.getLabTests());
    	 //TODO: write proper return value
    	 return true;
    }
    
    @RequestMapping(value = "/rest-api/lab/equipments", method = RequestMethod.GET)
    public @ResponseBody List<Equipment> equipments() {
    	 return labService.getAllEquipments();
    }
    
    @RequestMapping(value = "/rest-api/lab/equipments", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Boolean updateEquipmentInfo(@RequestBody List<Equipment> equipments) {
    	 labService.updateEquipmentsAvailability(equipments);
    	 //TODO: proper return value
    	 return true;
    }
}