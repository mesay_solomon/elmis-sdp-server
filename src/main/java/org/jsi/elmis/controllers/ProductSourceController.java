package org.jsi.elmis.controllers;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.model.ProductSource;
import org.jsi.elmis.service.interfaces.IProductSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProductSourceController {

	@Autowired
	IProductSource productSourceService;


	@RequestMapping("/product-sources")
	public @ResponseBody List<ProductSource> getAllProductSources() {
		return productSourceService.getAllProductSources();
	}

	
	@RequestMapping(value = "/create-product-sources", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer createProdutSources(@RequestBody ProductSource request) {
    	 return productSourceService.create(request);
    }
	
	@RequestMapping(value = "/edit-product-sources", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer editProdutSources(@RequestBody ProductSource request) {
    	 return productSourceService.edit(request);
    }
	
	
	@RequestMapping(value = "/delete-product-sources", method = RequestMethod.POST ,  consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Integer deleteProdutSources(@RequestBody ProductSource request) {
    	 return productSourceService.delete(request);
    }
	

}
