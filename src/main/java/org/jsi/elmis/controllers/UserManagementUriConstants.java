package org.jsi.elmis.controllers;

public class UserManagementUriConstants {
	private static final String LOCATION = "/users/";
	
	public static final String AVAILABLE_USER_MGT_WS = LOCATION+"available";
	public static final String CREATE_USER = LOCATION+"createuser";
	public static final String UPDATE_USER = LOCATION+"updateuser";
	public static final String DELETE_USER = LOCATION+"deleteuser";
	public static final String SELECT_BY_USER_ID = LOCATION+"user/id/{id}";
	public static final String SELECT_BY_USER_EMAIL = LOCATION+"user/email/{email:.+}";
	public static final String VERIFY_USER = LOCATION+"verifyuser";
	public static final String USERS = LOCATION+"users";
	
	public static final String SAVE_ROLE = LOCATION+"createrole";
	public static final String ROLES = LOCATION+"roles";
	public static final String UPDATE_ROLE = LOCATION+"updaterole";
	public static final String DELETE_ROLE = LOCATION+"deleterole/{roleId}";
	
	public static final String ASSIGN_USER_NODE_ROLE = LOCATION+"assignusernoderole";
	public static final String UPATE_USER_NODE_ROLE = LOCATION+"updateusernoderole";
	
	public static final String CREATE_ROLE_RIGHT = LOCATION+"createroleright";
	public static final String SAVE_ROLE_RIGHTS = LOCATION+"saverolerights";
	public static final String GET_ROLE_RIGHTS = LOCATION+"rolerights";
	public static final String UPDATE_ROLE_RIGHT = LOCATION+"updateroleright";
	
	public static final String RIGHTS = LOCATION+"rights";

	public static final String LOGIN = LOCATION +"authenticate";

	public static final String GET_ALL_USER_NODE_ROLE_RIGHTS = LOCATION + "getusernoderolerights";
	public static final String REVOKE_RIGHTS_FROM_ROLE = LOCATION + "revokerightsfromrole";
	public static final String REVOKE_ROLE_FROM_USER = LOCATION + "revokerolesfromuser";
	public static final String REVOKE_NODE_FROM_USER = LOCATION + "revokenodesfromuser";

	public static final String GET_USER_ROLE_BY_NODE = LOCATION + "getuserrolesbyuserandnode";

	public static final String GET_ROLE_RIGHTS_BY_ROLE_ID = LOCATION + "getrolerightbyrole";
	
	public String getLocation(){
		return LOCATION;
	}
	
	public String getAvailableWebServices(){
		StringBuffer availableWS = new StringBuffer();
		availableWS.append(CREATE_USER);
		
		return "";
	}
}
