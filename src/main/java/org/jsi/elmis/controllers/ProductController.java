/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.model.FacilityApprovedProduct;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductCategory;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.rest.result.FacilityApprovedProductResult;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */

@Controller
public class ProductController {
	@Autowired
	MiscellaneousService cmnServices;
	@Autowired
	ProductService productService;
	@Autowired
	NodeService nodeService;

	@RequestMapping("/programs")
	public @ResponseBody List<Program> getAllPrograms() {
		return cmnServices.getAllPrograms();
	}
	
	@RequestMapping("/rest-api/product/{id}")
	public @ResponseBody Product getProduct(Integer id) {
		return productService.getProductById(id);
	}

	@RequestMapping(value = "/facility-approved-products")
	public @ResponseBody List<FacilityApprovedProductResult> getFacilityApprovedProducts() {
		return convertFacilityApprovedProduct(productService
				.getFacilityApprovedProducts());
	}
	
	@RequestMapping(value = "/facility-approved-node-products")
	public @ResponseBody List<NodeProduct> getNodeFacilityApprovedProducts(
			@RequestParam(value="nodeId", required=true, defaultValue="") Integer nodeId,
			@RequestParam(value="programCode", required=true, defaultValue="") String programCode) {
		return nodeService.getFacilityApprovedNodeProducts(nodeId, programCode);
	}
	
	@RequestMapping(value = "/rest-api/product-categories")
	public @ResponseBody List<ProductCategory> getProductCategories() {
		return productService.getProductCategories();
	}

	private List<FacilityApprovedProductResult> convertFacilityApprovedProduct(
			List<FacilityApprovedProduct> fapList) {

		List<FacilityApprovedProductResult> fapResultList = new ArrayList<FacilityApprovedProductResult>();

		for (FacilityApprovedProduct fap : fapList) {
			FacilityApprovedProductResult fapResult = new FacilityApprovedProductResult();
			Product product = fap.getProgramProduct().getProduct();
			Program program = fap.getProgramProduct().getProgram();
			fapResult.setProductId(product.getId());
			fapResult.setProductCode(product.getCode());
			fapResult.setProductName(product.getPrimaryname());
			fapResult.setCategoryId(product.getCategoryid());
			fapResult.setProgramCode(program.getCode());
			fapResult.setGeneralStrength(product.getStrength());
			fapResult.setPackSize(Integer.valueOf(product.getPacksize()));
			fapResult.setRemark("");

			fapResultList.add(fapResult);
		}

		return fapResultList;
	}
}
