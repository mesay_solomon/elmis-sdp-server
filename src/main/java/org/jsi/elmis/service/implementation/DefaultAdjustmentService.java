/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.dao.AdjustmentDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.ProductPeriodStatusDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.Adjustment;
import org.jsi.elmis.model.AdjustmentProduct;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductPeriodStatus;
import org.jsi.elmis.model.ProductPeriodStatusAdjustment;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.service.interfaces.AdjustmentService;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductPeriodStatusService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultAdjustmentService implements AdjustmentService{
	
	@Autowired
	TransactionService txnService;
	@Autowired
	ProductService prodService;
	@Autowired
	NodeService nodeService;
	@Autowired
	ProductPeriodStatusService productPeriodStatusService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	ProductPeriodStatusDAO productPeriodStatusDAO;
	@Autowired
	AdjustmentDAO adjDAO;
	@Autowired
	NodeProductDAO npDAO;
	
	
	@Value("${rnr.schedule.weekly}")
	Boolean rnrSubmittedWeekly;
	@Value("${rnr.schedule.monthly}")
	Boolean rnrSubmittedMonthly;
	
	@Override
	public Boolean doAdjustment(Node node, List<AdjustmentProduct> products, Date date, Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException {
		
		for (AdjustmentProduct adjProd : products) {
			doAdjustment(node, adjProd.getProductCode(),
					adjProd.getLaType(), date, adjProd.getQuantity() , userId , actionOrder);
			if(actionOrder != null){
				if(actionOrder.getPrecedence().equals(ActionOrder.BEFORE)){
					actionOrder.setPrecedence(ActionOrder.AFTER);
					actionOrder.setNeignboringOrder(actionOrder.getNeignboringOrder());
				} else {
					actionOrder.setNeignboringOrder(actionOrder.getNeignboringOrder() + 1);
				}
			}
		}
		
		return true;

	}
	
	
	@Override
	public Integer doAdjustment(Node node, String productCode,
			LossAdjustmentType type, Date date, Double quantity , Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException {
		
		List<TransactionProduct> txnProducts = new ArrayList<TransactionProduct>();
		
		Product product = prodService.getProductByCode(productCode);
		
		TransactionProduct txnProduct = new TransactionProduct();
		txnProduct.setProductId(product.getId());
		txnProduct.setQuantity(BigDecimal.valueOf(quantity));
		
		txnProducts.add(txnProduct);
		
		String txnTypeName = resolveTransactionTypeForAdjustment(type);
		Integer txnId = null;
	
		if (actionOrder == null){
			if(npDAO.selectTxnCountForNodeProductAfterDate(node.getId(), null, date) > 0){
				Integer order = txnService.getMaxActionOrderInADay(date);
				ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
				txnId = txnService.performTransaction(node, txnProducts, txnTypeName, date , userId , ao);
			} else {
				txnId = txnService.performTransaction(node, txnProducts, txnTypeName, date , userId);
			}
		} else {
			txnId = txnService.performTransaction(node, txnProducts, txnTypeName, date , userId , actionOrder);
		}
		
		Adjustment adjustment = new Adjustment();
		adjustment.setTransactionId(txnId);
		adjustment.setAdjustmentType(type.getName());
		
		NodeProduct nodeProd = nodeService.findProductByNodeAndProduct(node.getId(), product.getId());
		adjustment.setNodeProductId(nodeProd.getId());
		adjDAO.insertAdjustment(adjustment);
		
		
		return adjustment.getId();
	}
	
	public Integer saveAdjustment(Adjustment adjustment){
		return adjDAO.insertAdjustment(adjustment);
	}
	
	private String resolveTransactionTypeForAdjustment(LossAdjustmentType laType){
		
		if(laType.getAdditive()){
			return TransactionType.POSITIVE_ADJUSTMENT;
		} else {
			return TransactionType.NEGATIVE_ADJUSTMENT;
		}
	}

	@Override
	public List<LossAdjustmentType> getAllAdjustments() {
		return adjDAO.getAllAdjustmets();
	}
	
	public Integer saveProductPeriodStatusAdjustment(Integer productId , String adjustmentType , BigDecimal quantity , String scheduleCode){
		ProcessingPeriod period = miscService.getCurrentPeriod(scheduleCode);
		ProductPeriodStatus ppStatus =productPeriodStatusDAO.getProductPeriodStatus(period.getId(), productId);
		//TODO: could ppStatus be null?
		ProductPeriodStatusAdjustment ppStatusAdjustment = productPeriodStatusDAO.getProductAdjustmentForPeriod(ppStatus.getId(), adjustmentType);
		if(ppStatusAdjustment == null){
			ppStatusAdjustment = new ProductPeriodStatusAdjustment();
			ppStatusAdjustment.setAdjustmentType(adjustmentType);
			ppStatusAdjustment.setProductPeriodStatusId(ppStatus.getId());
			ppStatusAdjustment.setQuantity(quantity);
			productPeriodStatusDAO.insertProductPeriodStatusAdjustment(ppStatusAdjustment);
		} else {
			ppStatusAdjustment.setQuantity(ppStatusAdjustment.getQuantity().add(quantity));
			productPeriodStatusDAO.updateProductPeriodStatusAdjustment(ppStatusAdjustment);
		}
		return ppStatusAdjustment.getId();
	}

}
