/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.Connectivity;
import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.AdjustmentDAO;
import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.dao.ProcessingPeriodDAO;
import org.jsi.elmis.dao.ProductDAO;
import org.jsi.elmis.dao.ProductPeriodStatusDAO;
import org.jsi.elmis.dao.ProgramDAO;
import org.jsi.elmis.dao.RnRDAO;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.interfacing.openlmis.BaseFactory;
import org.jsi.elmis.interfacing.openlmis.LookupFactory;
import org.jsi.elmis.interfacing.openlmis.RnRSubmissionResult;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProductPeriodStatus;
import org.jsi.elmis.model.ProductPeriodStatusAdjustment;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.model.ReportAndRequisition;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.dto.TransactionHistoryDTO;
import org.jsi.elmis.rnr.LossesAndAdjustments;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.rnr.RnRLineItem;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import us.monoid.json.JSONException;

import com.google.gson.Gson;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultRnRService implements RnRService{
	
	@Autowired
	StoreManagementService storeService;
	@Autowired
	TransactionService txnService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	ProgramDAO programDAO;
	@Autowired
	ProductPeriodStatusDAO prodPeriodStatusDAO;
	@Autowired
	AdjustmentDAO adjDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	RnRDAO rnrDAO;
	@Autowired
	ProcessingPeriodDAO periodDAO;
	@Autowired
	BaseFactory baseFactory;
	
	@Value("${facility.code}")
	private String facilityCode;
	@Value("${elmis.central.approver.name}")
	private String approverName;
	
	@Value("${adjustment.computer.generated.positive}")
	private String computerGeneratedPositiveAdjustment;
	@Value("${adjustment.computer.generated.negative}")
	private String computerGeneratedNegativeAdjustment;
	
	@Value("${elmis.hub.url}")
	private String hubURL;
	@Value("${elmis.rnr.send.to.hub}")
	private Boolean sendRnRToHub;
	@Value("${elmis.central.url}")
	private  String eLMISCentralURL;
	
    @Autowired
    LookupFactory lf;
	
	
	@Override
	public RnR generateRnR(Integer periodId, String programCode , Boolean emergency , String scheduleCode) {
		List<ProgramProduct> programProducts = productDAO.getFacilityApprovedProductsInProgram(programCode , facilityCode);
		
		RnR rnr = new RnR();
		
		rnr.setAgentCode(facilityCode);
		periodId = (emergency)?commonDAO.getCurrentPeriod(scheduleCode).getId():commonDAO.getPreviousPeriod(scheduleCode).getId();
		rnr.setPeriodId(periodId);
		rnr.setApproverName(approverName);
		rnr.setProgramCode(programCode);
		rnr.setEmergency(emergency);
		rnr.setProducts(new ArrayList<RnRLineItem>());
		for (ProgramProduct programProduct : programProducts) {
			ProductPeriodStatus ppStatus = prodPeriodStatusDAO.getProductPeriodStatus(periodId, programProduct.getProductid());
			if(ppStatus != null){
				RnRLineItem rnrLineItem = new RnRLineItem();
				rnrLineItem.setProductCode(programProduct.getProduct().getCode()); 
				//TODO:better casting?
				
				rnrLineItem.setBeginningBalance(ppStatus.getBeginningBalance().intValue());
				rnrLineItem.setQuantityReceived(ppStatus.getQuantityReceived().intValue());
//				rnrLineItem.setQuantityDispensed(ppStatus.getQuantityDispensed().intValue());
				rnrLineItem.setQuantityDispensed((int)Math.ceil(ppStatus.getQuantityDispensed().doubleValue()));
				rnrLineItem.setStockInHand(ppStatus.getStockOnHand().intValue());
				rnrLineItem.setStockOutDays(ppStatus.getStockOutDays());
				rnrLineItem.setAmc(calculateAMC(programProduct.getProductid(), ELMISConstants.AMC_NUMBER_OF_MONTHS , scheduleCode).intValue());
				rnrLineItem.setMaxStockQuantity(ELMISConstants.AMC_NUMBER_OF_MONTHS * rnrLineItem.getAmc());
				Integer qtyRequested = rnrLineItem.getMaxStockQuantity() - rnrLineItem.getStockInHand();
				rnrLineItem.setQuantityRequested(qtyRequested = (qtyRequested >= 0)?qtyRequested:0);
				rnrLineItem.setQuantityApproved(rnrLineItem.getQuantityRequested());
				rnrLineItem.setReasonForRequestedQuantity("R&R");
				
				rnrLineItem.setNewPatientCount(0); //TODO: Remove hard coded value
				
				List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<LossesAndAdjustments>();
				List<ProductPeriodStatusAdjustment> productPeriodAdjustments = prodPeriodStatusDAO.getProductAdjustmentsForPeriod(ppStatus.getId());
				
				for (ProductPeriodStatusAdjustment productPeriodStatusAdjustment : productPeriodAdjustments) {
					LossesAndAdjustments lna = new LossesAndAdjustments();
					lna.setType(adjDAO.findAdjustmentByName(productPeriodStatusAdjustment.getAdjustmentType()));
					lna.setQuantity(productPeriodStatusAdjustment.getQuantity().intValue());
					lossesAndAdjustments.add(lna);
				}
				rnrLineItem.setLossesAndAdjustments(lossesAndAdjustments);
				normalizeRnRLineItem(rnrLineItem);
				rnr.getProducts().add(rnrLineItem);
			}
		}
		return rnr;
	}
	@Override
	public RnR generateRnROnTheFly(Integer periodId, String programCode , Boolean emergency , String scheduleCode) throws Exception {
		List<ProgramProduct> programProducts = productDAO.getFacilityApprovedProductsInProgram(programCode , facilityCode);
		
		RnR rnr = new RnR();
		
		rnr.setAgentCode(facilityCode);
		ProcessingPeriod period = null;
		if(periodId == null){
			period = (emergency)?commonDAO.getCurrentPeriod(scheduleCode):commonDAO.getPreviousPeriod(scheduleCode);
		} else {
			period = miscService.getProcessingPeriod(periodId);
		}
		if(period == null){
			throw new Exception("Period Unavailable");//TODO: specific exception
		}
		rnr.setPeriodId(period.getId());
		rnr.setApproverName(approverName);
		rnr.setProgramCode(programCode);
		rnr.setEmergency(emergency);
		rnr.setProducts(new ArrayList<RnRLineItem>());
		for (ProgramProduct programProduct : programProducts) {
			ProductPeriodStatus ppStatus = buildProductPeriodStatus(programProduct.getProductid(), periodId);
			if(ppStatus != null){
				RnRLineItem rnrLineItem = new RnRLineItem();
				rnrLineItem.setProductCode(programProduct.getProduct().getCode()); 
				//TODO:better casting?
				
				rnrLineItem.setBeginningBalance(ppStatus.getBeginningBalance().intValue());
				rnrLineItem.setQuantityReceived(ppStatus.getQuantityReceived().intValue());
//				rnrLineItem.setQuantityDispensed(ppStatus.getQuantityDispensed().intValue());
				rnrLineItem.setQuantityDispensed((int)Math.ceil(ppStatus.getQuantityDispensed().doubleValue()));
				rnrLineItem.setStockInHand(ppStatus.getStockOnHand().intValue());
				rnrLineItem.setStockOutDays(ppStatus.getStockOutDays());
				rnrLineItem.setAmc(calculateAMCOnTheFly(programProduct.getProductid(), periodId, ELMISConstants.AMC_NUMBER_OF_MONTHS, scheduleCode).intValue());
				rnrLineItem.setMaxStockQuantity(ELMISConstants.AMC_NUMBER_OF_MONTHS * rnrLineItem.getAmc());//TODO: get real value
				Integer qtyRequested = rnrLineItem.getMaxStockQuantity() - rnrLineItem.getStockInHand();
				rnrLineItem.setQuantityRequested(qtyRequested = (qtyRequested >= 0)?qtyRequested:0);
				rnrLineItem.setQuantityApproved(rnrLineItem.getQuantityRequested());
				rnrLineItem.setReasonForRequestedQuantity("R&R");
				
				rnrLineItem.setNewPatientCount(0); //TODO: Remove hard coded value
				
				List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<LossesAndAdjustments>();
				List<ProductPeriodStatusAdjustment> productPeriodAdjustments = prodPeriodStatusDAO.getFacilityStockAdjustments(ppStatus.getProductId(), period.getStartdate(), period.getEnddate());
				
				for (ProductPeriodStatusAdjustment productPeriodStatusAdjustment : productPeriodAdjustments) {
					LossesAndAdjustments lna = new LossesAndAdjustments();
					lna.setType(adjDAO.findAdjustmentByName(productPeriodStatusAdjustment.getAdjustmentType()));
					lna.setQuantity(productPeriodStatusAdjustment.getQuantity().intValue());
					lossesAndAdjustments.add(lna);
				}
				rnrLineItem.setLossesAndAdjustments(lossesAndAdjustments);
				normalizeRnRLineItem(rnrLineItem);
				rnr.getProducts().add(rnrLineItem);
			}
		}
		return rnr;
	}
	
	private RnRLineItem normalizeRnRLineItem(RnRLineItem rnrLineItem){
		Integer totalLnA = 0;
		for (LossesAndAdjustments lna : rnrLineItem.getLossesAndAdjustments()) {
			totalLnA = (lna.getType().getAdditive())? totalLnA + lna.getQuantity():totalLnA - lna.getQuantity();
		}
		
		Integer discrepancy = rnrLineItem.getBeginningBalance() 
				+ rnrLineItem.getQuantityReceived() 
				+ totalLnA 
				- rnrLineItem.getQuantityDispensed()
				- rnrLineItem.getStockInHand() ;
		
		if (discrepancy > 0){
			LossesAndAdjustments generatedLnA = new LossesAndAdjustments();
			generatedLnA.setQuantity(Math.abs(discrepancy));
			generatedLnA.setType(adjDAO.findAdjustmentByName(computerGeneratedNegativeAdjustment));
			if(generatedLnA.getType() == null){
				rnrLineItem.setQuantityDispensed(rnrLineItem.getQuantityDispensed() + discrepancy);
			} else {
 			//	rnrLineItem.getLossesAndAdjustments().add(generatedLnA);
				addLnA(rnrLineItem.getLossesAndAdjustments(), generatedLnA);
			}
		} else if (discrepancy < 0){
			LossesAndAdjustments generatedLnA = new LossesAndAdjustments();
			generatedLnA.setQuantity(Math.abs(discrepancy));
			generatedLnA.setType(adjDAO.findAdjustmentByName(computerGeneratedPositiveAdjustment));
			if(generatedLnA.getType() == null){
				rnrLineItem.setStockInHand(rnrLineItem.getStockInHand() - discrepancy);
			} else {
			//	rnrLineItem.getLossesAndAdjustments().add(generatedLnA);
				addLnA(rnrLineItem.getLossesAndAdjustments(), generatedLnA);
			}
		}
		return rnrLineItem;
	}
	
	private List<LossesAndAdjustments> addLnA(List<LossesAndAdjustments> lnas , LossesAndAdjustments lna){
		for (LossesAndAdjustments currentLnA : lnas) {
			if(currentLnA.getType().equals(lna.getType())){
				currentLnA.setQuantity(currentLnA.getQuantity() + lna.getQuantity());
				return lnas;
			} 
		}
		lnas.add(lna);
		return lnas;
	}

	private BigDecimal calculateAMC(Integer productId , Integer noOfMonths , String scheduleCode){
		List<ProductPeriodStatus> ppStatuses = prodPeriodStatusDAO.getPreviousConsumptions(productId , scheduleCode);
		BigDecimal consumptionSum = BigDecimal.ZERO;
		int i;
			for (i = 0 ; i < noOfMonths && i < ppStatuses.size(); i++) {
				consumptionSum = consumptionSum.add(ppStatuses.get(ppStatuses.size() - 1 - i).getQuantityDispensed());
			}
			if( i == 0 ) return BigDecimal.ZERO;
		return BigDecimal.valueOf(consumptionSum.doubleValue()/i);
	}
	
	private BigDecimal calculateAMCOnTheFly(Integer productId , Integer periodId , Integer noOfPeriods , String scheduleCode){
		return txnService.getAveragePeriodicConsumption(productId, periodId, scheduleCode, noOfPeriods);
	}
	
	public Integer stockOutDays(Integer productId , ProcessingPeriod period){
		Integer stockOutDays = 0;
		
		List<TransactionHistoryDTO> txnHistoryItems = txnService.selectTransactionHistory(null, productId, period.getStartdate());
		Integer curStockOutPeriodStart = -1;
		Boolean stockOutPeriodStarted = false;
		if(txnHistoryItems != null && !txnHistoryItems.isEmpty()){
			if(txnHistoryItems.get(0).getQtyAfterTxn().compareTo(BigDecimal.ZERO) == 0){
				stockOutDays += DateCustomUtil.dateDiff(txnHistoryItems.get(0).getTxnTimestamp(),
						((period.getEnddate().after(DateCustomUtil.getCurrentTime()))?DateCustomUtil.getCurrentTime():period.getEnddate()));
				stockOutPeriodStarted = true;
				curStockOutPeriodStart = 0;
			}
			for(int i= 1; i < txnHistoryItems.size(); i++){
				if(stockOutPeriodStarted){
					if(txnHistoryItems.get(i).getQtyAfterTxn().compareTo(BigDecimal.ZERO) > 0 && 
							txnHistoryItems.get(i).getQtyBeforeTxn().compareTo(BigDecimal.ZERO) == 0){
						stockOutPeriodStarted = false;
					}	
				}
				
			}
		}
		return stockOutDays;
	}

	@Override
	public RnRSubmissionResult submitRnR(RnR rnr) {
		
		//update physical counts
		prodPeriodStatusDAO.updateProductPeriodStatusPhysicalCounts(rnr.getProgramCode() , rnr.getPeriodId());
		
		if( rnr.getProgramCode().equals(ELMISConstants.ARV.getValue()) && sendRnRToHub ){
			if(!Connectivity.isReachable(hubURL))
				return new RnRSubmissionResult(false,ELMISConstants.FAILED.getValue(),"Unable to reach server. Please check your connection and try again.");
		} else {
			if(!Connectivity.isReachable(eLMISCentralURL)){
				return new RnRSubmissionResult(false,ELMISConstants.FAILED.getValue(),"Unable to reach server. Please check your connection and try again.");
			} 
		}
		
		Gson gson = new Gson();
		String rnrJSON = gson.toJson(rnr);
		Boolean isSuccessful = false;
		String errorMsg = null;
		
/*		
		if(!sendRnRToHub){
			ArrayList<Program> programs = lf.getPrograms();//TODO: to be removed , you know when :)
		}
		*/
		try {
			isSuccessful = (Boolean) baseFactory.uploadJSON(ELMISConstants.SERVICE_SDP_REQUISITION.getValue(), "R&R", rnrJSON , Long.class , rnr.getProgramCode());
		}catch(JSONException ex){
			if(ex.getMessage().contains("must begin with")){
				 errorMsg = "User doesn't have permission to submit R&R.";
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorMsg = e.getMessage();
			if(e.getMessage().contains("full authentication required")){
				errorMsg = "User doesn't have permission to submit R&R.";
			}
		}
		
		
		if(isSuccessful){
			
			rnrDAO.saveRnR(new ReportAndRequisition(null, rnr.getPeriodId() , ELMISConstants.SUCCESSFUL.getValue(), "R&R successfully submitted!" , 
					programDAO.selectByCode(rnr.getProgramCode()).getId()));
			return new RnRSubmissionResult(isSuccessful,ELMISConstants.SUCCESSFUL.getValue(),"R&R successfully submitted!");
		} else {
		//	rnrDAO.saveRnR(new ReportAndRequisition(null, rnr.getPeriodId() , ELMISConstants.SUCCESSFUL.getValue(), errorMsg , programDAO.selectByCode(rnr.getProgramCode()).getId()));
			return new RnRSubmissionResult(isSuccessful,ELMISConstants.FAILED.getValue(),errorMsg);
		}

	} 
	
	private ProductPeriodStatus buildProductPeriodStatus(Integer productId , Integer periodId){
		ProductPeriodStatus ppStatus = new ProductPeriodStatus();
		ppStatus.setPeriodId(periodId);
		ppStatus.setProductId(productId);
		ppStatus.setBeginningBalance(storeService.getBeginningBalance(periodId, productId));
		try {
			ppStatus.setLatestPhysicalCount(storeService.getFacilityPhysicalCount(periodId, productId));
		} catch (UnavailablePhysicalCountException e) {
			// TODO Auto-generated catch block
			ppStatus.setBeginningBalance(BigDecimal.ZERO);
			e.printStackTrace();
		}
		ppStatus.setStockOnHand(ppStatus.getLatestPhysicalCount());
		ppStatus.setStockOutDays(0);//TODO: get a correct figure
		ppStatus.setQuantityReceived(txnService.aggregatedTxnQtyByType(productId, TransactionType.RECEIVING, periodId));
		ppStatus.setQuantityDispensed(txnService.aggregatedTxnQtyByType(productId, TransactionType.DISPENSING, periodId));
		return ppStatus;
	}
	@Override
	public ProcessingPeriod getLatestPeriodRnRisSubmittedOrSkippedFor(
			String programCode) {
		return periodDAO.getLatestPeriodRnRisSubmittedOrSkippedFor(programCode);
	}
	
}