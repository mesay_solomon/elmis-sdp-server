package org.jsi.elmis.service.implementation;

import java.util.List;

import org.jsi.elmis.dao.ProductSourceDAO;
import org.jsi.elmis.model.ProductSource;
import org.jsi.elmis.service.interfaces.IProductSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductSourceDefault implements IProductSource{
	@Autowired
	ProductSourceDAO prodsourceDAO;
	private Integer myFacilityId;


	@Override
	public List<ProductSource> getAllProductSources() {
				return this.prodsourceDAO.getAllProductSources();
	}


	@Override
	public Integer create(ProductSource prodSource) {
		prodSource.setFacilityid(myFacilityId);
		return this.prodsourceDAO.insertProductSource(prodSource);
	}


	@Override
	public Integer edit(ProductSource prodSource) {
		
		return  this.prodsourceDAO.editProductSource(prodSource);
	}


	@Override
	public Integer delete(ProductSource request) {

		return this.prodsourceDAO.deleteProductSource(request);
	}

}
