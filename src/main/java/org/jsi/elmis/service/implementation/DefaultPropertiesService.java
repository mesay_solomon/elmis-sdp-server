package org.jsi.elmis.service.implementation;

import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class DefaultPropertiesService implements PropertiesService{
	
	@Value("${facility.code}")
	private String myFacilityCode;
	
	@Value("${art.number.default.prefix}")
	private String artDefaultPrefix;
	
	@Value("${elmis.rnr.send.to.hub}")
	private Boolean sendRnRToHub;

	@Override
	public String getARTDefaultPrefix(){
		return artDefaultPrefix;
	}
	
	@Override
	public String getFacilityCode(){
		return artDefaultPrefix;
	}
	
	@Override
	public Boolean getSendRnRToHub(){
		return sendRnRToHub;
	}
	
/*	
			dbpassword
			dbuser
			dbport
			elmis.central.url
			elmis.central.password
			dbdrive
			facilityid
			dbhost
			dburl=
			facility.code
			elmis.central.approver.name
			art.number.default.prefix
*/
}
