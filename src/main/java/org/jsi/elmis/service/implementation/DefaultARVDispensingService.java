/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.ARVDispensingDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.RegimenDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.ARVDispensation;
import org.jsi.elmis.model.ARVDispensationLineItem;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.DosageFrequency;
import org.jsi.elmis.model.DosageUnit;
import org.jsi.elmis.model.FacilityClient;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.RegimenProductCombination;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.dto.ARVActivityRegister;
import org.jsi.elmis.model.dto.ARVActivityRegisterItem;
import org.jsi.elmis.model.dto.ARVDispensationLineItemDTO;
import org.jsi.elmis.model.dto.RegimenCombinationProductDTO;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultARVDispensingService implements ARVDispensingService {

	@Autowired
	TransactionService txnService;
	@Autowired
	ARVDispensingDAO arvDispDAO;
	@Autowired
	RegimenDAO regDAO;
	@Autowired
	NodeProductDAO npDAO;

	@Override
	public Regimen getRegimenByARTNo(String artNo) {
		return regDAO.getRegimenByART(artNo);
	}

	@Override
	public Integer registerARVClient(FacilityClient arvClient) {
		return arvDispDAO.saveFacilityClient(arvClient);
	}

	@Override
	public Integer updateARVClient(FacilityClient arvClient) {
		return arvDispDAO.updateFacilityClient(arvClient);
	}

	@Override
	public List<RegimenProductCombination> getRegimenProductCombos(
			Integer regimenId) {
		return regDAO.getRegimenProductCombos(regimenId);
	}

	@Override
	public List<RegimenCombinationProductDTO> getRegimenComboProducts(
			String artNo, Integer nodeId, Integer comboId) {
		return regDAO.getRegimenComboProducts(artNo, nodeId, comboId);
	}

	@Override
	public ARVDispensation getPreviousDispensationForCombination(
			Integer comboId, String artNo) {
		return arvDispDAO.selectPreviousDispensationForCombination(comboId,
				artNo);
	}

	@Override
	public RegimenProductCombination getLastDispensedComboForClient(String artNo) {
		return arvDispDAO.getLastDispensedComboForClient(artNo);
	}

	@Override
	public Integer dispenseARV(Node node,
			List<ARVDispensationLineItemDTO> arvDispensationItems,
			Date dispensationDate, Integer userId, Integer productComboId,
			Integer clientId, ActionOrder actionOrder)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException {

		List<TransactionProduct> txnProducts = new ArrayList<TransactionProduct>();
		List<ARVDispensationLineItem> arvLineItems = new ArrayList<ARVDispensationLineItem>();
		for (ARVDispensationLineItemDTO item : arvDispensationItems) {
			txnProducts.add(item.getTxnProduct());
			arvLineItems.add(item.getArvDispensationLineItem());
		}

		Integer txnId = null;
		if (actionOrder == null) {
			if (npDAO.selectTxnCountForNodeProductAfterDate(node.getId(), null,
					dispensationDate) > 0) {
				Integer order = txnService
						.getMaxActionOrderInADay(dispensationDate);// upto date
																	// instead
																	// of in
																	// date
				actionOrder = new ActionOrder(order, ActionOrder.AFTER);
				txnId = txnService.performTransaction(node, txnProducts,
						TransactionType.ARV_DISPENSING, dispensationDate,
						userId, actionOrder);
			} else {
				txnId = txnService.performTransaction(node, txnProducts,
						TransactionType.ARV_DISPENSING, dispensationDate,
						userId);
			}

		} else {
			txnId = txnService.performTransaction(node, txnProducts,
					TransactionType.ARV_DISPENSING, dispensationDate, userId);
		}
		ARVDispensation arvDispensation = new ARVDispensation();
		Date nextVisitDate = calculateNextAppointmentDate(dispensationDate,
				arvLineItems);
		arvDispensation.setNextVisitDate(nextVisitDate);
		arvDispensation.setProductComboId(productComboId);
		arvDispensation.setTransactionId(txnId);
		arvDispensation.setClientId(clientId);

		Integer dispId = arvDispDAO.saveDispensation(arvDispensation);

		for (int i = 0; i < arvLineItems.size(); i++) {
			arvLineItems.get(i).setTransactionProductId(
					txnProducts.get(i).getId());
			arvLineItems.get(i).setArvDispensationId(dispId);
			arvDispDAO.saveDispensationItem(arvLineItems.get(i));
		}
		return dispId;
	}

	private Date calculateNextAppointmentDate(Date currentDispensationDate,
			List<ARVDispensationLineItem> arvDispensationItems) {
		Integer minNoOfDays = null;
		// TODO: proof of correctness of logic
		if (arvDispensationItems.size() > 0)
			minNoOfDays = arvDispensationItems.get(0).getNumberOfDays()
					+ arvDispensationItems.get(0).getRemainingNoOfDays();
		for (ARVDispensationLineItem item : arvDispensationItems) {
			if (item.getNumberOfDays() < minNoOfDays) {
				minNoOfDays = item.getNumberOfDays()
						+ item.getRemainingNoOfDays();
			}
		}
		return DateCustomUtil.addDays(currentDispensationDate, minNoOfDays);
	}

	@Override
	public Integer changeRegimen(FacilityClient client) {
		return arvDispDAO.updateFacilityClientRegimen(client);
	}

	@Override
	public FacilityClient getARVClinetByARTNOByART(String artNo) {
		return arvDispDAO.getARVClientByART(artNo);
	}

	@Override
	public FacilityClient getARVClinetByARTNOByNRC(String nrcNo) {
		return arvDispDAO.getARVClientByNRC(nrcNo);
	}

	@Override
	public List<FacilityClient> getARVClientsExpectedToday() {
		return arvDispDAO.getARVClientExpectedToday();
	}

	@Override
	public List<FacilityClient> getARVClinetByARTNOByPropertyCombinations(
			Date from, Date to, String firstName, String lastName, String sex) {
		return arvDispDAO.getARVClientByPropertyCombinations(from, to,
				firstName, lastName, sex);
	}

	@Override
	public List<Regimen> getAllRegimens() {
		return arvDispDAO.getAllRegimens();
	}

	@Override
	public List<Regimen> getRegimensByLineId(Integer lineId) {
		return arvDispDAO.getAllRegimensByLineID(lineId);
	}

	@Override
	public List<RegimenLine> getAllRegimenLines() {
		return arvDispDAO.getAllRegimenLines();
	}

	@Override
	public List<DosageUnit> getDosageUnits() {
		return regDAO.getDosageUnits();
	}

	@Override
	public List<DosageFrequency> getDosageFrequencies() {
		return regDAO.getDosageFrequencies();
	}

	@Override
	public ARVActivityRegister generateARVActivityRegister(Integer nodeId,
			Date from, Date to) {
		List<ARVActivityRegisterItem> arvActivityRegisterItems = arvDispDAO
				.getActivityRegisterLineItems(nodeId, from, to);
		ARVActivityRegister activityRegister = new ARVActivityRegister();
		activityRegister.setFrom(from);
		activityRegister.setTo(to);
		activityRegister.setActivityRegisterItems(arvActivityRegisterItems);
		return activityRegister;
	}

	@Override
	public Integer resetClientNextVisitDate(Integer clientId) {
		return arvDispDAO.resetClientNextVisitDate(clientId);
	}
}
