/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.ActionDAO;
import org.jsi.elmis.dao.AdjustmentDAO;
import org.jsi.elmis.dao.NodeDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.PhysicalCountDAO;
import org.jsi.elmis.dao.ProcessingPeriodDAO;
import org.jsi.elmis.dao.ProductDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.Action;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.Adjustment;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.NodeProgram;
import org.jsi.elmis.model.NodeType;
import org.jsi.elmis.model.PhysicalCount;
import org.jsi.elmis.model.PhysicalCountAdjustment;
import org.jsi.elmis.model.PhysicalCountProduct;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultNodeService implements NodeService{
	
	@Autowired
	PhysicalCountDAO pcDAO;
	@Autowired
	NodeProductDAO npDAO;
	@Autowired
	NodeDAO nodeDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	ActionDAO actionDAO;
	@Autowired
	ProcessingPeriodDAO periodDAO;
	@Autowired
	AdjustmentDAO adjDAO;
	
	@Autowired
	StoreManagementService storeService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	DefaultTransactionService txnService;
	
	@Value("${facility.code}")
	private String facilityCode;

	@Override
	public Integer doPhysicalCount(Integer nodeId, Integer productId , Double countedQuantity , Date date , Integer userId) {

		PhysicalCount phCount = new PhysicalCount();
		NodeProduct nodeProduct = npDAO.selectNodeProductByNodeandProduct(nodeId, productId);
		
		Action action = new Action();
		action.setTimestamp(date);
		action.setType(ELMISConstants.PHYSICAL_COUNT.getValue());
		action.setUserId(userId);
		action.setOrdinalNumber(actionDAO.getNextOrdinalNumber());
		
		phCount.setNodeProductId(nodeProduct.getId());
		phCount.setCountedQuantity(BigDecimal.valueOf(countedQuantity));
		phCount.setExpectedQuantity(nodeProduct.getQuantityOnHand());
		phCount.setDifference(phCount.getExpectedQuantity().subtract(phCount.getCountedQuantity()).abs());
		phCount.setUserId(userId);
		phCount.setDate(date);
		phCount.setActionId(action.getId());
		
		Integer pcCountId = pcDAO.savePhysicalCount(phCount);
		
		nodeProduct.setQuantityOnHand(BigDecimal.valueOf(countedQuantity));
		nodeProduct.setLatestPhysicalCountId(pcCountId);
		
		npDAO.saveNodeProduct(nodeProduct);
		return pcCountId;
	}
	
	@Override
	public boolean doPhysicalCount(Integer nodeId, List<PhysicalCountProduct> pcProducts , Date date , Integer userId) {
		if(npDAO.selectTxnCountForNodeProductAfterDate(nodeId, null, date) > 0){
			Integer order = null;
			for (PhysicalCountProduct pcp : pcProducts) {
				
				//TODO: action specific action for each product in a physical count?
				order = txnService.getMaxActionOrderInADay(date);
				Action action = new Action();
				action.setTimestamp(date);
				action.setType(ELMISConstants.PHYSICAL_COUNT.getValue());
				action.setUserId(userId);
				
				ActionOrder actionOrder = new ActionOrder(order, ActionOrder.AFTER);
				
				actionDAO.saveAction(action , actionOrder);
				
				PhysicalCount phCount = new PhysicalCount();
				NodeProduct nodeProduct = npDAO.selectNodeProductByNodeandProduct(nodeId, pcp.getProductId());
				
				phCount.setNodeProductId(nodeProduct.getId());
				phCount.setCountedQuantity(pcp.getQuantity());
				phCount.setExpectedQuantity(nodeDAO.getPreviousStockOnHand(pcp.getProductId(), nodeId, order, ActionOrder.AFTER));
				phCount.setDifference(phCount.getExpectedQuantity().subtract(phCount.getCountedQuantity()).abs());
				phCount.setUserId(userId);
				phCount.setDate(date);
				phCount.setActionId(action.getId());
				
				pcDAO.savePhysicalCount(phCount);
			}
		} else {
			for (PhysicalCountProduct pcp : pcProducts) {
				
				//TODO: action specific action for each product in a physical count?
				Action action = new Action();
				action.setTimestamp(date);
				action.setType(ELMISConstants.PHYSICAL_COUNT.getValue());
				action.setUserId(userId);
				action.setOrdinalNumber(actionDAO.getNextOrdinalNumber());
				
				actionDAO.saveAction(action);
				
				PhysicalCount phCount = new PhysicalCount();
				NodeProduct nodeProduct = npDAO.selectNodeProductByNodeandProduct(nodeId, pcp.getProductId());
				
				phCount.setNodeProductId(nodeProduct.getId());
				phCount.setCountedQuantity(pcp.getQuantity());
				phCount.setExpectedQuantity(nodeProduct.getQuantityOnHand());
				phCount.setDifference(phCount.getExpectedQuantity().subtract(phCount.getCountedQuantity()).abs());
				phCount.setUserId(userId);
				phCount.setDate(date);
				phCount.setActionId(action.getId());
				
				Integer pcCountId = pcDAO.savePhysicalCount(phCount);
				
				nodeProduct.setQuantityOnHand(pcp.getQuantity());
				nodeProduct.setLatestPhysicalCountId(pcCountId);
				
				npDAO.saveNodeProduct(nodeProduct);
			}
		}
		
		return true;
	}

	@Override
	public Boolean nodeIsDispensingPointForProduct(Integer nodeId , Integer productId ) {
		List<Program> productPrograms = productDAO.getProductPrograms(productId);
		for (Program program : productPrograms) {
			boolean isDispensingPt = npDAO.nodeIsDispensingPointForProgram(nodeId, program.getId());
			if(isDispensingPt) return true;
		}
		return false;
	}
	
	@Override
	public Boolean nodeIsDispensingPointForProgram(Integer nodeId , Integer programId ) {
		return npDAO.nodeIsDispensingPointForProgram(nodeId , programId);
	}
	
	@Override
	public NodeProduct saveNodeProduct(NodeProduct nodeProduct) {
		return npDAO.saveNodeProduct(nodeProduct);
	}
	
	@Override
	public NodeProduct findProductByNodeAndProduct(Integer nodeId ,  Integer productId) {
		return npDAO.selectNodeProductByNodeandProduct(nodeId, productId);
	}

	@Override
	public List<NodeProduct> getFacilityApprovedNodeProducts(Integer nodeId,
			String programCode) {
		return npDAO.selectNodeProducts(nodeId, programCode , facilityCode);
	}
	
	@Override
	public List<Node> getNodesByType(String type) {
		return nodeDAO.getNodesByType(type);
	}
	
	@Override
	public BigDecimal getBeginningBalanceFromTransactionHistory(Integer nodeId, Integer productId){
		return npDAO.getBeginningBalanceFromTransactionHistory(nodeId, productId); 
	}

	@Override
	public Integer saveNode(Node node) {
		return nodeDAO.saveNode(node);
	}

	@Override
	public Integer updateNode(Node node) {
		return nodeDAO.updateNode(node);
	}

	@Override
	public Integer deleteNode(Integer nodeId) {
		return nodeDAO.deleteNode(nodeId);
	}

	@Override
	public List<Node> getAllNodes() {
		return nodeDAO.getAllNodes();
	}

	@Override
	public Boolean saveNodePrograms(ArrayList<NodeProgram> nodePrograms) {
		return nodeDAO.saveNodePrograms(nodePrograms);
	}

	@Override
	public List<Node> getNodesByProgram(Integer programId) {
		return nodeDAO.getNodesByProgram(programId);
	}

	@Override
	public List<NodeProgram> getNodeProgramsByNode(Integer nodeId) {
		return nodeDAO.selectNodeProgramsByNode(nodeId);
	}

	@Override
	public Boolean deleteNodePrograms(ArrayList<NodeProgram> nodePrograms) {
		try{
			for (NodeProgram nodeProgram : nodePrograms) {
				nodeDAO.deleteNodeProgram(nodeProgram.getId());
			}
		}catch(Exception ex){
			return false;
		}
		return true;
	}

	@Override
	public PhysicalCount getLatestPhysicalCount(Integer nodeId,
			Integer productId) throws UnavailablePhysicalCountException, UnavailableNodeProductException {
		NodeProduct nodeProduct = findProductByNodeAndProduct(nodeId, productId);
		if(nodeProduct == null)
			throw new UnavailableNodeProductException("Node product not available");
		if(nodeProduct.getLatestPhysicalCountId() == null)
			throw new UnavailablePhysicalCountException("No physical count available");
		return npDAO.selectPhysicalCount(nodeProduct.getLatestPhysicalCountId());
	}


	@Override
	public PhysicalCount getEndOfDayPhysicalCount(Integer nodeId,
			Integer productId, Date date) {
		return null;
	}


	@Override
	public PhysicalCount getEndOfMonthPhysicalCount(Integer nodeId,
			Integer productId, ProcessingPeriod period) {

		return null;
	}

	@Override
	public Boolean updateNodePrograms(ArrayList<NodeProgram> nodePrograms) {
		return nodeDAO.updateNodePrograms(nodePrograms);
	}
	
	@Override
	public List<NodeProduct> getNodeProductsPCNotCompletedFor(Integer periodId , String scheduleCode ,String programCode){
		List<NodeProduct> nodeProducts = npDAO.selectNodeProducts(programCode);
		List<NodeProduct> nodeProductsWithoutPC = new ArrayList<NodeProduct>();
		ProcessingPeriod period = null;
		if(periodId != null){
			period = periodDAO.getProcessingPeriod(periodId);
		} else {
			period = miscService.getCurrentPeriod(scheduleCode);
		}
		for (NodeProduct nodeProduct : nodeProducts) {
			if(!isPCDoneForNodeProduct(period, nodeProduct)){
				nodeProductsWithoutPC.add(nodeProduct);
			}
		}	
		return nodeProductsWithoutPC;
	}
	
	private Boolean isPCDoneForNodeProduct(ProcessingPeriod period, NodeProduct nodeProduct){
		List<StockControlCard> sccList =  storeService.selectMostRecentPhysicalCount(nodeProduct.getNodeId(),
				period.getStartdate(), period.getEnddate(), nodeProduct.getProduct().getCode()) ;
		return sccList != null && !sccList.isEmpty();
	}

	@Override
	public List<NodeType> getNodeTypes() {
		return nodeDAO.getNodeTypes();
	}

	@Override
	public Boolean deletePhysicalCount(Integer physicalCountId, Integer userId) throws UnavailableNodeProductException, InsufficientNodeProductException {
		Adjustment adjustment = null;
		List<PhysicalCountAdjustment> physicalCountAdjustments = pcDAO.getPhysicalCountAdjustments(physicalCountId);
		for(PhysicalCountAdjustment physicalCountAdjustment : physicalCountAdjustments){
			adjustment = adjDAO.selectById(physicalCountAdjustment.getAdjustmentId());
			//TODO : reverse adjustment
			txnService.reverseNonTransitiveTransaction(adjustment.getTransactionId(), userId);
			//TODO : delete pcadj
			pcDAO.deletePhysicalCountAdjustment(physicalCountAdjustment.getId());
			//TODO : delete adjustment
			adjDAO.deleteById(adjustment.getTransactionId());
		}
		//TODO : delete pc
		return pcDAO.deletePhysicalCount(physicalCountId);
	}

	@Override
	public Boolean savePhysicalCountAdjustment(
			PhysicalCountAdjustment physicalCountAdjustment) {
		return pcDAO.savePhysicalCountAdjustment(physicalCountAdjustment);
	}

}
