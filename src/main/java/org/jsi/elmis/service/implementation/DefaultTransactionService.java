/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.ActionDAO;
import org.jsi.elmis.dao.NodeDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.PhysicalCountDAO;
import org.jsi.elmis.dao.TransactionDAO;
import org.jsi.elmis.dao.TransactionProductDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.Action;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.NodeProductDateBalance;
import org.jsi.elmis.model.NodeProductTransaction;
import org.jsi.elmis.model.NodeTransaction;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProcessingSchedule;
import org.jsi.elmis.model.ReversalTransaction;
import org.jsi.elmis.model.Transaction;
import org.jsi.elmis.model.TransactionHistoryItem;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.dto.TransactionHistoryDTO;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductPeriodStatusService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultTransactionService implements TransactionService {

	@Autowired
	ProductService productService;
	@Autowired
	ProductPeriodStatusService productPeriodStatusService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	NodeService nodeService;
	@Autowired
	TransactionDAO txnDAO;
	@Autowired
	NodeDAO nodeDAO;
	@Autowired
	ActionDAO actionDAO;
	@Autowired
	TransactionProductDAO txnProdDAO;
	@Autowired
	NodeProductDAO nodeProdDAO;
	@Autowired
	PhysicalCountDAO pcDAO;
	
	@Value("${rnr.schedule.weekly}")
	Boolean rnrSubmittedWeekly;
	@Value("${rnr.schedule.monthly}")
	Boolean rnrSubmittedMonthly;
	@Value("${delete.pc.backlog.entry}")
	Boolean deletePCOnRegisterBacklogTxn;
	
	@Override
	public Integer performTransaction(Node node, List<TransactionProduct> txnProducts, String txnTypeName,
			Date txnDate , Integer userId ) throws UnavailableNodeProductException, InsufficientNodeProductException{
		
		
		TransactionType txnType = txnDAO.getTransactionTypeByName(txnTypeName);
		node.setProducts(nodeProdDAO.selectNodeProducts(node.getId()));
		
		List<NodeProduct> nodeProducts = new ArrayList<NodeProduct>();

		for (TransactionProduct txnProduct : txnProducts){
			NodeProduct nodeProduct = findTxnProductInNode(node.getProducts(), txnProduct);
			if(nodeProduct == null){
				if(txnType.isPositive()){
					nodeProduct = new NodeProduct();
					nodeProduct.setNodeId(node.getId());
					nodeProduct.setProductId(txnProduct.getProductId());
					nodeProduct.setQuantityOnHand(BigDecimal.ZERO);
					nodeProduct.setTotalAdjustments(BigDecimal.ZERO);
					nodeProduct.setTotalInflow(BigDecimal.ZERO);
					nodeProduct.setTotalOutflow(BigDecimal.ZERO);
					
				} else {
					throw new UnavailableNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + node.getName()+".");
				}
			} else {
				if(!txnType.isPositive()){
					if(nodeProduct.getQuantityOnHand().compareTo(txnProduct.getQuantity()) < 0){
						throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + node.getName() + " in the required amount.");
					} 
				}
			}
			nodeProducts.add(nodeProduct);
		}
		Action action = new Action();
		action.setTimestamp(txnDate);
		action.setType(ELMISConstants.TRANSACTION.getValue());
		action.setUserId(userId);
		action.setOrdinalNumber(actionDAO.getNextOrdinalNumber());
		
		actionDAO.saveAction(action);
		
		
		Transaction txn = new Transaction();
		
		txn.setTransactionType(txnType.getId());
		txn.setTransactionTimestamp(txnDate);
		txn.setUserId(userId);
		txn.setActionId(action.getId());
		
		txnDAO.saveTransaction(txn);
		Integer txnID = txn.getId();
		
		NodeTransaction nodeTxn = new NodeTransaction();
		
		nodeTxn.setNodeId(node.getId());
		nodeTxn.setTransactionId(txnID);
		nodeTxn.setPositive(txnType.isPositive());
		
		Integer nodeTxnId = txnDAO.saveNodeTransaction(nodeTxn);
		
/*		ProcessingPeriod weeklyPeriod = null;
		if(rnrSubmittedWeekly)
			weeklyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.WEEKLY);//TODO: what if period is null? Can it even be?
		
		ProcessingPeriod monthlyPeriod = null;
		
		if(rnrSubmittedMonthly)
			monthlyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.MONTHLY);//TODO: what if period is null? Can it even be?
*/		
		Integer i = 0;
		for (TransactionProduct txnProduct : txnProducts) {

			// save txnProduct
			txnProduct.setTransactionId(txnID);
			NodeProduct nodeProduct = nodeProducts.get(i++);//findTxnProductInNode(node.getProducts(), txnProduct);
			
			txnProdDAO.saveTransactionProduct(txnProduct);
			
			NodeProductTransaction npTxn = new NodeProductTransaction();
			
			npTxn.setNodeTransactionId(nodeTxnId);
			npTxn.setQuantityBeforeTransaction(nodeProduct.getQuantityOnHand());
			npTxn.setTransactionQuantity(txnProduct.getQuantity()); 
			
			if(txnType.isPositive() != null){
				if(txnType.isPositive()){
					nodeProduct.setQuantityOnHand(nodeProduct.getQuantityOnHand().add(txnProduct.getQuantity()));
					nodeProduct.setTotalInflow(nodeProduct.getTotalInflow().add(txnProduct.getQuantity()));//TODO: what if transaction is reversal?
				} else {
					nodeProduct.setQuantityOnHand(nodeProduct.getQuantityOnHand().subtract(txnProduct.getQuantity()));
					nodeProduct.setTotalOutflow(nodeProduct.getTotalOutflow().add(txnProduct.getQuantity()));	//TODO: what if transaction is reversal?
				}
				npTxn.setQuantityAfterTransaction(nodeProduct.getQuantityOnHand());
			}
			nodeProdDAO.saveNodeProduct(nodeProduct);
			npTxn.setNodeProductId(nodeProduct.getId());
			txnProdDAO.saveNodeProductTransaction(npTxn);
			
			/*if(rnrSubmittedWeekly && weeklyPeriod != null)
				productPeriodStatusService.saveProductPeriodStatus(weeklyPeriod.getId(), txnProduct.getProductId(), txnProduct.getQuantity(), txnType , ProcessingSchedule.WEEKLY);
			if(rnrSubmittedMonthly && monthlyPeriod != null)
				productPeriodStatusService.saveProductPeriodStatus(monthlyPeriod.getId(), txnProduct.getProductId(), txnProduct.getQuantity(), txnType , ProcessingSchedule.MONTHLY);*/
		}
		return txnID;
	}
	
	@Override
	public Integer performTransaction(Node supplier , Node recipient, List<TransactionProduct> txnProducts, String txnTypeName,
			Date txnDate ,Integer userId) throws UnavailableNodeProductException, InsufficientNodeProductException{
		
		
		TransactionType txnType = txnDAO.getTransactionTypeByName(txnTypeName);
		
/*		ProcessingPeriod weeklyPeriod = null;
		if(rnrSubmittedWeekly)
			weeklyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.WEEKLY);//TODO: what if period is null? Can it even be?
		
		ProcessingPeriod monthlyPeriod = null;
		
		if(rnrSubmittedMonthly)
			monthlyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.MONTHLY);//TODO: what if period is null? Can it even be?
*/		
		supplier.setProducts(nodeProdDAO.selectNodeProducts(supplier.getId()));
		recipient.setProducts(nodeProdDAO.selectNodeProducts(recipient.getId()));
		
		List<NodeProduct> supplierProducts = new ArrayList<NodeProduct>();
		List<NodeProduct> recipientProducts = new ArrayList<NodeProduct>();
		
		for (TransactionProduct txnProduct : txnProducts) {
			NodeProduct supplierProduct = findTxnProductInNode(supplier.getProducts(), txnProduct);
			NodeProduct recipientProduct = findTxnProductInNode(recipient.getProducts(), txnProduct);
			
			if(supplierProduct == null){
				throw new UnavailableNodeProductException("Product with id " + txnProduct.getProductId() +" not available in " + supplier.getName()+".");
			} else if ( txnProduct.getQuantity().compareTo(supplierProduct.getQuantityOnHand()) > 0){
				throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() +" not available in " + supplier.getName() + " in the required amount.");
			}
			if(recipientProduct == null){
				recipientProduct = new NodeProduct();
				recipientProduct.setNodeId(recipient.getId());
				recipientProduct.setProductId(txnProduct.getProductId());
				recipientProduct.setQuantityOnHand(BigDecimal.ZERO);
				recipientProduct.setTotalAdjustments(BigDecimal.ZERO);
				recipientProduct.setTotalInflow(BigDecimal.ZERO);
				recipientProduct.setTotalOutflow(BigDecimal.ZERO);
			}
			supplierProducts.add(supplierProduct);
			recipientProducts.add(recipientProduct);
		}
		
		Action action = new Action();
		action.setTimestamp(txnDate);
		action.setType(ELMISConstants.TRANSACTION.getValue());
		action.setUserId(userId);
		action.setOrdinalNumber(actionDAO.getNextOrdinalNumber());
		
		actionDAO.saveAction(action);
		
		Transaction txn = new Transaction();
		txn.setTransactionType(txnType.getId());
		txn.setTransactionTimestamp(txnDate);
		txn.setUserId(userId);
		txn.setActionId(action.getId());
		txnDAO.saveTransaction(txn);
		Integer txnID = txn.getId();
		
		NodeTransaction supplierTxn = new NodeTransaction();
		supplierTxn.setNodeId(supplier.getId());
		supplierTxn.setTransactionId(txnID);
		supplierTxn.setPositive(false);
		
		NodeTransaction recipientTxn = new NodeTransaction();
		recipientTxn.setNodeId(recipient.getId());
		recipientTxn.setTransactionId(txnID);
		recipientTxn.setPositive(true);
		
		Integer supTxnId = txnDAO.saveNodeTransaction(supplierTxn);
		Integer recTxnId = txnDAO.saveNodeTransaction(recipientTxn);
		
		Integer i = 0;
		for (TransactionProduct txnProduct : txnProducts) {

			Boolean recipientIsABlackHole = nodeService.nodeIsDispensingPointForProduct(recipient.getId(), txnProduct.getProductId());
			//A black hole for a program is a node where any product in that program received by the node gets lost forever(not tracked anymore)
			// save txnProduct
			txnProduct.setTransactionId(txnID);
			txnProdDAO.saveTransactionProduct(txnProduct);
			
			NodeProduct supplierProduct = supplierProducts.get(i);//findTxnProductInNode(supplier.getProducts(), txnProduct);
			NodeProduct recipientProduct = recipientProducts.get(i++);//findTxnProductInNode(recipient.getProducts(), txnProduct);
			
			NodeProductTransaction spProdTxn = new NodeProductTransaction();
			
			spProdTxn.setTransactionQuantity(txnProduct.getQuantity());
			spProdTxn.setNodeTransactionId(supTxnId);
			spProdTxn.setQuantityBeforeTransaction(supplierProduct.getQuantityOnHand());

			NodeProductTransaction rpProdTxn = new NodeProductTransaction();
			
			rpProdTxn.setTransactionQuantity(txnProduct.getQuantity());
			rpProdTxn.setNodeTransactionId(recTxnId);
			rpProdTxn.setQuantityBeforeTransaction(recipientProduct.getQuantityOnHand());

			recipientProduct.setQuantityOnHand(recipientProduct.getQuantityOnHand().add(txnProduct.getQuantity()));
			recipientProduct.setTotalInflow(recipientProduct.getTotalInflow().add(txnProduct.getQuantity()));
			rpProdTxn.setQuantityAfterTransaction(recipientProduct.getQuantityOnHand());

			supplierProduct.setQuantityOnHand(supplierProduct.getQuantityOnHand().subtract(txnProduct.getQuantity()));
			supplierProduct.setTotalOutflow(supplierProduct.getTotalOutflow().add(txnProduct.getQuantity()));	
			spProdTxn.setQuantityAfterTransaction(supplierProduct.getQuantityOnHand());
			
			nodeProdDAO.updateNodeProduct(supplierProduct);
			if(!recipientIsABlackHole)
				nodeProdDAO.saveNodeProduct(recipientProduct);
			
			spProdTxn.setNodeProductId(supplierProduct.getId());
			rpProdTxn.setNodeProductId(recipientProduct.getId());
			
			txnProdDAO.saveNodeProductTransaction(spProdTxn);
			if(!recipientIsABlackHole)
				txnProdDAO.saveNodeProductTransaction(rpProdTxn);
			
		/*	if(recipientIsABlackHole){
				if(rnrSubmittedWeekly && weeklyPeriod != null)
					productPeriodStatusService.saveProductPeriodStatus(weeklyPeriod.getId(), txnProduct.getProductId(), txnProduct.getQuantity(), txnType , ProcessingSchedule.WEEKLY);
				if(rnrSubmittedMonthly && monthlyPeriod !=  null){
					productPeriodStatusService.saveProductPeriodStatus(monthlyPeriod.getId(), txnProduct.getProductId(), txnProduct.getQuantity(), txnType , ProcessingSchedule.MONTHLY);
				}
			}*/
		}
			
		return txnID;
	}
	
	@Override
	public Integer reverseNonTransitiveTransaction(Integer transactionId , Integer userId ) throws UnavailableNodeProductException, InsufficientNodeProductException{
		Transaction txn = txnDAO.getTransaction(transactionId);
		TransactionType txnType = txnDAO.getTransactionType(txn.getTransactionType());
		List<Node> nodes = txnDAO.getNodesInTransaction(transactionId);//TODO: get NodeTransaction instead? Especially when you reverse transitive transactions
		Integer reverseTxnId = performTransaction(nodes.get(0), txn.getTxnProducts(), TransactionType.getReverseTransactionType(txnType.getName()), DateCustomUtil.getCurrentTime(), userId);
		ReversalTransaction revTxn = new ReversalTransaction(null, transactionId, reverseTxnId);
		txnDAO.saveReversalTransaction(revTxn);
		txnDAO.deleteAssociatedInformationWithReversedTxn(transactionId);//TODO: Should be changed. Deleting (but marking) reversed txn info won't be necessary. 
		return reverseTxnId;
	}
	
	private NodeProduct findTxnProductInNode(List<NodeProduct> nodeProducts,
			TransactionProduct txnProduct) {
		for (NodeProduct nodeProduct : nodeProducts) {
			System.out.println(nodeProduct.getProductId() + " - " + txnProduct.getProductId());
			if (nodeProduct.getProductId().equals(txnProduct.getProductId())) {
				System.out.println();
				return nodeProduct;
			}
		}
		return null;
	}
	
	@Override
	public TransactionType getTransactionTypeByName(String name) {
		return txnDAO.getTransactionTypeByName(name);
	}

	@Override
	public List<TransactionHistoryItem> getTransactionsToReverse(Integer nodeId , Integer limit , Integer offset , String scheduleCode) {
		return txnDAO.getTransactionsToReverse(nodeId , limit , offset , scheduleCode);
	}
	
	@Override
	public String getTransactionDescription(Integer txnId){
		return txnDAO.getTransactionDescription(txnId);
	}

	@Override
	public Integer getTransactionsToReverseCount(Integer nodeId, String scheduleCode) {
		return txnDAO.selectTransactionsForReversalCount(nodeId, scheduleCode);
	}
	
	@Override
	public String getScheduleCodeForReversal(){
		if(rnrSubmittedWeekly){
			return ProcessingSchedule.WEEKLY;
		} else {
			return ProcessingSchedule.MONTHLY;
		}
	}

	@Override
	public BigDecimal aggregatedTxnQtyByType(Integer productId, String txnType,
			Integer periodId) {
		ProcessingPeriod period = miscService.getProcessingPeriod(periodId);
		BigDecimal sum = txnDAO.aggregatedTxnQtyByType(productId, txnType, period.getStartdate(), period.getEnddate());
		return (sum == null)?BigDecimal.ZERO:sum;
	}

	@Override
	public BigDecimal getAveragePeriodicConsumption(Integer productId,
			Integer periodId, String scheduleCode, Integer noOfPeriods) {
		
		BigDecimal apc = txnDAO.selectAveragePeriodicConsumption(periodId, productId, scheduleCode, noOfPeriods);
		return (apc == null)?BigDecimal.ZERO:apc;
	}
	
	@Override
	public List<TransactionHistoryDTO> selectTransactionHistory(Integer nodeId,Integer productId,Date from){
		List<TransactionHistoryDTO> txnHistoryItems = txnDAO.selectTransactionHistory(nodeId, productId, from);
		BigDecimal stockOnHand = null;
		if(nodeId == null){
			stockOnHand = nodeProdDAO.getFacilityStockOnHand(productId);
		} else {
			NodeProduct nodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, productId);
			stockOnHand = nodeProd.getQuantityOnHand();
		}
		if(txnHistoryItems != null && !txnHistoryItems.isEmpty()){
			txnHistoryItems.get(0).setQtyAfterTxn(stockOnHand);
			txnHistoryItems.get(0).setQtyBeforeTxn(stockOnHand.subtract(txnHistoryItems.get(0).getSignedTxnQuantity()));
			for(int i = 1; i < txnHistoryItems.size() ; i++){
				txnHistoryItems.get(i).setQtyAfterTxn(txnHistoryItems.get(i - 1).getQtyBeforeTxn());
				txnHistoryItems.get(i).setQtyBeforeTxn(txnHistoryItems.get(i).getQtyAfterTxn().subtract(txnHistoryItems.get(i).getSignedTxnQuantity()));
			}
		}
		return txnHistoryItems; 
	}

	@Override
		public Integer performTransaction(Node supplier , Node recipient, List<TransactionProduct> txnProducts, String txnTypeName,
				Date txnDate ,Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException{
			
			
			TransactionType txnType = txnDAO.getTransactionTypeByName(txnTypeName);
			
	/*		ProcessingPeriod weeklyPeriod = null;
			if(rnrSubmittedWeekly), 
				weeklyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.WEEKLY);//TODO: what if period is null? Can it even be?
			
			ProcessingPeriod monthlyPeriod = null;
			
			if(rnrSubmittedMonthly)
				monthlyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.MONTHLY);//TODO: what if period is null? Can it even be?
	*/		
			supplier.setProducts(nodeProdDAO.selectNodeProducts(supplier.getId()));
			recipient.setProducts(nodeProdDAO.selectNodeProducts(recipient.getId()));
			
			List<NodeProduct> supplierProducts = new ArrayList<NodeProduct>();
			List<NodeProduct> recipientProducts = new ArrayList<NodeProduct>();
			
			for (TransactionProduct txnProduct : txnProducts) {
				NodeProduct supplierProduct = findTxnProductInNode(supplier.getProducts(), txnProduct);
				NodeProduct recipientProduct = findTxnProductInNode(recipient.getProducts(), txnProduct);
				
				if(supplierProduct == null){
					throw new UnavailableNodeProductException("Product with id " + txnProduct.getProductId() +" not available in " + supplier.getName()+".");
				} else {
					
					List<Integer> actionOrders = actionDAO.getActionsAfterAction(supplier.getId(), txnProduct.getProductId(), actionOrder.getNeignboringOrder());
					
					if(actionOrder.getPrecedence().equals(ActionOrder.BEFORE)){
						BigDecimal stockOnHand = nodeDAO.getPreviousStockOnHand(txnProduct.getProductId(), 
								supplier.getId(), actionOrder.getNeignboringOrder(), ActionOrder.BEFORE);
						if(stockOnHand.compareTo(txnProduct.getQuantity()) < 0){
							throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + supplier.getName() + " in the required amount.");
						}
					}
					
					for (Integer order : actionOrders) {
						BigDecimal stockOnHand = nodeDAO.getPreviousStockOnHand(txnProduct.getProductId(), supplier.getId(), order, ActionOrder.AFTER);
						if(stockOnHand.compareTo(txnProduct.getQuantity()) < 0){
							throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + supplier.getName() + " in the required amount.");
						}
					}
					
					
					
				}
				if(recipientProduct == null){
					recipientProduct = new NodeProduct();
					recipientProduct.setNodeId(recipient.getId());
					recipientProduct.setProductId(txnProduct.getProductId());
					recipientProduct.setQuantityOnHand(BigDecimal.ZERO);
					recipientProduct.setTotalAdjustments(BigDecimal.ZERO);
					recipientProduct.setTotalInflow(BigDecimal.ZERO);
					recipientProduct.setTotalOutflow(BigDecimal.ZERO);
				}
				supplierProducts.add(supplierProduct);
				recipientProducts.add(recipientProduct);
			}
			
			Action action = new Action();
			action.setTimestamp(txnDate);
			action.setType(ELMISConstants.TRANSACTION.getValue());
			action.setUserId(userId);
			
			actionDAO.saveAction(action , actionOrder);
			
			Transaction txn = new Transaction();
			txn.setTransactionType(txnType.getId());
			txn.setTransactionTimestamp(txnDate);
			txn.setUserId(userId);
			txn.setActionId(action.getId());
			txnDAO.saveTransaction(txn);
			Integer txnID = txn.getId();
			
			NodeTransaction supplierTxn = new NodeTransaction();
			supplierTxn.setNodeId(supplier.getId());
			supplierTxn.setTransactionId(txnID);
			supplierTxn.setPositive(false);
			
			NodeTransaction recipientTxn = new NodeTransaction();
			recipientTxn.setNodeId(recipient.getId());
			recipientTxn.setTransactionId(txnID);
			recipientTxn.setPositive(true);
			
			Integer supTxnId = txnDAO.saveNodeTransaction(supplierTxn);
			Integer recTxnId = txnDAO.saveNodeTransaction(recipientTxn);
			
			Integer i = 0;
			for (TransactionProduct txnProduct : txnProducts) {
	
				Boolean recipientIsABlackHole = nodeService.nodeIsDispensingPointForProduct(recipient.getId(), txnProduct.getProductId());
				//A black hole for a program is a node where any product in that program received by the node gets lost forever(not tracked anymore)
				// save txnProduct
				txnProduct.setTransactionId(txnID);
				txnProdDAO.saveTransactionProduct(txnProduct);
				
				NodeProduct supplierProduct = supplierProducts.get(i);//findTxnProductInNode(supplier.getProducts(), txnProduct);
				NodeProduct recipientProduct = recipientProducts.get(i++);//findTxnProductInNode(recipient.getProducts(), txnProduct);
				
				NodeProductTransaction spProdTxn = new NodeProductTransaction();
				
				spProdTxn.setTransactionQuantity(txnProduct.getQuantity());
				spProdTxn.setNodeTransactionId(supTxnId);
				spProdTxn.setQuantityBeforeTransaction(supplierProduct.getQuantityOnHand());
	
				NodeProductTransaction rpProdTxn = new NodeProductTransaction();
				
				rpProdTxn.setTransactionQuantity(txnProduct.getQuantity());
				rpProdTxn.setNodeTransactionId(recTxnId);
				rpProdTxn.setQuantityBeforeTransaction(recipientProduct.getQuantityOnHand());
	
				recipientProduct.setQuantityOnHand(recipientProduct.getQuantityOnHand().add(txnProduct.getQuantity()));
				recipientProduct.setTotalInflow(recipientProduct.getTotalInflow().add(txnProduct.getQuantity()));
				rpProdTxn.setQuantityAfterTransaction(recipientProduct.getQuantityOnHand());
	
				supplierProduct.setQuantityOnHand(supplierProduct.getQuantityOnHand().subtract(txnProduct.getQuantity()));
				supplierProduct.setTotalOutflow(supplierProduct.getTotalOutflow().add(txnProduct.getQuantity()));	
				spProdTxn.setQuantityAfterTransaction(supplierProduct.getQuantityOnHand());
				
				nodeProdDAO.updateNodeProduct(supplierProduct);
				if(!recipientIsABlackHole)
					nodeProdDAO.saveNodeProduct(recipientProduct);
				
				spProdTxn.setNodeProductId(supplierProduct.getId());
				rpProdTxn.setNodeProductId(recipientProduct.getId());
				
				txnProdDAO.saveNodeProductTransaction(spProdTxn);
				if(!recipientIsABlackHole)
					txnProdDAO.saveNodeProductTransaction(rpProdTxn);
				
			/*	if(recipientIsABlackHole){
					if(rnrSubmittedWeekly && weeklyPeriod != null)
						productPeriodStatusService.saveProductPeriodStatus(weeklyPeriod.getId(), txnProduct.getProductId(), txnProduct.getQuantity(), txnType , ProcessingSchedule.WEEKLY);
					if(rnrSubmittedMonthly && monthlyPeriod !=  null){
						productPeriodStatusService.saveProductPeriodStatus(monthlyPeriod.getId(), txnProduct.getProductId(), txnProduct.getQuantity(), txnType , ProcessingSchedule.MONTHLY);
					}
				}*/
				if(actionOrder != null && deletePCOnRegisterBacklogTxn){
					pcDAO.deletePhysicalCountsAfterDate(txnDate, recipientProduct.getNode().getId(), recipientProduct.getProductId());
					pcDAO.deletePhysicalCountsAfterDate(txnDate, supplierProduct.getNode().getId(), supplierProduct.getProductId());
				}
			}
				
			return txnID;
		}

	@Override
		public Integer performTransaction(Node node, List<TransactionProduct> txnProducts, String txnTypeName,
				Date txnDate , Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException{
			
			
			TransactionType txnType = txnDAO.getTransactionTypeByName(txnTypeName);
			node.setProducts(nodeProdDAO.selectNodeProducts(node.getId()));
			
			List<NodeProduct> nodeProducts = new ArrayList<NodeProduct>();
	
			for (TransactionProduct txnProduct : txnProducts){
				NodeProduct nodeProduct = findTxnProductInNode(node.getProducts(), txnProduct);
				if(nodeProduct == null){
					if(txnType.isPositive()){
						nodeProduct = new NodeProduct();
						nodeProduct.setNodeId(node.getId());
						nodeProduct.setProductId(txnProduct.getProductId());
						nodeProduct.setQuantityOnHand(BigDecimal.ZERO);
						nodeProduct.setTotalAdjustments(BigDecimal.ZERO);
						nodeProduct.setTotalInflow(BigDecimal.ZERO);
						nodeProduct.setTotalOutflow(BigDecimal.ZERO);
						
					} else {
						throw new UnavailableNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + node.getName()+".");
					}
				} else {
					if(!txnType.isPositive()){
						
						if(actionOrder == null){
							if(nodeProduct.getQuantityOnHand().compareTo(txnProduct.getQuantity()) < 0){
								throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + node.getName() + " in the required amount.");
							}
						} else {
							List<Integer> actionOrders = actionDAO.getActionsAfterAction(node.getId(), txnProduct.getProductId(), actionOrder.getNeignboringOrder());
							
							if(actionOrder.getPrecedence().equals(ActionOrder.BEFORE)){
								BigDecimal stockOnHand = nodeDAO.getPreviousStockOnHand(txnProduct.getProductId(), 
										node.getId(), actionOrder.getNeignboringOrder(), ActionOrder.BEFORE);
								if(stockOnHand.compareTo(txnProduct.getQuantity()) < 0){
									throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + node.getName() + " in the required amount.");
								}
							}
							
							for (Integer order : actionOrders) {
								BigDecimal stockOnHand = nodeDAO.getPreviousStockOnHand(txnProduct.getProductId(), node.getId(), order, ActionOrder.AFTER);
								if(stockOnHand.compareTo(txnProduct.getQuantity()) < 0){
									throw new InsufficientNodeProductException("Product with id " + txnProduct.getProductId() + " is unavailable in " + node.getName() + " in the required amount.");
								}
							}
							
						}
					}
				}
				nodeProducts.add(nodeProduct);
			}
			
			Action action = new Action();
			action.setTimestamp(txnDate);
			action.setType(ELMISConstants.TRANSACTION.getValue());
			action.setUserId(userId);
			
			actionDAO.saveAction(action , actionOrder);
			
			Transaction txn = new Transaction();
			
			txn.setTransactionType(txnType.getId());
			txn.setTransactionTimestamp(txnDate);
			txn.setUserId(userId);
			txn.setActionId(action.getId());
			
			txnDAO.saveTransaction(txn);
			Integer txnID = txn.getId();
			
			NodeTransaction nodeTxn = new NodeTransaction();
			
			nodeTxn.setNodeId(node.getId());
			nodeTxn.setTransactionId(txnID);
			nodeTxn.setPositive(txnType.isPositive());
			
			Integer nodeTxnId = txnDAO.saveNodeTransaction(nodeTxn);
			
	/*		ProcessingPeriod weeklyPeriod = null;
			if(rnrSubmittedWeekly)
				weeklyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.WEEKLY);//TODO: what if period is null? Can it even be?
			
			ProcessingPeriod monthlyPeriod = null;
			
			if(rnrSubmittedMonthly)
				monthlyPeriod = miscService.getCurrentPeriod(ProcessingSchedule.MONTHLY);//TODO: what if period is null? Can it even be?
	*/		
			Integer i = 0;
			for (TransactionProduct txnProduct : txnProducts) {
	
				// save txnProduct
				txnProduct.setTransactionId(txnID);
				NodeProduct nodeProduct = nodeProducts.get(i++);//findTxnProductInNode(node.getProducts(), txnProduct);
				
				txnProdDAO.saveTransactionProduct(txnProduct);
				
				NodeProductTransaction npTxn = new NodeProductTransaction();
				
				npTxn.setNodeTransactionId(nodeTxnId);
				npTxn.setQuantityBeforeTransaction(nodeProduct.getQuantityOnHand());
				npTxn.setTransactionQuantity(txnProduct.getQuantity()); 
				
				if(txnType.isPositive() != null){
					if(txnType.isPositive()){
						nodeProduct.setQuantityOnHand(nodeProduct.getQuantityOnHand().add(txnProduct.getQuantity()));
						nodeProduct.setTotalInflow(nodeProduct.getTotalInflow().add(txnProduct.getQuantity()));//TODO: what if transaction is reversal?
					} else {
						nodeProduct.setQuantityOnHand(nodeProduct.getQuantityOnHand().subtract(txnProduct.getQuantity()));
						nodeProduct.setTotalOutflow(nodeProduct.getTotalOutflow().add(txnProduct.getQuantity()));	//TODO: what if transaction is reversal?
					}
					npTxn.setQuantityAfterTransaction(nodeProduct.getQuantityOnHand());
				}
				nodeProdDAO.saveNodeProduct(nodeProduct);
				npTxn.setNodeProductId(nodeProduct.getId());
				txnProdDAO.saveNodeProductTransaction(npTxn);
				
				if(actionOrder != null && deletePCOnRegisterBacklogTxn){
					pcDAO.deletePhysicalCountsAfterDate(txnDate, nodeProduct.getNode().getId(), nodeProduct.getProduct().getId());
				}
			}
			return txnID;
		}
	
	@Override
	public NodeProductDateBalance getBalanceOnDate(Date date , Integer nodeId , Integer productId){
		Integer order = getMaxActionOrderInADay(date);
		BigDecimal stockOnHand = nodeDAO.getPreviousStockOnHand(productId, nodeId, order, ActionOrder.AFTER);
		List<Integer> actionOrders = actionDAO.getActionsAfterAction(nodeId, productId , order);
		BigDecimal minSoH = null;
		for (Integer aOrder : actionOrders) {
			BigDecimal stockOnHandAfterAction = nodeDAO.getPreviousStockOnHand(productId, nodeId, aOrder, ActionOrder.AFTER);
			if(minSoH == null || stockOnHandAfterAction.compareTo(minSoH) < 0 ){
				minSoH = stockOnHandAfterAction;
			}
		}
		
		return new NodeProductDateBalance(stockOnHand, minSoH);
	}
	
	@Override
	public Action getAction(Integer id){
		return actionDAO.getAction(id);
	}

	@Override
	public Integer getTransactionCountOnNodeProductAfterDate(Integer nodeId,
			Integer productId, Date txnDate) {
		return nodeProdDAO.selectTxnCountForNodeProductAfterDate(nodeId, productId, txnDate);
	}
	
	@Override
	public Integer getMaxActionOrderInADay(Date date){
		return txnDAO.maxActionOrderOnDate(date);
	}
}
