/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.common.constants.Constant;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.HIVTestingDAO;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.StoreDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableHIVTestProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.HIVTest;
import org.jsi.elmis.model.HIVTestProduct;
import org.jsi.elmis.model.HIVTestStep;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.PhysicalCount;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.dto.HIVProductAvailabilityDTO;
import org.jsi.elmis.model.dto.HIVTestDARItem;
import org.jsi.elmis.model.dto.HIVTestDTO;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.rest.result.HIVTestDARResult;
import org.jsi.elmis.service.interfaces.HIVTestingService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultHIVTestingService implements HIVTestingService {

	@Autowired
	TransactionService transctionService;
	@Autowired
	ProductService productService;
	@Autowired
	HIVTestingDAO hivTestingDAO;
	@Autowired
	NodeProductDAO nodeProdDAO;
	@Autowired
	StoreManagementService storeService;
	@Autowired
	NodeService nodeService;
	@Autowired
	StoreDAO storeDAO;
	
	@Override
	public Integer doHIVTest(HIVTestDTO hivTest, Node site,
			Date txnDate, Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException {
		Integer confirmatoryTxnId = null;
		Integer screeningTxnId = null;
		if(hivTest.getPurpose().equalsIgnoreCase(ELMISConstants.QUALITY_CONTROL.getValue())
				|| hivTest.getPurpose().equalsIgnoreCase(ELMISConstants.OTHER.getValue())){
			String finalResult = null;
			Integer hivTestID = hivTestingDAO.saveHIVTest(new HIVTest(null, site
					.getId(), finalResult, null , hivTest.getPurpose()));
			
			//
			if(hivTest.getConfirmatoryResult() != null){
				
				List<TransactionProduct> confirmatoryProducts = new ArrayList<TransactionProduct>() {
					private static final long serialVersionUID = 1L;
	
					{
						add(new TransactionProduct(null, getHIVProduct(ELMISConstants.CONFIRMATORY.getValue()).getProductId(), BigDecimal.ONE, null));
					}
				};
				if(actionOrder == null){
					if(nodeProdDAO.selectTxnCountForNodeProductAfterDate(site.getId(), null, txnDate) > 0){
						Integer order = transctionService.getMaxActionOrderInADay(txnDate);
						ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
						confirmatoryTxnId = transctionService.performTransaction(site, confirmatoryProducts,
								TransactionType.CONFIRMATORY_TEST, txnDate, userId , ao);
					} else {
						confirmatoryTxnId = transctionService.performTransaction(site, confirmatoryProducts,TransactionType.CONFIRMATORY_TEST, txnDate, userId);
					}
				} else {
					confirmatoryTxnId = transctionService.performTransaction(site, confirmatoryProducts,TransactionType.CONFIRMATORY_TEST, txnDate, userId , actionOrder);
				}
				//TODO: HIV Test step for QC makes any sense?
				hivTestingDAO.saveHIVTestStep(new HIVTestStep(null,confirmatoryTxnId,ELMISConstants.CONFIRMATORY.getValue(), hivTestID , hivTest.getConfirmatoryResult()));
				
				
				
			} else if (hivTest.getScreeningResult() != null) {
				// record screenig transaction
				List<TransactionProduct> screeningProducts = new ArrayList<TransactionProduct>() {
					private static final long serialVersionUID = 1L;
					{
						add(new TransactionProduct(null, getHIVProduct(
								ELMISConstants.SCREENING.getValue())
								.getProductId(), BigDecimal.ONE, null));
					}
				};
				if (actionOrder == null) {
					if (nodeProdDAO.selectTxnCountForNodeProductAfterDate(site.getId(), null, txnDate) > 0) {
						
						Integer order = transctionService.getMaxActionOrderInADay(txnDate);//upto date instead of in date
						
						ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
						screeningTxnId = transctionService.performTransaction(site, screeningProducts,TransactionType.SCREENING_TEST,txnDate, userId , ao);
						
					} else {
						screeningTxnId = transctionService.performTransaction(site, screeningProducts,TransactionType.SCREENING_TEST,txnDate, userId);
					}
				} else {
					screeningTxnId = transctionService.performTransaction(site,screeningProducts, TransactionType.SCREENING_TEST,txnDate, userId, actionOrder);
				}
		
				hivTestingDAO.saveHIVTestStep(new HIVTestStep(null, screeningTxnId,ELMISConstants.SCREENING.getValue(),hivTestID , hivTest.getScreeningResult()));
			}
			return hivTestID;
			
		} else {
				// record hiv test
				String finalResult = null;
				if(hivTest.getConfirmatoryResult() != null){
					//confirmatory non-reactive is invalid
					if(hivTest.getConfirmatoryResult().equalsIgnoreCase(ELMISConstants.NON_REACTIVE.getValue())){
						finalResult = ELMISConstants.INDETERMINATE.getValue();
					} else if (hivTest.getConfirmatoryResult().equalsIgnoreCase(ELMISConstants.REACTIVE.getValue())){
						finalResult = ELMISConstants.REACTIVE.getValue();
					}
				} else {
					if(hivTest.getScreeningResult().equalsIgnoreCase(ELMISConstants.NON_REACTIVE.getValue())){
						finalResult = ELMISConstants.NON_REACTIVE.getValue();
					} else if (hivTest.getScreeningResult().equalsIgnoreCase(ELMISConstants.INVALID.getValue())){
						finalResult =  ELMISConstants.INVALID.getValue();
					}
				}
				Integer hivTestID = hivTestingDAO.saveHIVTest(new HIVTest(null, site
						.getId(), finalResult, hivTest.getClientNumber(), hivTest.getPurpose()));
				// record screenig transaction
				List<TransactionProduct> screeningProducts = new ArrayList<TransactionProduct>() {
					private static final long serialVersionUID = 1L;
		
					{
						add(new TransactionProduct(null, getHIVProduct(ELMISConstants.SCREENING.getValue()).getProductId(), BigDecimal.ONE, null));
					}
				};
				if(actionOrder == null){
					if(nodeProdDAO.selectTxnCountForNodeProductAfterDate(site.getId(), null, txnDate) > 0){
						Integer order = transctionService.getMaxActionOrderInADay(txnDate);//upto date instead of in date
						
						ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
						screeningTxnId = transctionService.performTransaction(site,screeningProducts, TransactionType.SCREENING_TEST, txnDate,userId , ao);
					} else {
						screeningTxnId = transctionService.performTransaction(site,screeningProducts, TransactionType.SCREENING_TEST, txnDate,userId);
					}
					
				} else {
					screeningTxnId = transctionService.performTransaction(site,screeningProducts, TransactionType.SCREENING_TEST, txnDate,userId , actionOrder);
				}
				// record screenig test
				hivTestingDAO.saveHIVTestStep(new HIVTestStep(null, screeningTxnId,ELMISConstants.SCREENING.getValue(),hivTestID , hivTest.getScreeningResult()));
				
				// is reactive?
				if (hivTest.getScreeningResult().equalsIgnoreCase(
						ELMISConstants.REACTIVE.getValue())) {
					
					List<TransactionProduct> confirmatoryProducts = new ArrayList<TransactionProduct>() {
						private static final long serialVersionUID = 1L;
		
						{
							add(new TransactionProduct(null, getHIVProduct(ELMISConstants.CONFIRMATORY.getValue()).getProductId(), BigDecimal.ONE, null));
						}
					};
					if(actionOrder == null){
						if(nodeProdDAO.selectTxnCountForNodeProductAfterDate(site.getId(), null, txnDate) > 0){
							Integer order = transctionService.getMaxActionOrderInADay(txnDate);
							ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
							confirmatoryTxnId = transctionService.performTransaction(site, confirmatoryProducts,TransactionType.CONFIRMATORY_TEST, txnDate, userId , ao);
						} else {
							confirmatoryTxnId = transctionService.performTransaction(site, confirmatoryProducts,TransactionType.CONFIRMATORY_TEST, txnDate, userId);
						}
					} else {
						if(actionOrder.getPrecedence().equals(ActionOrder.BEFORE)){
							actionOrder.setPrecedence(ActionOrder.AFTER);
							actionOrder.setNeignboringOrder(actionOrder.getNeignboringOrder());
						} else {
							actionOrder.setNeignboringOrder(actionOrder.getNeignboringOrder() + 1);
						}
						confirmatoryTxnId = transctionService.performTransaction(
								site, confirmatoryProducts,
								TransactionType.CONFIRMATORY_TEST, txnDate, userId);
					}
					hivTestingDAO.saveHIVTestStep(new HIVTestStep(null,confirmatoryTxnId,ELMISConstants.CONFIRMATORY.getValue(), hivTestID , hivTest.getConfirmatoryResult()));
				}
				return hivTestID;
		}
	}

	@Override
	public List<Constant> getHIVTestPurposes() {
		return new ArrayList<Constant>() {
			private static final long serialVersionUID = 1L;
			{
				add(ELMISConstants.PMTCT);
				add(ELMISConstants.VCT);
				add(ELMISConstants.QUALITY_CONTROL);
				add(ELMISConstants.CLINICAL_DIAGNOSIS);
				add(ELMISConstants.OTHER);
			}
		};
	}

	@Override
	public List<Constant> getHIVTestResults() {
		return new ArrayList<Constant>() {
			private static final long serialVersionUID = 1L;
			{
				add(ELMISConstants.REACTIVE);
				add(ELMISConstants.NON_REACTIVE);
				add(ELMISConstants.INVALID);
			}
		};
	}

	@Override
	public List<Constant> getHIVTestTypes() {
		return new ArrayList<Constant>() {
			private static final long serialVersionUID = 1L;
			{
				add(ELMISConstants.SCREENING);
				add(ELMISConstants.CONFIRMATORY);
			}
		};
	}
	
	@Override
	public HIVTestProduct getHIVProduct(String hivTestType){
		return hivTestingDAO.getHIVProduct(hivTestType);
	}
	
	@Override
	public HIVProductAvailabilityDTO checkAvailabilityOfProduct(String hivTestType, Integer nodeId){
		HIVTestProduct testProd = getHIVProduct(hivTestType);
		NodeProduct nodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, testProd.getProductId());
		Product prod = productService.getProductById(testProd.getProductId());
		
		if (nodeProd == null){
			return new HIVProductAvailabilityDTO(prod, null ,  null, false);
		}
		HIVProductAvailabilityDTO productDTO = new HIVProductAvailabilityDTO(prod, null , null , nodeProd.getQuantityOnHand().compareTo(BigDecimal.ONE) > 0);
		return productDTO;
	}
	
	@Override
	public List<HIVTestDARItem> getHIVDARItems(Date from , Date to , Integer nodeId){
		return hivTestingDAO.getHIVDARItems(from, to, nodeId);
	}
	
	@Override
	public HIVProductAvailabilityDTO getCurrentBeginningBalance(String hivTestType , Integer nodeId , Integer numberOfDays) throws UnavailableHIVTestProductException, UnavailablePhysicalCountException, UnavailableNodeProductException{ // numberOfDays - since when do you want too look for physical counts
		HIVTestProduct testProd = getHIVProduct(hivTestType);
		if(testProd == null){
			throw new UnavailableHIVTestProductException("No product is set for " + hivTestType.toLowerCase());
		}
		NodeProduct nodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, testProd.getProductId());
		if (nodeProd == null){
			throw new UnavailableNodeProductException("Product unavailable in node");
		}
		Product prod = productService.getProductById(testProd.getProductId());
		
		HIVProductAvailabilityDTO prodAvailabilityDTO = new HIVProductAvailabilityDTO();
		
//		BigDecimal lastPhyCount = hivTestingDAO.selectLastPhysicalCount(nodeId, prod.getId());
		PhysicalCount  pCount =  nodeProdDAO.selectPhysicalCount(nodeProd.getLatestPhysicalCountId());
		if(pCount == null){
			throw new UnavailablePhysicalCountException("No physical count for the previous day for " + prod.getPrimaryname());
		}
		BigDecimal lastPhyCount = pCount.getCountedQuantity();
		
		/*if(lastPhyCount == null){
			BigDecimal begBalanceFromTxnHistory = nodeProdDAO.getBeginningBalanceFromTransactionHistory(nodeId, prod.getId());
				if( begBalanceFromTxnHistory == null){
					prodAvailabilityDTO.setCurrentBeginningBalance(nodeProd.getQuantityOnHand());
				} else {
					prodAvailabilityDTO.setCurrentBeginningBalance(begBalanceFromTxnHistory);
				}
		} else {
			prodAvailabilityDTO.setCurrentBeginningBalance(lastPhyCount);
		}
		*/
		prodAvailabilityDTO.setCurrentBeginningBalance(lastPhyCount);
		prodAvailabilityDTO.setProduct(prod);
		return prodAvailabilityDTO;
	}
	
	@Override 
	public HIVTestDARResult generateHIVDAR(Date from, Date to , Integer nodeId) throws UnavailableHIVTestProductException, UnavailableNodeProductException, UnavailablePhysicalCountException{
		HIVTestDARResult result = new HIVTestDARResult();
		
		HIVTestProduct screeningHIVProd = getHIVProduct(ELMISConstants.SCREENING.getValue());
		if(screeningHIVProd == null){
			throw new UnavailableHIVTestProductException("No product is set for " + ELMISConstants.SCREENING.getDisplayName());
		}
		NodeProduct screeningNodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, screeningHIVProd.getProductId());
		if (screeningNodeProd == null){
			throw new UnavailableNodeProductException("Product unavailable in node");
		}
		Product screeningProd = productService.getProductById(screeningHIVProd.getProductId());

		
		HIVTestProduct confirmatoryHIVProd = getHIVProduct(ELMISConstants.CONFIRMATORY.getValue());
		if(confirmatoryHIVProd == null){
			throw new UnavailableHIVTestProductException("No product is set for " + ELMISConstants.CONFIRMATORY.getDisplayName());
		}
		NodeProduct confirmatoryNodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, confirmatoryHIVProd.getProductId());
		if (confirmatoryNodeProd == null){
			throw new UnavailableNodeProductException("Product unavailable in node");
		}
		Product confirmatoryProd = productService.getProductById(confirmatoryHIVProd.getProductId());
		
		result.setScreeningProductQtyReceived(storeDAO.getTotalProductReceipts(nodeId, screeningProd.getId(), DateCustomUtil.getBeginningOfDay(from), to));
		result.setScreeningBeginningBalance(selectMostRecentPC(
				storeService.selectMostRecentPhysicalCount(
						nodeId, 
						DateCustomUtil.addDays(from, -1), 
						DateCustomUtil.addDays(to, -1), 
						screeningProd.getCode())));
		PhysicalCount scrPC = nodeService.getLatestPhysicalCount(nodeId, screeningProd.getId());
		result.setScreeningProductLatestPhysicalCount((scrPC == null)?null:scrPC.getCountedQuantity());
		result.setScreeningProductTotalLnA(storeDAO.getTotalLossesAndAdjustments(nodeId, screeningProd.getId(), DateCustomUtil.getBeginningOfDay(from), to));

		result.setConfirmatoryProductQtyReceived(storeDAO.getTotalProductReceipts(nodeId, confirmatoryProd.getId(), DateCustomUtil.getBeginningOfDay(from), to));
		result.setConfirmatoryProductBeginningBalance(selectMostRecentPC(
				storeService.selectMostRecentPhysicalCount(
						nodeId, 
						DateCustomUtil.addDays(from, -1), 
						DateCustomUtil.addDays(to, -1),  
						confirmatoryProd.getCode())));
		PhysicalCount confPC = nodeService.getLatestPhysicalCount(nodeId, confirmatoryProd.getId());
		result.setConfirmatoryProductLatestPhysicalCount((confPC == null)?null:confPC.getCountedQuantity());
		result.setConfirmatoryProductTotalLnA(storeDAO.getTotalLossesAndAdjustments(nodeId, confirmatoryProd.getId(), DateCustomUtil.getBeginningOfDay(from), to));
		
		result.setDarItems(getHIVDARItems(from, to, nodeId));
		
		return result;
	}
	
	public BigDecimal selectMostRecentPC( List<StockControlCard> sccList){
		if(sccList == null || sccList.isEmpty()){
			return null;
		} else return sccList.get(0).getBalance();
	}
}
