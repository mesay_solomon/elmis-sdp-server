/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.NodeProductDAO;
import org.jsi.elmis.dao.StoreDAO;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductReceipt;
import org.jsi.elmis.model.StockRequisition;
import org.jsi.elmis.model.StockRequisitionTransaction;
import org.jsi.elmis.model.StockRequistionItem;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.rest.result.StockRequisitionResult;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@/**
 * @author Mesay S. Taye
*
*/
Component
public class DefaultStoreManagementService implements StoreManagementService {
	
	@Autowired
	TransactionService txnService;
	@Autowired
	NodeService nodeService;
	@Autowired
	ProductService productService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	StoreDAO storeDAO;
	@Autowired
	NodeProductDAO npDAO;
	
	@Override
	public int issue(Node store , Node dispensingPoint , List<TransactionProduct> productsIssued , Date txnDate , Integer stockRequestId ,Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException{
		Integer txnId = null;
		if(actionOrder == null){
			if(npDAO.selectTxnCountForNodeProductAfterDate(store.getId(), null, txnDate) > 0 || npDAO.selectTxnCountForNodeProductAfterDate(dispensingPoint.getId(), null, txnDate) > 0){
				Integer order = txnService.getMaxActionOrderInADay(txnDate);
				ActionOrder ao  = new ActionOrder(order , ActionOrder.AFTER);
				txnId = txnService.performTransaction(store, dispensingPoint, productsIssued, TransactionType.ISSUING, txnDate , userId , ao);
			} else {
				txnId = txnService.performTransaction(store, dispensingPoint, productsIssued, TransactionType.ISSUING, txnDate , userId);
			}
		} else {
			txnId = txnService.performTransaction(store, dispensingPoint, productsIssued, TransactionType.ISSUING, txnDate , userId , actionOrder); 
		}
		// if issuing is initiated by a stock rquest, update request status and link transaction id with request id
		if(stockRequestId != null){
			StockRequisitionTransaction stockRequisitionTxn = new StockRequisitionTransaction(null, stockRequestId, txnId);
			storeDAO.insertStockRequisitionTxn(stockRequisitionTxn);
			storeDAO.updateStockRequisitionStatus(stockRequestId, ELMISConstants.COMPLETED.getValue());
		}
		return txnId;
	}
	
	@Override
	public int transfer(Node supplier , Node recipient , List<TransactionProduct> productsTransferred , Date txnDate ,  Integer userId) throws UnavailableNodeProductException, InsufficientNodeProductException{
		return txnService.performTransaction(supplier, recipient, productsTransferred, TransactionType.TRANSFER, txnDate , userId);
	}
	
	@Override
	public int receive(Node store , List<TransactionProduct> productsReceived , Date txnDate ,  Integer userId , String dispatchNo , Integer sourceId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException{
		//record receipt
		Integer txnId = null;
		if(actionOrder == null){
			if(npDAO.selectTxnCountForNodeProductAfterDate(store.getId(), null, txnDate) > 0){
				Integer order = txnService.getMaxActionOrderInADay(txnDate);//upto date instead of in date
				actionOrder = new ActionOrder(order , ActionOrder.AFTER);
				txnId = txnService.performTransaction(store, productsReceived, TransactionType.RECEIVING, txnDate , userId , actionOrder);
			} else {
				txnId = txnService.performTransaction(store, productsReceived, TransactionType.RECEIVING, txnDate , userId);
			}
			} else {
				txnId = txnService.performTransaction(store, productsReceived, TransactionType.RECEIVING, txnDate , userId , actionOrder);
			}
				
		storeDAO.insertProductReceipt(new ProductReceipt(null, txnId, dispatchNo, sourceId));
		return txnId; //TODO: should you return receipt id instead?
	}
	
/*	@Override
	public intsetupInitialStockForNode(Node node , List<TransactionProduct> products , Date txnDate ,  Integer userId ) throws UnavailableNodeProductException, InsufficientNodeProductException{
		//record receipt
		Integer txnId = txnService.performTransaction(node, products, TransactionType.INITIAL_STOCK_SETUP, txnDate , userId);
		return txnId; 
	}*/
	
	@Override
	public int issueToOtherFacility(Node store ,  List<TransactionProduct> productsIssued , Date txnDate , Integer userId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException{
		Integer txnId = null; 
		if(actionOrder == null){
			if(npDAO.selectTxnCountForNodeProductAfterDate(store.getId(), null, txnDate) > 0 ){
				Integer order = txnService.getMaxActionOrderInADay(txnDate);//upto date instead of in date
				ActionOrder ao = new ActionOrder(order , ActionOrder.AFTER);
				txnId = txnService.performTransaction(store,  productsIssued, TransactionType.ISSUE_OUT, txnDate , userId , ao);
			} else {
				txnId = txnService.performTransaction(store,  productsIssued, TransactionType.ISSUE_OUT, txnDate , userId); 
			}
		} else {
			txnId = txnService.performTransaction(store,  productsIssued, TransactionType.ISSUE_OUT, txnDate , userId , actionOrder);
		}
		return txnId;
	}

	@Override
	public List<StockControlCard> generateStockControlCard(Node node, Date from,
			Date to , String productCode) {
		Product product = productService.getProductByCode(productCode);
		
		List<Map<String, Object>> sccMap = storeDAO.selectStockControlCardProducts(node.getId(), product.getId() , from ,to );
		List<StockControlCard> sccList = new ArrayList<StockControlCard>();
		
		BigDecimal currentBalance = npDAO.selectNodeProductByNodeandProduct(node.getId(), product.getId()).getQuantityOnHand();//TODO: could be null? Doesn't seem to be :)
		for (Map<String, Object> sMap : sccMap) {
			StockControlCard scc = new StockControlCard();
				scc.setDate((java.util.Date) sMap.get("date"));
				scc.setFromOrTo((String) sMap.get("from_or_to"));
				scc.setLossesNAdjustments( (BigDecimal) sMap.get(""));
				//scc.setBalance( (BigDecimal) sMap.get("balance"));
				scc.setRefNo((String) sMap.get("ref_no"));
				scc.setRemark((String) sMap.get(""));
				scc.setUser((String) sMap.get("username"));
				
				String txnType = (String) sMap.get("txn_type");
				Boolean txnIsPositive = (Boolean) sMap.get("txn_direction");	
				
				if(txnType.equalsIgnoreCase(TransactionType.RECEIVING)){
					scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
				} else if (txnType.equalsIgnoreCase(TransactionType.ISSUING)){
					
					if(txnIsPositive){
						scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
					} else {
						scc.setQtyIssued( (BigDecimal) sMap.get("txn_qty"));
					}
					//TODO: when viewed from the recipient's side, the issued quanity must appear under received column
				} else if (txnType.equalsIgnoreCase(TransactionType.NEGATIVE_ADJUSTMENT) || txnType.equalsIgnoreCase(TransactionType.POSITIVE_ADJUSTMENT)){
					scc.setLossesNAdjustments( (BigDecimal) sMap.get("txn_qty"));
				}
				if(txnIsPositive != null){
					if(txnIsPositive){
						scc.setBalance(currentBalance);
						currentBalance = currentBalance.subtract((BigDecimal) sMap.get("txn_qty"));
					} else {
						scc.setBalance(currentBalance);
						currentBalance = currentBalance.add((BigDecimal) sMap.get("txn_qty"));
					}
				} else {
					scc.setBalance( (BigDecimal) sMap.get("balance"));
				}
			sccList.add(scc);
		}
		return sccList;
	}
	
	//TODO: should be moved to node service, along with corresponsing modifications in the StoreController
	@Override
	public List<StockControlCard> selectMostRecentPhysicalCount(Integer nodeId, Date from,
			Date to , String productCode) {
		Product product = productService.getProductByCode(productCode);
		
		List<Map<String, Object>> sccMap = storeDAO.selectMostRecentActionOnNodeProduct(nodeId, product.getId() , from ,to );
		List<StockControlCard> sccList = new ArrayList<StockControlCard>();
		
		for (Map<String, Object> sMap : sccMap) {
			String txnType = (String) sMap.get("txn_type");
			if(txnType.equalsIgnoreCase(ELMISConstants.PHYSICAL_COUNT.getValue())){
			StockControlCard scc = new StockControlCard();

			    scc.setDate((java.util.Date) sMap.get("date"));
				scc.setFromOrTo((String) sMap.get("from_or_to"));
				scc.setLossesNAdjustments( (BigDecimal) sMap.get(""));
				scc.setBalance( (BigDecimal) sMap.get("balance"));
				scc.setRefNo((String) sMap.get("ref_no"));
				scc.setRemark((String) sMap.get(""));
				scc.setUser((String) sMap.get("username"));
				
				
				
				if(txnType.equalsIgnoreCase(TransactionType.RECEIVING)){
					scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
				} else if (txnType.equalsIgnoreCase(TransactionType.ISSUING)){

					Boolean txnIsPositive = (Boolean) sMap.get("txn_direction");	
					if(txnIsPositive){
						scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
					} else {
						scc.setQtyIssued( (BigDecimal) sMap.get("txn_qty"));
					}
				} else if (txnType.equalsIgnoreCase(TransactionType.NEGATIVE_ADJUSTMENT) || txnType.equalsIgnoreCase(TransactionType.POSITIVE_ADJUSTMENT)){
					scc.setLossesNAdjustments( (BigDecimal) sMap.get("txn_qty"));
				}
			sccList.add(scc);
			}
		}
		return sccList;
	}
	
	@Override
	public List<BigDecimal> selectMostRecentPC(Integer nodeId, Date from,
			Date to , String productCode) {
		Product product = productService.getProductByCode(productCode);
		
		List<Map<String, Object>> sccMap = storeDAO.selectMostRecentActionOnNodeProduct(nodeId, product.getId() , from ,to );
		List<BigDecimal> pcList = new ArrayList<BigDecimal>();
		
		for (Map<String, Object> sMap : sccMap) {
			String txnType = (String) sMap.get("txn_type");
			if(txnType.equalsIgnoreCase(ELMISConstants.PHYSICAL_COUNT.getValue())){
				pcList.add((BigDecimal) sMap.get("balance"));
			}
		}
		return pcList;
	}
	
	@Override
	public BigDecimal getTotalLnA(Node node, Date from,
			Date to , String productCode) {

		Product product = productService.getProductByCode(productCode);
		BigDecimal totalLnA = storeDAO.getTotalLossesAndAdjustments(node.getId(), product.getId() , from ,to );
		if(totalLnA == null) return BigDecimal.ZERO;
		return totalLnA;
	}
	
	@Override
	public BigDecimal getTotalProductReceipts(Node node, Date from,
			Date to , String productCode) {

		Product product = productService.getProductByCode(productCode);
		BigDecimal totalLnA = storeDAO.getTotalProductReceipts(node.getId(), product.getId() , from ,to );
		if(totalLnA == null) return BigDecimal.ZERO;
		return totalLnA;
	}

	@Override
	public int requestProducts(Integer requesterNodeId, Integer supplierNodeId,
			List<StockRequistionItem> stockRequistionItems, Date requestTime,
			Integer userId) {
		//insert 
		StockRequisition stockRequisition = new StockRequisition(null, requesterNodeId, supplierNodeId, requestTime, ELMISConstants.PENDING.getValue(), userId , null);
		Integer stockRequisitionId = storeDAO.insertStockRequisition(stockRequisition);
		for (StockRequistionItem stockRequistionItem : stockRequistionItems) {
			stockRequistionItem.setStockRequisitionId(stockRequisitionId);
			storeDAO.insertStockRequisitionItem(stockRequistionItem);
		}
		
		return stockRequisitionId;
	}
	
	@Override
	public int updateStockRequisitionStatus(Integer stockRequisitionId, String status) {
		//insert 
		storeDAO.updateStockRequisitionStatus(stockRequisitionId, status);
		return stockRequisitionId;
	}

	@Override
	public List<StockRequisitionResult> getStockRequestsBySupplierId(
			Integer supplierNodeId, Date afterTime) {
		return storeDAO.selectRequisitionBySupplierId(supplierNodeId, afterTime);
	}
	
	@Override
	public BigDecimal getFacilityPhysicalCount(Integer periodId , Integer productId) throws UnavailablePhysicalCountException{
		ProcessingPeriod period = miscService.getProcessingPeriod(periodId);
		List<NodeProduct> nodeProducts = npDAO.selectNodeProductsByProductId(productId);
		BigDecimal pcSum = BigDecimal.ZERO;
		for (NodeProduct nodeProduct : nodeProducts) {
			List<BigDecimal> pcList = selectMostRecentPC(nodeProduct.getNodeId() , period.getStartdate() , period.getEnddate() , nodeProduct.getProduct().getCode());
			if(pcList != null && !pcList.isEmpty()){
				pcSum = pcSum.add(pcList.get(0));
			} else {
				throw new UnavailablePhysicalCountException("Physical count missing for " + nodeProduct.getProduct().getPrimaryname() + " in " + nodeProduct.getNode().getName());
			}
		}
		return pcSum;
	}
	
	@Override
	public BigDecimal getBeginningBalance(Integer periodId , Integer productId){
		ProcessingPeriod period = miscService.getPrecedingProcessingPeriod(periodId);
		if(period == null) return BigDecimal.ZERO;

		BigDecimal beginningBalance;
		try {
			beginningBalance = getFacilityPhysicalCount(period.getId(), productId);
		} catch (UnavailablePhysicalCountException e) {
			// //TODO: what if previous physical count is unavailable
			return BigDecimal.ZERO;
		}
		return beginningBalance;
	}
}