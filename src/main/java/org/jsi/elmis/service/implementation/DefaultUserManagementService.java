package org.jsi.elmis.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.dao.RightDAO;
import org.jsi.elmis.dao.RoleDAO;
import org.jsi.elmis.dao.RoleRightDAO;
import org.jsi.elmis.dao.UserDAO;
import org.jsi.elmis.dao.UserNodeRoleDAO;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Right;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.RoleRight;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNodeRole;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;
import org.jsi.elmis.service.interfaces.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class DefaultUserManagementService implements UserManagementService{
	
	@Autowired
	UserNodeRoleDAO userNodeRoleDao;
	@Autowired
	RoleDAO roleDao;
	@Autowired
	UserDAO userDao;
	@Autowired
	RoleRightDAO roleRightDao;
	@Autowired
	RightDAO rightDao;

	@Override
	public Integer saveUserInfo(User user) {
		
		return userDao.saveUser(user);
	}
	@Override
	public User selectUserById(int id) {
		return userDao.selectUserById(id);
	}

	@Override
	public Integer updateUser(User user) {
		return userDao.updateUser(user);
	}
	
	@Override
	public Integer saveRole(Role role) {
		return roleDao.saveRole(role);
	}

	@Override
	public Integer assignUserNodeRoles(User user, Node node,
			List<Role> nodeRoles) {
		UserNodeRole userNodeRole = new UserNodeRole();
		userNodeRole.setUserId(user.getId());
		userNodeRole.setNodeId(node.getId());		
		return userNodeRoleDao.saveUserNodeRole(userNodeRole, nodeRoles);
	}
	@Override
	public Integer saveRoleRight(RoleRight roleRight) {
		
		return roleRightDao.saveRoleRight(roleRight);
	}
	@Override
	public List<Role> getRoles() {
		return roleDao.getRoles();
	}
	@Override
	public List<RoleRight> getRoleRights() {
		return roleRightDao.getRoleRights();
	}
	@Override
	public Integer updateRole(Role role) {
		
		return roleDao.updateRole(role);
	}
	@Override
	public Integer updateRoleRights(RoleRight roleRight) {
		
		return roleRightDao.updateRoleRights(roleRight);
	}
	@Override
	public Integer updateUserNodeRoles(User user, Node node,
			List<Role> nodeRoles, int userNodeRoleId) {
		UserNodeRole userNodeRole = new UserNodeRole();
		userNodeRole.setId(userNodeRoleId);
		userNodeRole.setUserId(user.getId());
		userNodeRole.setNodeId(node.getId());
		
		return userNodeRoleDao.updateUserNodeRole(userNodeRole, nodeRoles);
	}
	@Override
	public List<User> getUsers() {
		return userDao.getUsers();
	}
	@Override
	public List<Right> getRights() {
		return rightDao.getRights();
	}
	
	@Override
	public Integer saveRoleRights(List<RoleRight> roleRights) {
		for (RoleRight roleRight : roleRights) {
			roleRightDao.saveRoleRight(roleRight);
		}	
		return 1;
	}
	
	@Override
	public Integer deleteUser(int userId) {
		return userDao.deleteUser(userId);
	}
	@Override
	public Boolean insertUserNodeRoles(List<UserNodeRole> userNodeRoles) {
		for (UserNodeRole userNodeRole : userNodeRoles) {
			try{
				userNodeRoleDao.insertUserNodeRole(userNodeRole);
			}catch(Exception ex){
				//something has broken
				return false;
			}
		}
		return true;
	}
	@Override
	public ArrayList<UserNodeRoleRightResult> authenticate(User user) {
		return userDao.authenticate(user);
	}
	@Override
	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserId(
			Integer userId) {
		return userDao.getUserNodeRoleRights(userId);
	}
	@Override
	public Boolean revokeRolesFromUser(ArrayList<UserNodeRole> userNodeRoleList) {
		return userDao.revokeRolesFromUser(userNodeRoleList);
	}
	@Override
	public Boolean revokeNodesFromUser(ArrayList<UserNodeRole> userNodeRoleList) {
		return userDao.revokeNodesFromUser(userNodeRoleList);
	}
	@Override
	public Boolean revokeRightsFromRole(Integer roleId,
			ArrayList<String> rightList) {
		return userDao.revokeRightsFromRole(roleId, rightList);
	}
	@Override
	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserAndNode(
			UserNodeRole unr) {
		return userDao.getUserNodeRoleRightsByUserAndNode(unr);
	}
	@Override
	public List<RoleRight> getRoleRightsByRoleId(Integer roleId) {
		return roleRightDao.getRoleRightsByRoleId(roleId);
	}
	@Override
	public Integer deleteRole(int roleId) {
		return roleDao.deleteRole(roleId);
	}
	@Override
	public User selectUserByEmail(String email) {
		return userDao.selectUserByEmail(email);
	}

}
