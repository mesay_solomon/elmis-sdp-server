package org.jsi.elmis.service.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Right;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.RoleRight;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNodeRole;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;

/**
 * @author mesay
 *
 */
public interface UserManagementService {

	public List<User> getUsers();
	
	public Integer saveUserInfo(User user);
	
	public Integer updateUser(User user);
	
	public User selectUserById(int id);
	
	public User selectUserByEmail(String email);
	
	public Integer assignUserNodeRoles(User user , Node node , List<Role> nodeRoles);
	
	public Integer saveRole(Role role);
	
	public List<Role> getRoles();
	
	public Integer updateRole(Role role);
	
	public Integer saveRoleRight(RoleRight roleRight);
	
	public List<RoleRight> getRoleRights();
	
	public List<RoleRight> getRoleRightsByRoleId(Integer roleId);
	
	public Integer updateRoleRights(RoleRight roleRight);
	
	public Integer updateUserNodeRoles( User user , Node node , List<Role> nodeRoles, int userNodeRoleId);
	
	public List<Right> getRights();

	public Integer saveRoleRights(List<RoleRight> roleRights);

	public Integer deleteUser(int userId);
	
	public Integer deleteRole(int roleId);

	public Boolean insertUserNodeRoles(List<UserNodeRole> userNodeRoles);
	
	public ArrayList<UserNodeRoleRightResult> authenticate(User user);

	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserId(
			Integer userId);
	
	public Boolean revokeRightsFromRole(Integer roleId, ArrayList<String> rightList);
	
	public Boolean revokeRolesFromUser(ArrayList<UserNodeRole> userNodeRoles);
	
	public Boolean revokeNodesFromUser(ArrayList<UserNodeRole> userNodeRoles);

	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserAndNode(
			UserNodeRole unr);
	
	

}
