/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.NodeProgram;
import org.jsi.elmis.model.NodeType;
import org.jsi.elmis.model.PhysicalCount;
import org.jsi.elmis.model.PhysicalCountAdjustment;
import org.jsi.elmis.model.PhysicalCountProduct;
import org.jsi.elmis.model.ProcessingPeriod;

/**
 * @author Mesay S. Taye
 *
 */
public interface NodeService {
	
	public Integer doPhysicalCount(Integer nodeId, Integer productId,

			Double countedQuantity, Date date , Integer userId);

	public NodeProduct saveNodeProduct(NodeProduct nodeProduct);

	public Integer saveNode(Node node);
	
	public Integer updateNode(Node node);
	
	public Integer deleteNode(Integer nodeId);

	public NodeProduct findProductByNodeAndProduct(Integer nodeId, Integer productId);
	
	public List<NodeProduct> getFacilityApprovedNodeProducts(Integer nodeId , String programCode);

	public List<Node> getNodesByType(String type);

	public boolean doPhysicalCount(Integer nodeId, List<PhysicalCountProduct> pcProducts , Date date , Integer userId);

	public BigDecimal getBeginningBalanceFromTransactionHistory(Integer nodeId,
			Integer productId);
	
	public List<Node> getAllNodes();
	
	public Boolean saveNodePrograms(ArrayList<NodeProgram> nodePrograms);
	
	public Boolean updateNodePrograms(ArrayList<NodeProgram> nodePrograms);

	public Boolean nodeIsDispensingPointForProduct(Integer nodeId, Integer productId);

	public Boolean nodeIsDispensingPointForProgram(Integer nodeId, Integer programId);
	
	public List<Node> getNodesByProgram(Integer programId);
	
	public List<NodeProgram> getNodeProgramsByNode(Integer nodeId);
	
	public Boolean deleteNodePrograms(ArrayList<NodeProgram> nodePrograms);

	public PhysicalCount getLatestPhysicalCount(Integer nodeId,
			Integer productId) throws UnavailablePhysicalCountException, UnavailableNodeProductException;
	
	public PhysicalCount getEndOfDayPhysicalCount(Integer nodeId , Integer productId , Date date);
	
	public PhysicalCount getEndOfMonthPhysicalCount(Integer nodeId , Integer productId , ProcessingPeriod period);

	public List<NodeProduct> getNodeProductsPCNotCompletedFor(Integer periodId, String scheduleCode, String programCode);
	
	public List<NodeType> getNodeTypes();
	
	public Boolean deletePhysicalCount(Integer physicalCountId, Integer userId) throws UnavailableNodeProductException, InsufficientNodeProductException;
	
	public Boolean savePhysicalCountAdjustment(PhysicalCountAdjustment physicalCountAdjustment);

}
