package org.jsi.elmis.service.interfaces;


/**
 * @author mesay
 *
 */
public interface PropertiesService {
	
	public String getARTDefaultPrefix();

	public String getFacilityCode();

	public Boolean getSendRnRToHub();

}
