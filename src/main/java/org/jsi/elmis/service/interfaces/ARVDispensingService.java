/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.util.Date;
import java.util.List;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.ARVDispensation;
import org.jsi.elmis.model.ActionOrder;
import org.jsi.elmis.model.DosageFrequency;
import org.jsi.elmis.model.DosageUnit;
import org.jsi.elmis.model.FacilityClient;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.RegimenProductCombination;
import org.jsi.elmis.model.dto.ARVActivityRegister;
import org.jsi.elmis.model.dto.ARVDispensationLineItemDTO;
import org.jsi.elmis.model.dto.RegimenCombinationProductDTO;

/**
 * @author Mesay S. Taye
 *
 */
public interface ARVDispensingService {
	
	public Regimen getRegimenByARTNo(String artNo);
	
	public List<RegimenProductCombination> getRegimenProductCombos(Integer regimenId); 
	
	public List<RegimenCombinationProductDTO> getRegimenComboProducts(String artNo, Integer nodeId , Integer comboId);

	public Integer dispenseARV(Node node,List<ARVDispensationLineItemDTO> arvDispensationItems,Date dispensationDate, Integer userId, Integer productComboId , 
			Integer clientId , ActionOrder actionOrder) throws UnavailableNodeProductException, InsufficientNodeProductException;

	public FacilityClient getARVClinetByARTNOByART(String artNo);
	
	public FacilityClient getARVClinetByARTNOByNRC(String nrcNo);

	public List<FacilityClient> getARVClinetByARTNOByPropertyCombinations(Date from, Date to, String firstName, String lastName, String sex);

	public List<Regimen> getAllRegimens();
	
	public List<RegimenLine> getAllRegimenLines();

	public List<Regimen> getRegimensByLineId(Integer lineId);

	public RegimenProductCombination getLastDispensedComboForClient(String artNo);

	public Integer changeRegimen(FacilityClient client);

	public List<DosageUnit> getDosageUnits();

	public List<DosageFrequency> getDosageFrequencies();

	public Integer registerARVClient(FacilityClient arvClient);

	public Integer updateARVClient(FacilityClient arvClient);

	public ARVDispensation getPreviousDispensationForCombination(Integer comboId,
			String artNo);
	public ARVActivityRegister generateARVActivityRegister(Integer nodeId , Date from, Date to);

	public List<FacilityClient> getARVClientsExpectedToday();

	public Integer resetClientNextVisitDate(Integer clientId);
	
}
