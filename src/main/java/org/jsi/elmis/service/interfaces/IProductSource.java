package org.jsi.elmis.service.interfaces;

import java.util.List;

import org.jsi.elmis.model.ProductSource;

public interface IProductSource {
	

	
	public List<ProductSource> getAllProductSources(); 
	public Integer create(ProductSource prodSource);
	public Integer edit(ProductSource prodSource);
	public Integer delete(ProductSource request);
}
