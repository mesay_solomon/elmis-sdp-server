/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.UserMapper;
import org.jsi.elmis.model.RoleRight;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNodeRole;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class UserDAO extends DAO {


	public int saveUser(User user){
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		int result = 0;

		try {
			mapper.insert(user);
			result = user.getId();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public int updateUser(User user){
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		int result = 0;

		try {
			result = mapper.updateByPrimaryKey(user);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	public User selectUserById(Integer id){

		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		User user = null;

		try {
			user = mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}
	
	public List<User> getUsers(){

		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		List<User> users = null;

		try {
			users = mapper.getUsers();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return users;
	}

	public Integer deleteUser(int userId) {
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		Integer status = null;

		try {
			status = mapper.deleteByPrimaryKey(userId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return status;
	}

	public ArrayList<UserNodeRoleRightResult> authenticate(User userToAuthenticate) {
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		ArrayList<UserNodeRoleRightResult> listOfNodeRoleRights = null;

		try {
			listOfNodeRoleRights = mapper.authenticate(userToAuthenticate);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return listOfNodeRoleRights;
	}

	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRights(Integer userId) {
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		ArrayList<UserNodeRoleRightResult> listOfNodeRoleRights = null;

		try {
			listOfNodeRoleRights = mapper.getUserNodeRoleRights(userId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return listOfNodeRoleRights;
	}
	
	public Boolean revokeRightsFromRole(Integer roleId, ArrayList<String> rightList){
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		RoleRight revokedRoleRight = null;
		
		try {
			for (String rightName : rightList) {
				
				revokedRoleRight = new RoleRight();
				revokedRoleRight.setRoleid(roleId);
				revokedRoleRight.setRightname(rightName);
				
				mapper.revokeRightsFromRole(revokedRoleRight);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		return true;
	}
	
	public Boolean revokeRolesFromUser(ArrayList<UserNodeRole> userNodeRoleList){
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		
		try {
			for (UserNodeRole userNodeRole : userNodeRoleList) {
				mapper.revokeRolesFromUser(userNodeRole);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		return true;
	}
	
	public Boolean revokeNodesFromUser(ArrayList<UserNodeRole> userNodeRoleList){
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		
		try {
			for (UserNodeRole userNodeRole : userNodeRoleList) {
				mapper.revokeNodesFromUser(userNodeRole);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		return true;
	}

	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserAndNode(
			UserNodeRole unr) {
		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		ArrayList<UserNodeRoleRightResult> listOfNodeRoleRights = null;

		try {
			listOfNodeRoleRights = mapper.getUserNodeRoleRightsByUserAndNode(unr);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return listOfNodeRoleRights;
	}
	
	public User selectUserByEmail(String email){

		SqlSession session = getSqlMapper().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		User user = null;

		try {
			user = mapper.selectByEmail(email);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}

}
