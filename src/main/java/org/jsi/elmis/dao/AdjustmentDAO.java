/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.AdjustmentMapper;
import org.jsi.elmis.dao.mappers.LossAdjustmentTypeMapper;
import org.jsi.elmis.dao.mappers.NodeProductMapper;
import org.jsi.elmis.model.Adjustment;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.NodeProduct;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */

@Component
public class AdjustmentDAO extends DAO {

	public int insertAdjustment(Adjustment adjustment){
		
		SqlSession session = getSqlMapper().openSession();
		AdjustmentMapper mapper = session.getMapper(AdjustmentMapper.class);
		
		try {
			mapper.insert(adjustment);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return adjustment.getId();
		
	}
	
	public int updateNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		try {
			result = mapper.updateByPrimaryKeySelective(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public LossAdjustmentType findAdjustmentByName(String name){
		
		SqlSession session = getSqlMapper().openSession();
		LossAdjustmentTypeMapper mapper = session.getMapper(LossAdjustmentTypeMapper.class);
		LossAdjustmentType laType = null;
		
		try {
			laType = mapper.selectByName(name);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return laType;
		
	}
	
	
	public List<LossAdjustmentType> getAllAdjustmets(){
		
		SqlSession session = getSqlMapper().openSession();
		LossAdjustmentTypeMapper mapper = session.getMapper(LossAdjustmentTypeMapper.class);
		List<LossAdjustmentType> laTypes = null;
		
		try {
			laTypes = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return laTypes;
	}
	
	public Adjustment selectById(Integer id){
		
		SqlSession session = getSqlMapper().openSession();
		AdjustmentMapper mapper = session.getMapper(AdjustmentMapper.class);
		Adjustment result = null;
		try {
			result = mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public Boolean deleteById(Integer id){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		Boolean result = false;
		try {
			result = mapper.deleteByPrimaryKey(id) != 0;

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}