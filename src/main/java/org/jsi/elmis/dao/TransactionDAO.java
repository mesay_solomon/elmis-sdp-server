/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.NodeTransactionMapper;
import org.jsi.elmis.dao.mappers.ReversalTransactionMapper;
import org.jsi.elmis.dao.mappers.TransactionMapper;
import org.jsi.elmis.dao.mappers.TransactionProductMapper;
import org.jsi.elmis.dao.mappers.TransactionTypeMapper;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeTransaction;
import org.jsi.elmis.model.ReversalTransaction;
import org.jsi.elmis.model.Transaction;
import org.jsi.elmis.model.TransactionHistoryItem;
import org.jsi.elmis.model.TransactionProduct;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.model.dto.TransactionHistoryDTO;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class TransactionDAO extends DAO {

	public int saveTransaction(Transaction transaction){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(transaction);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public int saveNodeTransaction(NodeTransaction nodeTxn){
		SqlSession session = getSqlMapper().openSession();
		NodeTransactionMapper mapper = session.getMapper(NodeTransactionMapper.class);
		
		try {
			mapper.insert(nodeTxn);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return nodeTxn.getId();
	}
	
	public TransactionType getTransactionType(Integer id){
		SqlSession session = getSqlMapper().openSession();
		TransactionTypeMapper mapper = session.getMapper(TransactionTypeMapper.class);

		TransactionType txnType = null;
		
		try {
			txnType = mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return txnType;
	}
	
	public TransactionType getTransactionTypeByName(String name){
		SqlSession session = getSqlMapper().openSession();
		TransactionTypeMapper mapper = session.getMapper(TransactionTypeMapper.class);

		TransactionType txnType = null;
		
		try {
			txnType = mapper.selectByName(name);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return txnType;
	}

	public Transaction getTransaction(Integer transactionId) {
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);

		Transaction txn = null;
		
		try {
			txn = mapper.selectByPrimaryKey(transactionId);
			txn.setTxnProducts(getTransactionProducts(transactionId));

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txn;
	}
	
	public List<TransactionProduct> getTransactionProducts(Integer transactionId) {
		SqlSession session = getSqlMapper().openSession();
		TransactionProductMapper mapper = session.getMapper(TransactionProductMapper.class);

		List<TransactionProduct> txnProducts = null;
		
		try {
			txnProducts = mapper.selectByTxnID(transactionId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnProducts;
	}
	
	public List<Node> getNodesInTransaction(Integer transactionId) {
		SqlSession session = getSqlMapper().openSession();
		NodeTransactionMapper mapper = session.getMapper(NodeTransactionMapper.class);

		List<Node> nodes = null;
		
		try {
			nodes = mapper.selectNodesByTxnId(transactionId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodes;
	}
	
	public int saveReversalTransaction(ReversalTransaction revTxn){
		SqlSession session = getSqlMapper().openSession();
		ReversalTransactionMapper mapper = session.getMapper(ReversalTransactionMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(revTxn);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public List<TransactionHistoryItem> getTransactionsToReverse(
			Integer nodeId, Integer limit, Integer offset , String scheduleCode) {
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		List<TransactionHistoryItem> txnItems = null;
		List<Transaction> txns = null;
		
		try {
			txns = mapper.selectTransactionsForReversal(nodeId, limit, offset , scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		if(txns != null)
			txnItems = convertTxnsToHistoryItems(txns);
		return txnItems;
	}
	
	public Integer selectTransactionsForReversalCount(
			Integer nodeId , String scheduleCode){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		Integer noOfTxns = null;
		
		try {
			noOfTxns = mapper.selectTransactionsForReversalCount(nodeId, scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return noOfTxns;
		
	}
	
	public List<TransactionHistoryItem> convertTxnsToHistoryItems(List<Transaction> txns){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		List<TransactionHistoryItem> txnHistoryItems = new ArrayList<TransactionHistoryItem>();
		for (Transaction txn : txns) {
			TransactionType txnType = getTransactionType(txn.getTransactionType());
			if(txnType.getName().equals(TransactionType.SCREENING_TEST)){
				ArrayList<Transaction> historyItemTxns = new ArrayList<Transaction>();
				
				Transaction txnConf = mapper.selectConfirmatoryCounterpart(txn.getId());
				historyItemTxns.add(txn);
				historyItemTxns.add(txnConf);
				String description = getTransactionDescription(txn.getId());
				TransactionHistoryItem txnHistoryItem = new TransactionHistoryItem(txn.getTransactionTimestamp(),description,historyItemTxns);
				txnHistoryItems.add(txnHistoryItem);
				
				
				
			} else {
				
				ArrayList<Transaction> historyItemTxns = new ArrayList<Transaction>();
				historyItemTxns.add(txn);
				String description = getTransactionDescription(txn.getId());
				TransactionHistoryItem txnHistoryItem = new TransactionHistoryItem(txn.getTransactionTimestamp(),description,historyItemTxns);
				txnHistoryItems.add(txnHistoryItem);
				
			}
		}
		return txnHistoryItems;
	}

	public String getTransactionDescription(Integer txnId) {
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);

		String txnDesc = null;
		
		try {
			txnDesc = mapper.selectTransactionDescription(txnId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnDesc;
	}
	
	public Integer deleteAssociatedInformationWithReversedTxn(Integer txnId) {
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);

		Integer txnDesc = null;
		
		try {
			txnDesc = mapper.deleteAssocitedEntriesWithReversedTxn(txnId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return txnDesc;
	}
	
	public BigDecimal aggregatedTxnQtyByType(Integer productId,String txnType,Date startDate, Date endDate){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		BigDecimal sumOfQty = null;
		
		try {
			sumOfQty = mapper.aggregatedTxnQtyByType(productId, txnType, startDate, endDate);
	
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return sumOfQty;
	}
	
	public BigDecimal selectAveragePeriodicConsumption(Integer periodId,Integer productId,String scheduelCode,Integer noOfPeriods){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		BigDecimal sumOfQty = null;
		
		try {
			sumOfQty = mapper.selectAveragePeriodicConsumption(periodId, productId, scheduelCode, noOfPeriods);
	
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return sumOfQty;
	}
	
	
	
	public List<TransactionHistoryDTO> selectTransactionHistory(Integer nodeId,Integer productId,Date from){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		List<TransactionHistoryDTO> txnHistoryItems = null;
		
		try {
			txnHistoryItems = mapper.selectTransactionHistory(nodeId, productId, from);
	
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnHistoryItems;
	}
	
	public Integer maxActionOrderOnDate(Date date){
		SqlSession session = getSqlMapper().openSession();
		TransactionMapper mapper = session.getMapper(TransactionMapper.class);
		Integer order = 0;
		
		try {
			order = mapper.selectMaxActionOrderOnADate(date);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return order == null? 0: order;
	}
	
}
