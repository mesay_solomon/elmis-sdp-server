package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.RightMapper;
import org.jsi.elmis.model.Right;
import org.springframework.stereotype.Component;
/**
 * 
 * @author Mekbib
 *
 */
@Component
public class RightDAO extends DAO{
	
	public List<Right> getRights() {

		SqlSession session = getSqlMapper().openSession();
		RightMapper mapper = session.getMapper(RightMapper.class);
		List<Right> rights = null;

		try {
			rights = mapper.getRights();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rights;
	}

}
