/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.ARVDispensationLineItemMapper;
import org.jsi.elmis.dao.mappers.ARVDispensationMapper;
import org.jsi.elmis.dao.mappers.FacilityClientMapper;
import org.jsi.elmis.dao.mappers.RegimenLineMapper;
import org.jsi.elmis.dao.mappers.RegimenMapper;
import org.jsi.elmis.dao.mappers.RegimenProductCombinationMapper;
import org.jsi.elmis.model.ARVDispensation;
import org.jsi.elmis.model.ARVDispensationLineItem;
import org.jsi.elmis.model.FacilityClient;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.RegimenProductCombination;
import org.jsi.elmis.model.dto.ARVActivityRegisterItem;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ARVDispensingDAO extends DAO {
	
	public int saveDispensation(ARVDispensation arvDispensation){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);

		try {
			mapper.insert(arvDispensation);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvDispensation.getId();
	}
	
	public int updateFacilityClientRegimen(FacilityClient facilityClient){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		try {
			 mapper.updateByPrimaryKeySelective(facilityClient);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return facilityClient.getId();
	}
	
	public int saveDispensationItem(ARVDispensationLineItem arvDispensationItem){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationLineItemMapper mapper = session.getMapper(ARVDispensationLineItemMapper.class);
		try {
			 mapper.insert(arvDispensationItem);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvDispensationItem.getId();
	}
	
	public FacilityClient getARVClientByART(String artNo){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		FacilityClient arvClient = null;
		try {
			 arvClient = mapper.getFacilityClientByART(artNo);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvClient;
	}
	
	public FacilityClient getARVClientByNRC(String nrcNo){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		FacilityClient arvClient = null;
		try {
			 arvClient = mapper.getFacilityClientByNRC(nrcNo);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return arvClient;
	}
	
	public List<FacilityClient>  getARVClientExpectedToday(){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		List<FacilityClient> arvClients = null;
		try {
			 arvClients = mapper.selectClientsExpectedToday();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return arvClients;
	}
	
	public Integer saveFacilityClient(FacilityClient facilityClient){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		Integer clientId = null;
		try {
			 clientId = mapper.insert(facilityClient);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return facilityClient.getId();
	}
	
	public Integer updateFacilityClient(FacilityClient facilityClient){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		Integer clientId = null;
		try {
			 clientId = mapper.updateByPrimaryKeySelective(facilityClient);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return clientId;
	}
	
	public RegimenProductCombination getLastDispensedComboForClient(String artNo){
		SqlSession session = getSqlMapper().openSession();
		RegimenProductCombinationMapper mapper = session.getMapper(RegimenProductCombinationMapper.class);
		RegimenProductCombination comboId = null;
		try {
			 comboId = mapper.selectLastProductComboForClient(artNo);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return comboId;
	}
	
	
	
	public ARVDispensation selectPreviousDispensationForCombination(Integer comboId , String artNo){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		ARVDispensation disp = null;
		try {
			disp = mapper.selectPreviousDispensationForCombination(artNo, comboId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return disp;
	}
	
	public List<FacilityClient> getARVClientByPropertyCombinations(Date from , Date to , String firstName , String lastName , String sex){
		SqlSession session = getSqlMapper().openSession();
		FacilityClientMapper mapper = session.getMapper(FacilityClientMapper.class);
		List<FacilityClient> arvClient = null;
		try {
			 arvClient = mapper.selectByPropertyCombinations(from, to, "%"+firstName+"%", "%"+lastName+"%", sex);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return arvClient;
	}

	public List<Regimen> getAllRegimens() {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		List<Regimen> regimens = null;
		try {
			 regimens = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}
	
	public List<Regimen> getAllRegimensByLineID(Integer lineId) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		List<Regimen> regimens = null;
		try {
			 regimens = mapper.selectRegimensByLineId(lineId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}

	public List<RegimenLine> getAllRegimenLines() {
		SqlSession session = getSqlMapper().openSession();
		RegimenLineMapper mapper = session.getMapper(RegimenLineMapper.class);
		List<RegimenLine> regLines = null;
		try {
			 regLines = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regLines;
	}
	
	public List<ARVActivityRegisterItem> getActivityRegisterLineItems(Integer nodeId , Date from , Date to) {
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		List<ARVActivityRegisterItem> activityRegisterLineItems = null;
		try {
			activityRegisterLineItems = mapper.selectActivityRegisterItems(nodeId , from, to);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return activityRegisterLineItems;
	}
	
	public Integer resetClientNextVisitDate(Integer clientId){
		SqlSession session = getSqlMapper().openSession();
		ARVDispensationMapper mapper = session.getMapper(ARVDispensationMapper.class);
		Integer result = null;
		try {
			result = mapper.resetClientNextVisitDate(clientId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

}
