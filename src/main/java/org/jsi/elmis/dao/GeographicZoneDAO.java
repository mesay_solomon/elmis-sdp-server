/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.GeographicZoneMapper;
import org.jsi.elmis.model.GeographicZone;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class GeographicZoneDAO extends DAO {

	public void upsertGeographicZones(ArrayList<GeographicZone> geographicZones) {
		SqlSession session = getSqlMapper().openSession();
		GeographicZoneMapper mapper = session
				.getMapper(GeographicZoneMapper.class);
		try{
		for (GeographicZone geographicZone : geographicZones) {
			System.out.println(geographicZone.getName());

			GeographicZone zone = new GeographicZone();

			zone.setId(geographicZone.getId());
			zone.setCode(geographicZone.getCode());
			zone.setCatchmentpopulation(geographicZone.getCatchmentpopulation());
			zone.setName(geographicZone.getName());
			zone.setLevelid(geographicZone.getLevelid());
			zone.setParentid(geographicZone.getParentid());
			zone.setParentid(null);
			zone.setLatitude(geographicZone.getLatitude());
			zone.setLongitude(geographicZone.getLongitude());

			mapper.upsertByCode(zone);

		}

		

		for (GeographicZone geographicZone : geographicZones) {
			 mapper.upsertByCode(geographicZone);
		}
		session.commit();
		} finally {
			//session.close();
		}
		
	}

	public List<GeographicZone> getAllGeographicZones() {
		List<GeographicZone> result;
		
		SqlSession session = getSqlMapper().openSession();
		GeographicZoneMapper mapper = session
				.getMapper(GeographicZoneMapper.class);
		try {
			result = mapper.selectAllGeographicZones();
		}catch(Exception ex){
			result = null;
		}finally {
			session.close();
		}
		
		return result;
	}
}
