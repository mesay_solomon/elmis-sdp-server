/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.NodeMapper;
import org.jsi.elmis.dao.mappers.NodeProductMapper;
import org.jsi.elmis.dao.mappers.NodeProgramMapper;
import org.jsi.elmis.dao.mappers.PhysicalCountMapper;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.NodeProgram;
import org.jsi.elmis.model.PhysicalCount;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class NodeProductDAO extends DAO{

/*	public int saveNodeProduct(NodeProduct nodeProduct){
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(nodeProduct);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}*/
	
	public NodeProduct saveNodeProduct(NodeProduct nodeProduct){
		NodeProduct existingNodeProd = null;
       if (nodeProduct.getProductId() != null && nodeProduct.getNodeId() != null){
    	   existingNodeProd = selectNodeProductByNodeandProduct(nodeProduct.getNodeId(), nodeProduct.getProductId());
    	   if (existingNodeProd != null) nodeProduct.setId(existingNodeProd.getId());
		} else if (nodeProduct.getId() != null){
			existingNodeProd = selectNodeProductByPrimaryKey(nodeProduct.getId());
		}

		if(existingNodeProd != null){
			updateNodeProduct(nodeProduct);
		} else {
			insertNodeProduct(nodeProduct);
		}
		return nodeProduct;
	}
	
	public NodeProduct selectNodeProductByNodeandProduct(Integer nodeId, Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByProductIdandNodeID(nodeId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;
		
	}
	
	public int insertNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int updateNodeProduct(NodeProduct nodeProduct){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKeySelective(nodeProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int updateFacilityProductPhysicalCount(Integer periodId, String productCode){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateFacilityPhysicalCountForProduct(periodId, productCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public NodeProduct selectNodeProductByPrimaryKey(Integer nodeProductId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		NodeProduct nodeProduct = null;
		
		try {
			nodeProduct = mapper.selectByPrimaryKey(nodeProductId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProduct;
		
	}
	
	
	
	public Integer selectTxnCountForNodeProductAfterDate(Integer nodeId, Integer productId , Date txnDate){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		Integer txnCount = null;
		
		try {
			txnCount = mapper.selectTxnCountForNodeProductAfterDate(nodeId, productId ,txnDate);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return txnCount;
		
	}
	
	public List<NodeProduct> selectNodeProducts(Integer nodeId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByNodeId(nodeId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}
	
	public List<NodeProduct> selectNodeProductsByProductId(Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByProductId(productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}
	
	
	public List<NodeProduct> selectNodeProducts(String programCode){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectNodeProductsInProgram(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
		
	}
	
	public List<NodeProduct> selectNodeProducts(Integer nodeId , String programCode , String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		List<NodeProduct> nodeProducts = null;
		
		try {
			nodeProducts = mapper.selectByNodeIdAndProgram(nodeId, programCode, facilityCode);//TODO: remove hardcoded fcility code

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProducts;
	}
	
	public BigDecimal getBeginningBalanceFromTransactionHistory(Integer nodeId , Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		NodeProductMapper mapper = session.getMapper(NodeProductMapper.class);
		BigDecimal beginningBalance = null;
		
		try {
			beginningBalance = mapper.selectBeginningQtyFromTransactionHistory(nodeId, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return beginningBalance;
	}

	public Boolean nodeIsDispensingPointForProgram(Integer nodeId, Integer programId) {
		SqlSession session = getSqlMapper().openSession();
		NodeProgramMapper mapper = session.getMapper(NodeProgramMapper.class);
		NodeProgram nodeProgram = null;
		
		try {
			nodeProgram = mapper.selectNodeProgram(nodeId, programId , true);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return nodeProgram != null;
	}
	
	public Boolean nodeIsDispensingPointForProduct(Integer nodeId, Integer productId) {
		return null;
	}
	
	public PhysicalCount selectPhysicalCount(Integer physicalCountId) {
		SqlSession session = getSqlMapper().openSession();
		PhysicalCountMapper mapper = session.getMapper(PhysicalCountMapper.class);
		PhysicalCount pCount = null;
		
		try {
			pCount = mapper.selectByPrimaryKey(physicalCountId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return pCount;
	}

	
	public BigDecimal getFacilityStockOnHand(Integer productId) {
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		BigDecimal stockOnHand = null;
		
		try {
			stockOnHand = mapper.selectFacilityStockOnHand(productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return stockOnHand;
	}


}
