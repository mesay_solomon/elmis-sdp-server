package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.ProductReceipt;

public interface ProductReceiptMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductReceipt record);

    int insertSelective(ProductReceipt record);

    ProductReceipt selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductReceipt record);

    int updateByPrimaryKey(ProductReceipt record);
}