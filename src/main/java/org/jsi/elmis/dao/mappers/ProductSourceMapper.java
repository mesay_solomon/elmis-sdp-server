package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.jsi.elmis.model.ProductSource;

public interface ProductSourceMapper {
	
	
	
	 int insert(ProductSource record);
	 
	 int updateByPrimaryKey( ProductSource record);
	 
	 int deleteByPrimaryKey(ProductSource record);
	
	ProductSource selectProductsourceById(Integer progId);
	
	List<ProductSource> selectAllProductsources();
	
	
	

}
