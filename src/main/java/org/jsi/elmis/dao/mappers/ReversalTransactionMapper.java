package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.ReversalTransaction;

public interface ReversalTransactionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ReversalTransaction record);

    int insertSelective(ReversalTransaction record);

    ReversalTransaction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ReversalTransaction record);

    int updateByPrimaryKey(ReversalTransaction record);
}