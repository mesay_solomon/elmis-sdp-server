package org.jsi.elmis.dao.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.ARVDispensation;
import org.jsi.elmis.model.dto.ARVActivityRegisterItem;

public interface ARVDispensationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ARVDispensation record);

    int insertSelective(ARVDispensation record);

    ARVDispensation selectByPrimaryKey(Integer id);
    
    ARVDispensation selectPreviousDispensationForCombination(@Param("artNo")String artNo ,@Param("comboId") Integer comboId);

    int updateByPrimaryKeySelective(ARVDispensation record);

    int updateByPrimaryKey(ARVDispensation record);

	List<ARVActivityRegisterItem> selectActivityRegisterItems(@Param("nodeId")Integer nodeId,
			@Param("from")Date from, @Param("to")Date to);
	
	int resetClientNextVisitDate(Integer clientId);
}