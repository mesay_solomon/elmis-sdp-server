package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.ProductPeriodStatus;

public interface ProductPeriodStatusMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductPeriodStatus record);

    int insertSelective(ProductPeriodStatus record);

    ProductPeriodStatus selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductPeriodStatus record);

    int updateByPrimaryKey(ProductPeriodStatus record);
    
  	ProductPeriodStatus selectByProductAndPeriod(@Param("periodId")Integer periodId,
			@Param("productId")Integer productId);
  	
  	List<ProductPeriodStatus> selectPreviousConsumptions(@Param("productId")Integer productId , @Param("scheduleCode")String scheduleCode);
  	
  	//assumes that physical counts will be equal to stock-on-hands after 
  	//facilitiy-wide physical count is completed
  	int updateFacilityProgramProductPhysicalCounts(@Param("programCode") String programCode, @Param("periodId") Integer periodId);
}