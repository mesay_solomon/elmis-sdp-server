package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.FacilityType;

public interface FacilityTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FacilityType facilityType);

    int insertSelective(FacilityType facilityType);

    FacilityType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FacilityType facilityType);

    int updateByPrimaryKey(FacilityType facilityType);
    
    int upsertByCode(FacilityType facilityType);
}