package org.jsi.elmis.dao.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.ProductPeriodStatusAdjustment;

public interface ProductPeriodStatusAdjustmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductPeriodStatusAdjustment record);

    int insertSelective(ProductPeriodStatusAdjustment record);

    ProductPeriodStatusAdjustment selectByPrimaryKey(Integer id);
    
    ProductPeriodStatusAdjustment selectByPeriodStatusId(@Param("prodPeriodStatusId")Integer prodPeriodStatusId , @Param("adjustmentType")String adjustmentType);
    
    List<ProductPeriodStatusAdjustment> selectAdjustmentsByPeriodStatusId(@Param("prodPeriodStatusId")Integer prodPeriodStatusId);

    int updateByPrimaryKeySelective(ProductPeriodStatusAdjustment record);

    int updateByPrimaryKey(ProductPeriodStatusAdjustment record);

	List<ProductPeriodStatusAdjustment> selectFacilityStockAdjustments(
			@Param("startDate")Date startDate, @Param("endDate") Date endDate,  @Param("productId")Integer productId);
}