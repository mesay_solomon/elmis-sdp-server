package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.ReportAndRequisition;

public interface ReportAndRequisitionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ReportAndRequisition record);

    int insertSelective(ReportAndRequisition record);

    ReportAndRequisition selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ReportAndRequisition record);

    int updateByPrimaryKey(ReportAndRequisition record);
}