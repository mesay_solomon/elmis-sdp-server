package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.StockRequisitionTransaction;

public interface StockRequisitionTransactionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StockRequisitionTransaction record);

    int insertSelective(StockRequisitionTransaction record);

    StockRequisitionTransaction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StockRequisitionTransaction record);

    int updateByPrimaryKey(StockRequisitionTransaction record);
}