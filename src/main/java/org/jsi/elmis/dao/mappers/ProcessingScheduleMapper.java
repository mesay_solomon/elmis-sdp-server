package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.ProcessingSchedule;

public interface ProcessingScheduleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProcessingSchedule record);

    int insertSelective(ProcessingSchedule record);

    ProcessingSchedule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProcessingSchedule record);

    int updateByPrimaryKey(ProcessingSchedule record);

	int upsertByCode(ProcessingSchedule processingSchedule);
}