package org.jsi.elmis.dao.mappers;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.HIVTestProduct;

public interface HIVTestProductMapper {
    int deleteByPrimaryKey(Integer productId);

    int insert(HIVTestProduct record);

    int insertSelective(HIVTestProduct record);

    HIVTestProduct selectByPrimaryKey(Integer productId);
    
    HIVTestProduct selectByTestType(@Param("testType")String testType);
    
    BigDecimal selectLastPhysicalCount(@Param("productId") Integer productId,@Param("nodeId") Integer nodeId);

    int updateByPrimaryKeySelective(HIVTestProduct record);

    int updateByPrimaryKey(HIVTestProduct record);
}