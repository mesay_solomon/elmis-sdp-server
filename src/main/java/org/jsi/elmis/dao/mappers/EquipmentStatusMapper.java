package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.EquipmentStatus;

public interface EquipmentStatusMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(EquipmentStatus record);

    int insertSelective(EquipmentStatus record);

    EquipmentStatus selectByPrimaryKey(Integer id);
    
    List<EquipmentStatus> selectAllEquipmentsCurrentStatus(@Param("periodId") Integer periodId);
    
    int updateByPrimaryKeySelective(EquipmentStatus record);

    int updateByPrimaryKey(EquipmentStatus record);
    
    int upsertByPrimaryKey(EquipmentStatus record);
}