package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.ProductPeriodStockedOutPeriod;

public interface ProductPeriodStockedOutPeriodMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductPeriodStockedOutPeriod record);

    int insertSelective(ProductPeriodStockedOutPeriod record);

    ProductPeriodStockedOutPeriod selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductPeriodStockedOutPeriod record);

    int updateByPrimaryKey(ProductPeriodStockedOutPeriod record);
}