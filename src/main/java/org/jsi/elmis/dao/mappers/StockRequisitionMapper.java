package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.jsi.elmis.model.StockRequisition;
import org.jsi.elmis.rest.result.StockRequisitionResult;

public interface StockRequisitionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StockRequisition record);

    int insertSelective(StockRequisition record);

    StockRequisition selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StockRequisition record);

    int updateByPrimaryKey(StockRequisition record);
    
    List<StockRequisitionResult> selectStockRequisitions(StockRequisition stockRequisition);
}