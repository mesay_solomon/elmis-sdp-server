package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.HIVTestStep;

public interface HIVTestStepMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HIVTestStep record);

    int insertSelective(HIVTestStep record);

    HIVTestStep selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HIVTestStep record);

    int updateByPrimaryKey(HIVTestStep record);
}