package org.jsi.elmis.dao.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.HIVTest;
import org.jsi.elmis.model.dto.HIVTestDARItem;

public interface HIVTestMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HIVTest record);

    int insertSelective(HIVTest record);

    HIVTest selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HIVTest record);
    
    List<HIVTestDARItem> selectHIVTestsInRange(@Param("from") Date from, @Param("to") Date to , @Param("nodeId")Integer nodeId);

    int updateByPrimaryKey(HIVTest record);
}