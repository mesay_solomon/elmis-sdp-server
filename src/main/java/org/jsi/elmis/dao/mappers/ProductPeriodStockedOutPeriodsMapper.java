package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.ProductPeriodStockedOutPeriods;

public interface ProductPeriodStockedOutPeriodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductPeriodStockedOutPeriods record);

    int insertSelective(ProductPeriodStockedOutPeriods record);

    ProductPeriodStockedOutPeriods selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductPeriodStockedOutPeriods record);

    int updateByPrimaryKey(ProductPeriodStockedOutPeriods record);
}