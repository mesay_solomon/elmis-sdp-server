package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.jsi.elmis.model.PhysicalCountAdjustment;

public interface PhysicalCountAdjustmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PhysicalCountAdjustment record);

    int insertSelective(PhysicalCountAdjustment record);

    PhysicalCountAdjustment selectByPrimaryKey(Integer id);
    
    List<PhysicalCountAdjustment> selectAllByPhysicalCountId(Integer physicalCountId);

    int updateByPrimaryKeySelective(PhysicalCountAdjustment record);

    int updateByPrimaryKey(PhysicalCountAdjustment record);
}