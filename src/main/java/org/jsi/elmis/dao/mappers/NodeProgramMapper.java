package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.NodeProgram;

public interface NodeProgramMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(NodeProgram record);

    int insertSelective(NodeProgram record);

    NodeProgram selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NodeProgram record);

    int updateByPrimaryKey(NodeProgram record);

	NodeProgram selectNodeProgram(@Param("nodeId")Integer nodeId,@Param("programId") Integer programId,@Param("isDispensingPoint") boolean isDispensingPoint);
	
	List<NodeProgram> selectNodeProgramsByNodeId(Integer nodeId);
}