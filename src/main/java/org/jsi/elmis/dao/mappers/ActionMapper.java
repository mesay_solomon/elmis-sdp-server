package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.Action;

public interface ActionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Action record);

    int insertSelective(Action record);

    Action selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Action record);

    int updateByPrimaryKey(Action record);
    
    int selectNextOrdinalNumber();
    
    int updateActionIncreaseOrder(@Param("order") Integer order);
    
    List<Integer> selectActionOrderAfterAction(@Param("nodeId")Integer nodeId,@Param("productId")Integer productId,@Param("order")Integer order);
}