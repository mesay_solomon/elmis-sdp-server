/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao.mappers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.Node;

/** 
 * @author Mesay S. Taye
 *
 */
public interface NodeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Node record);

    int insertSelective(Node record);

    Node selectByPrimaryKey(Integer id);
    
    List<Node> selectByType(String type);

    int updateByPrimaryKeySelective(Node record);

    int updateByPrimaryKey(Node record);
    
    List<Map<String , Object>> selectStockControlCardTransactions(@Param("nodeId")Integer nodeId , @Param("productId")Integer productId 
    		,@Param("from") Date from, @Param("to") Date to);
    
    List<Map<String , Object>> selectMostRecentActionOnNodeProduct(@Param("nodeId")Integer nodeId , @Param("productId")Integer productId 
    		,@Param("from") Date from, @Param("to") Date to);
    
    BigDecimal selectTotalLossesAndAdjustments(@Param("nodeId")Integer nodeId , @Param("productId")Integer productId 
    		,@Param("from") Date from, @Param("to") Date to);
    
    List<Node> getAllNodes();
    
    List<Node> getNodesByProgram(Integer programId);

	BigDecimal selectTotalProductReceipts(@Param("nodeId")Integer nodeId, @Param("productId")Integer productId,
			@Param("from")Date from, @Param("to")Date to);
	
	BigDecimal selectFacilityStockOnHand(@Param("productId") Integer productId);
	
	BigDecimal selectStockOnHandBeforeAction(@Param("productId")Integer productId , @Param("nodeId")Integer nodeId , @Param("order")Integer order);
	
	BigDecimal selectStockOnHandAfterAction(@Param("productId")Integer productId , @Param("nodeId")Integer nodeId , @Param("order")Integer order);
}