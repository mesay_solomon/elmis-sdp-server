package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.StockRequistionItem;

public interface StockRequistionItemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StockRequistionItem record);

    int insertSelective(StockRequistionItem record);

    StockRequistionItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StockRequistionItem record);

    int updateByPrimaryKey(StockRequistionItem record);
}