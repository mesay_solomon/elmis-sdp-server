package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.LabTest;

public interface LabTestMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LabTest record);

    int insertSelective(LabTest record);

    LabTest selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LabTest record);

    int updateByPrimaryKey(LabTest record);
    
    int upsertByPrimaryKey(LabTest record);
}