package org.jsi.elmis.dao.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.EquipmentReagent;
import org.jsi.elmis.model.Product;

public interface EquipmentReagentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(EquipmentReagent record);

    int insertSelective(EquipmentReagent record);

    EquipmentReagent selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EquipmentReagent record);

    int updateByPrimaryKey(EquipmentReagent record);

    List<Product> getEquipmentReagents(@Param("equipmentId")Integer equipmentId);
}