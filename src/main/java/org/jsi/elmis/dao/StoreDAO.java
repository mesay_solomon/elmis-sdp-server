/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.NodeMapper;
import org.jsi.elmis.dao.mappers.ProductReceiptMapper;
import org.jsi.elmis.dao.mappers.StockRequisitionMapper;
import org.jsi.elmis.dao.mappers.StockRequisitionTransactionMapper;
import org.jsi.elmis.dao.mappers.StockRequistionItemMapper;
import org.jsi.elmis.model.ProductReceipt;
import org.jsi.elmis.model.StockRequisition;
import org.jsi.elmis.model.StockRequisitionTransaction;
import org.jsi.elmis.model.StockRequistionItem;
import org.jsi.elmis.rest.result.StockRequisitionResult;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class StoreDAO extends DAO {
	
	public List<Map<String, Object>> selectStockControlCardProducts(Integer nodeId, Integer productId , Date from , Date to){
		
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		
		List<Map<String, Object>> sccMap = null;
		
		try {
			
			sccMap = mapper.selectStockControlCardTransactions(nodeId, productId , from , to);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return sccMap;
		
	}
	
	public List<Map<String, Object>> selectMostRecentActionOnNodeProduct(Integer nodeId, Integer productId , Date from , Date to){
		
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		
		List<Map<String, Object>> sccMap = null;
		
		try {
			
			sccMap = mapper.selectMostRecentActionOnNodeProduct(nodeId, productId , from , to);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return sccMap;
		
	}
	
	public BigDecimal getTotalLossesAndAdjustments(Integer nodeId, Integer productId , Date from , Date to){
		
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		
		BigDecimal totalLnA = null;
		
		try {
			
			totalLnA = mapper.selectTotalLossesAndAdjustments(nodeId, productId , from , to);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return totalLnA;
		
	}
	
	public BigDecimal getTotalProductReceipts(Integer nodeId, Integer productId , Date from , Date to){
		
		SqlSession session = getSqlMapper().openSession();
		NodeMapper mapper = session.getMapper(NodeMapper.class);
		
		BigDecimal totalLnA = null;
		
		try {
			
			totalLnA = mapper.selectTotalProductReceipts(nodeId, productId , from , to);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		
		return totalLnA;
		
	}
	
	
	public int insertStockRequisition(StockRequisition stockRequisition){
		SqlSession session = getSqlMapper().openSession();
		StockRequisitionMapper mapper = session.getMapper(StockRequisitionMapper.class);
		try {
			mapper.insert(stockRequisition);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return stockRequisition.getId();
	}
	
	public int insertProductReceipt(ProductReceipt productReceipt){
		SqlSession session = getSqlMapper().openSession();
		ProductReceiptMapper mapper = session.getMapper(ProductReceiptMapper.class);
		try {
			mapper.insert(productReceipt);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return productReceipt.getId();
	}
	
	public int insertStockRequisitionTxn(StockRequisitionTransaction stockRequisitionTxn){
		SqlSession session = getSqlMapper().openSession();
		StockRequisitionTransactionMapper mapper = session.getMapper(StockRequisitionTransactionMapper.class);
		try {
			mapper.insert(stockRequisitionTxn);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return stockRequisitionTxn.getId();
	}
	
	public int insertStockRequisitionItem(StockRequistionItem stockRequisitionItem){
		SqlSession session = getSqlMapper().openSession();
		StockRequistionItemMapper mapper = session.getMapper(StockRequistionItemMapper.class);
		try {
			mapper.insert(stockRequisitionItem);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return stockRequisitionItem.getId();
	}
	
	public int updateStockRequisitionStatus(Integer stockRequistionId , String status){
		SqlSession session = getSqlMapper().openSession();
		StockRequisitionMapper mapper = session.getMapper(StockRequisitionMapper.class);
		StockRequisition stockRequisition = new StockRequisition();
		stockRequisition.setId(stockRequistionId);
		stockRequisition.setStatus(status);
		try {
			
			mapper.updateByPrimaryKeySelective(stockRequisition);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return stockRequisition.getId();
	}
	
	public List<StockRequisitionResult> selectRequisitionBySupplierId(Integer supplierNodeId, Date afterTime){
		SqlSession session = getSqlMapper().openSession();
		StockRequisitionMapper mapper = session.getMapper(StockRequisitionMapper.class);
		List<StockRequisitionResult> result = null;
		
		StockRequisition stockRequisition = new StockRequisition();
		stockRequisition.setSupplierId(supplierNodeId);
		stockRequisition.setTime(afterTime);
		try {
			
			result = mapper.selectStockRequisitions(stockRequisition);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

}
