/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.FacilityApprovedProductMapper;
import org.jsi.elmis.dao.mappers.ProcessingPeriodMapper;
import org.jsi.elmis.dao.mappers.ProcessingScheduleMapper;
import org.jsi.elmis.dao.mappers.ProductCategoryMapper;
import org.jsi.elmis.dao.mappers.ProductMapper;
import org.jsi.elmis.dao.mappers.ProgramMapper;
import org.jsi.elmis.dao.mappers.ProgramProductMapper;
import org.jsi.elmis.model.FacilityApprovedProduct;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProcessingSchedule;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductCategory;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.ProgramProduct;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ProcessingPeriodDAO extends DAO {
	

	public void upsertProcessingSchedules(
			ArrayList<ProcessingSchedule> schedules) {
	
	SqlSession session = getSqlMapper().openSession();
	ProcessingScheduleMapper mapper = session
			.getMapper(ProcessingScheduleMapper.class);
	try {
		for (int i = 0; i < schedules.size(); i++) {
			System.out.println(schedules.get(i).getName());
			// mapper.updateByPrimaryKey(products.get(i));
			mapper.upsertByCode(schedules.get(i));

		}
		session.commit();
	} finally {
		session.close();
	}
		
	}

	/**
	 * 
	 */
	public void deleteProcessingSchedules() {
		// TODO Auto-generated method stub
		
	}


	public void upsertPeriodByDateRange(
			ArrayList<ProcessingPeriod> processingPeriods) {
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session
				.getMapper(ProcessingPeriodMapper.class);
		try {
			for (int i = 0; i < processingPeriods.size(); i++) {
				System.out.println(processingPeriods.get(i).getName());
				// mapper.updateByPrimaryKey(products.get(i));
				mapper.upsertByDateRange(processingPeriods.get(i));

			}
			mapper.upadteProcessingSchedules();
			session.commit();
		} finally {
			session.close();
		}
		
		
	}
	
	public ProcessingPeriod getProcessingPeriod(Integer periodId ){
		
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;
		
		try {
			period = mapper.selectByPrimaryKey(periodId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}
	
	public ProcessingPeriod getLatestPeriodRnRisSubmittedOrSkippedFor(String programCode ){
		
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;
		
		try {
			period = mapper.selectLatestPeriodRnRisSubmittedOrSkippedFor(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}

	public ProcessingPeriod getPrecedingProcessingPeriod(Integer periodId) {
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;
		
		try {
			period = mapper.selectPrecedingPeriod(periodId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}
}
