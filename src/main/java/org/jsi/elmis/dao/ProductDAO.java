/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.FacilityApprovedProductMapper;
import org.jsi.elmis.dao.mappers.ProductCategoryMapper;
import org.jsi.elmis.dao.mappers.ProductMapper;
import org.jsi.elmis.dao.mappers.ProgramMapper;
import org.jsi.elmis.dao.mappers.ProgramProductMapper;
import org.jsi.elmis.model.FacilityApprovedProduct;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductCategory;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.ProgramProduct;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ProductDAO extends DAO {
	
	public Product selectProductByCode(String code){
		
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session.getMapper(ProductMapper.class);
		Product product = null;
		
		try {
			product = mapper.selectByCode(code);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return product;
		
	}
	
	public Product selectProductById(Integer id){
		
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session.getMapper(ProductMapper.class);
		Product product = null;
		
		try {
			product = mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return product;
		
	}
	
	public List<FacilityApprovedProduct> getFacilityApprovedProducts(String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session.getMapper(FacilityApprovedProductMapper.class);
		List<FacilityApprovedProduct>  facilityApprovedProducts = null;
		
		try {
			facilityApprovedProducts = mapper.selectAll(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityApprovedProducts;
		
	}
/*	
	public List<FacilityApprovedProduct> getFacilityApprovedProducts(String programCode){
		
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session.getMapper(FacilityApprovedProductMapper.class);
		List<FacilityApprovedProduct>  facilityApprovedProducts = null;
		
		try {
			facilityApprovedProducts = mapper.selectByProgram(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityApprovedProducts;
		
	}
	*/
	public List<ProgramProduct> getProgramProduct(){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	public List<ProgramProduct> getProductsInProgram(String programCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectProductsInAProgram(programCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	public List<ProgramProduct> getFacilityApprovedProductsInProgram(String programCode,String facilityCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		List<ProgramProduct>  programProducts = null;
		
		try {
			programProducts = mapper.selectFacilityApprovedProductsInAProgram(programCode,facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programProducts;
	}
	
	
	public List<Program> getProductPrograms(Integer productId){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		List<Program>  programs = null;
		
		try {
			programs = mapper.selectProductPrograms(productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programs;
	}
	
	public void upsertProductCategories(ArrayList<ProductCategory> categories){
		SqlSession session = getSqlMapper().openSession();
		ProductCategoryMapper mapper = session
				.getMapper(ProductCategoryMapper.class);
		try {
			for (int i = 0; i < categories.size(); i++) {
				System.out.println(categories.get(i).getCode());
				// mapper.updateByPrimaryKey(products.get(i));
				mapper.upsertByCode(categories.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}
		
	}

	public void upsertProducts(ArrayList<Product> products) {
		SqlSession session = getSqlMapper().openSession();
		ProductMapper mapper = session
				.getMapper(ProductMapper.class);
		try {
			for (int i = 0; i < products.size(); i++) {
				System.out.println(products.get(i).getCode());
				// mapper.updateByPrimaryKey(products.get(i));
				mapper.upsertByCode(products.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}	
		
	}
	
	public List<ProductCategory> getProductCategories(){
		
		SqlSession session = getSqlMapper().openSession();
		ProductCategoryMapper mapper = session.getMapper(ProductCategoryMapper.class);
		List<ProductCategory>  productCategories = null;
		
		try {
			productCategories = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return productCategories;
	}
}
