/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.ReportAndRequisitionMapper;
import org.jsi.elmis.model.ReportAndRequisition;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class RnRDAO extends DAO {

	public int saveRnR(ReportAndRequisition rnr){
		SqlSession session = getSqlMapper().openSession();
		ReportAndRequisitionMapper mapper = session.getMapper(ReportAndRequisitionMapper.class);
		
		try {
			 mapper.insert(rnr);

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return (rnr != null)?rnr.getId():0;
	}
}
