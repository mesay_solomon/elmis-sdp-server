/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.EquipmentMapper;
import org.jsi.elmis.dao.mappers.EquipmentReagentMapper;
import org.jsi.elmis.dao.mappers.EquipmentStatusMapper;
import org.jsi.elmis.dao.mappers.LabTestMapper;
import org.jsi.elmis.model.Equipment;
import org.jsi.elmis.model.EquipmentStatus;
import org.jsi.elmis.model.LabTest;
import org.jsi.elmis.model.Product;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class LABEquipmentsDAO extends DAO {
	public List<EquipmentStatus> getEquipmentsStatus(Integer periodId){
		SqlSession session = getSqlMapper().openSession();
		EquipmentStatusMapper mapper = session.getMapper(EquipmentStatusMapper.class);
		List<EquipmentStatus> equipmentsStatus = null;
		try {
			equipmentsStatus = mapper.selectAllEquipmentsCurrentStatus(periodId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return equipmentsStatus;
	}
	
	public int upsertEquipmentStatus(EquipmentStatus equipmentStatus){
		SqlSession session = getSqlMapper().openSession();
		EquipmentStatusMapper mapper = session.getMapper(EquipmentStatusMapper.class);
		try {
			mapper.upsertByPrimaryKey(equipmentStatus);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return equipmentStatus.getId();
	}
	
	public int upsertLabTest(LabTest labTest){
		
		SqlSession session = getSqlMapper().openSession();
		LabTestMapper mapper = session.getMapper(LabTestMapper.class);
		try {
			mapper.upsertByPrimaryKey(labTest);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return 0;
	}

	/**
	 * @return
	 */
	public List<Product> getEquipmentReagents(Integer equipmentId) {
		SqlSession session = getSqlMapper().openSession();
		EquipmentReagentMapper mapper = session.getMapper(EquipmentReagentMapper.class);
		List<Product> reagents = null;
		try {
			reagents = mapper.getEquipmentReagents(equipmentId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return reagents;
	}

	/**
	 * @return
	 */
	public List<Equipment> getAllEquipments() {
		SqlSession session = getSqlMapper().openSession();
		EquipmentMapper mapper = session.getMapper(EquipmentMapper.class);
		List<Equipment> equipments = null;
		try {
			equipments = mapper.selectAll();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return equipments;
	}

	/**
	 * @param equipments
	 */
	public void updateEquipment(Equipment equipment) {
		SqlSession session = getSqlMapper().openSession();
		EquipmentMapper mapper = session.getMapper(EquipmentMapper.class);
		try {
			mapper.updateByPrimaryKeySelective(equipment);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
}
