package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.RoleRightMapper;
import org.jsi.elmis.model.RoleRight;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Michael Mwebaze
 *
 */
@Component
public class RoleRightDAO extends DAO{

	public int saveRoleRight(RoleRight roleRight){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		int result = 0;

		try {
			result = mapper.insert(roleRight);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public List<RoleRight> getRoleRights(){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		List<RoleRight> roleRightList = null;

		try {
			roleRightList = mapper.getRoleRights();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return roleRightList; 
	}
	
	public List<RoleRight> getRoleRightsByRoleId(Integer roleId){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		List<RoleRight> roleRightList = null;

		try {
			roleRightList = mapper.getRoleRightsByRoleId(roleId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return roleRightList; 
	}
	
	public int updateRoleRights(RoleRight roleRight){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		int result = 0;

		try {
			result = mapper.update(roleRight);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}