package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.AdjustmentMapper;
import org.jsi.elmis.dao.mappers.LossAdjustmentTypeMapper;
import org.jsi.elmis.dao.mappers.NodeProductMapper;
import org.jsi.elmis.dao.mappers.ProductSourceMapper;
import org.jsi.elmis.model.ProductSource;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.NodeProduct;
import org.springframework.stereotype.Component;

@Component
public class ProductSourceDAO extends DAO {
	
	private ProductSourceDAO(){
		super();
	};
	
	private static ProductSourceDAO prodsrcDAO = null;
	
	public static ProductSourceDAO getInstance(){
		if(prodsrcDAO == null){
			prodsrcDAO = new ProductSourceDAO();
		} 
		return prodsrcDAO;
	}

	public int insertProductSource(ProductSource prodSource){
		
		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(prodSource);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	
public int editProductSource(ProductSource prodSource){
		
		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKey(prodSource);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}



public int deleteProductSource(ProductSource prodSource){
	
	SqlSession session = getSqlMapper().openSession();
	ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
	int result = 0;
	
	try {
		result = mapper.deleteByPrimaryKey(prodSource);

	} catch (Exception ex) {
		ex.printStackTrace();
	} finally {
		session.commit();
		session.close();
	}
	return result;
	
}


	
	public List<ProductSource> getAllProductSources(){
		
		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		List<ProductSource> prodsources = null;
		
		try {
			prodsources = mapper.selectAllProductsources();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodsources;
		
	}
	

}
