/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.HIVTestMapper;
import org.jsi.elmis.dao.mappers.HIVTestProductMapper;
import org.jsi.elmis.dao.mappers.HIVTestStepMapper;
import org.jsi.elmis.model.HIVTest;
import org.jsi.elmis.model.HIVTestProduct;
import org.jsi.elmis.model.HIVTestStep;
import org.jsi.elmis.model.dto.HIVTestDARItem;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class HIVTestingDAO extends DAO {
	
	public int saveHIVTest(HIVTest hivTest){
		SqlSession session = getSqlMapper().openSession();
		HIVTestMapper mapper = session.getMapper(HIVTestMapper.class);

		try {
			mapper.insert(hivTest);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return hivTest.getId();
	}
	
	public int saveHIVTestStep(HIVTestStep hivTestStep){
		SqlSession session = getSqlMapper().openSession();
		HIVTestStepMapper mapper = session.getMapper(HIVTestStepMapper.class);

		try {
			mapper.insert(hivTestStep);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return hivTestStep.getId();
	}
	
	public HIVTestProduct getHIVProduct(String hivTestType){
		SqlSession session = getSqlMapper().openSession();
		HIVTestProductMapper mapper = session.getMapper(HIVTestProductMapper.class);
		HIVTestProduct product = null;

		try {
			product = mapper.selectByTestType(hivTestType);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return product;
	}
	
	public List<HIVTestDARItem> getHIVDARItems(Date from , Date to , Integer nodeId){
		SqlSession session = getSqlMapper().openSession();
		HIVTestMapper mapper = session.getMapper(HIVTestMapper.class);
		List<HIVTestDARItem> DARItems = null;

		try {
			DARItems = mapper.selectHIVTestsInRange(from, to, nodeId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return DARItems;
	}
	
	public BigDecimal selectLastPhysicalCount(Integer nodeId , Integer productId){
		SqlSession session = getSqlMapper().openSession();
		HIVTestProductMapper mapper = session.getMapper(HIVTestProductMapper.class);
		BigDecimal previousPhysicalcount = null;

		try {
			previousPhysicalcount = mapper.selectLastPhysicalCount(productId, nodeId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return previousPhysicalcount;
	}
}
