/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.NodeProductTransactionMapper;
import org.jsi.elmis.dao.mappers.TransactionProductMapper;
import org.jsi.elmis.model.NodeProductTransaction;
import org.jsi.elmis.model.TransactionProduct;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class TransactionProductDAO extends DAO {

	public int saveTransactionProduct(TransactionProduct txnProduct) {
		SqlSession session = getSqlMapper().openSession();
		TransactionProductMapper mapper = session
				.getMapper(TransactionProductMapper.class);
		int result = 0;

		try {
			result = mapper.insert(txnProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	
	public int saveNodeProductTransaction( NodeProductTransaction npTxn) {
		SqlSession session = getSqlMapper().openSession();
		NodeProductTransactionMapper mapper = session
				.getMapper(NodeProductTransactionMapper.class);

		try {
			mapper.insert(npTxn);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return npTxn.getId();
	}
}
