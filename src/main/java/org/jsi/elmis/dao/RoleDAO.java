package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.RoleMapper;
import org.jsi.elmis.model.Role;
import org.springframework.stereotype.Component;
/**
 * 
 * @author Michael Mwebaze
 *
 */
@Component
public class RoleDAO extends DAO{

	public int saveRole(Role role){
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		int result = 0;

		try {
			result = mapper.insert(role);
			result = role.getId();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public List<Role> getRoles(){
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		List<Role> rolesList = null;

		try {
			rolesList = mapper.getRoles();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rolesList;
	}
	
	public int updateRole(Role role){
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		int result = 0;

		try {
			result = mapper.updateByPrimaryKey(role);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public Integer deleteRole(int roleId) {
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		int result = 0;

		try {
			result = mapper.deleteByPrimaryKey(roleId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}
