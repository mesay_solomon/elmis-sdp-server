/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.FacilityMapper;
import org.jsi.elmis.dao.mappers.ProcessingPeriodMapper;
import org.jsi.elmis.dao.mappers.ProgramMapper;
import org.jsi.elmis.dao.mappers.TransactionTypeMapper;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class CommonDAO extends DAO {
	public List<Program> getAllPrograms( ){
		
		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		List<Program> programs = null;
		
		try {
			programs = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programs;
	}
	
	public List<Facility> getAllFacilities(Integer geoZoneId ){
		
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		List<Facility> facilities = null;
		
		try {
			facilities = mapper.selectAllByGeoZoneId(geoZoneId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilities;
	}
	

	public ProcessingPeriod getCurrentPeriod(String scheduleCode ){
		
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;
		
		try {
			period = mapper.selectCurrentPeriod(scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}
	
	public ProcessingPeriod getPreviousPeriod(String scheduleCode ){
		
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;
		
		try {
			period = mapper.selectPreviousPeriod(scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}
	
	public FacilityDistrictAndProvinceResult selectDistrictAndProvince(String facilityCode){
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		FacilityDistrictAndProvinceResult facilityDistrictAndProvince = null;
		
		try {
			facilityDistrictAndProvince = mapper.selectDistrictAndProvince(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityDistrictAndProvince;
	}
	
	public List<ProcessingPeriod> getAllProcessingPeriods(String scheduleCode){
		
		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		List<ProcessingPeriod> result = null;
		
		try {
			result = mapper.getAllProcessingPeriods(scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public Facility getFacilityById(Integer facilityId) {
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		Facility facility = null;
		
		try {
			facility = mapper.selectByPrimaryKey(facilityId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facility;
	}

	public List<TransactionType> getTransactionTypes() {
		SqlSession session = getSqlMapper().openSession();
		TransactionTypeMapper mapper = session.getMapper(TransactionTypeMapper.class);
		List<TransactionType> result = null;
		
		try {
			result = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}



}
