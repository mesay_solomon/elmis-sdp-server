package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.UserNodeRoleMapper;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.UserNodeRole;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Michael Mwebaze
 *
 */
@Component
public class UserNodeRoleDAO extends DAO{
	
	public int saveUserNodeRole(UserNodeRole userNodeRole, List<Role> nodeRoles){
		SqlSession session = getSqlMapper().openSession();
		UserNodeRoleMapper mapper = session.getMapper(UserNodeRoleMapper.class);
		int result = 0;
		
		try {
			for (Role role : nodeRoles){
				userNodeRole.setRoleId(role.getId());
			result = mapper.insert(userNodeRole);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public int updateUserNodeRole(UserNodeRole userNodeRole,
			List<Role> nodeRoles) {
		SqlSession session = getSqlMapper().openSession();
		UserNodeRoleMapper mapper = session.getMapper(UserNodeRoleMapper.class);
		int result = 0;
		
		try {
			for (Role role : nodeRoles){
				userNodeRole.setRoleId(role.getId());
			result = mapper.updateByPrimaryKey(userNodeRole);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	public int insertUserNodeRole(UserNodeRole userNodeRole){
		SqlSession session = getSqlMapper().openSession();
		UserNodeRoleMapper mapper = session.getMapper(UserNodeRoleMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(userNodeRole);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}
