/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rnr;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RnRLineItem implements Cloneable {
	 	private String productCode;
	    private Integer beginningBalance;
	    private Integer quantityReceived;
	    private Integer quantityDispensed;
	    
	    private Integer stockInHand;
	    private Integer stockOutDays;
	    private Integer newPatientCount;
	    private Integer quantityRequested;
	    private String reasonForRequestedQuantity;

	    private Integer amc;
	    private Integer normalizedConsumption;
	    private Integer calculatedOrderQuantity;
	    private Integer maxStockQuantity;

	    private Integer quantityApproved;
	    private String remarks;
	    private List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<>();
}