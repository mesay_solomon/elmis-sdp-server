/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.common.constants;

/**
 * @author Mesay S. Taye
 *
 */
public class ELMISConstants {
	
	//generic
	public static final Constant SUCCESSFUL = new Constant("SUCCESSFUL" , "Successful");
	public static final Constant FAILED = new Constant("FAILED" , "Failed");
	//hiv test results
	public static final Constant REACTIVE = new Constant("REACTIVE" , "Reactive");
	public static final Constant NON_REACTIVE = new Constant("NON_REACTIVE" , "Non-reactive");
	public static final Constant INDETERMINATE = new Constant("INDETERMINATE" , "Indeterminate");
	public static final Constant INVALID = new Constant("INVALID" , "Invalid");
	
	//hiv test purposes
	public static final Constant PMTCT = new Constant("PMTCT" , "PMTCT");
	public static final Constant VCT = new Constant("VCT" , "VCT");
	public static final Constant QUALITY_CONTROL = new Constant("QUALITY_CONTROL" , "Quality Control");
	public static final Constant CLINICAL_DIAGNOSIS = new Constant("CLINICAL_DIAGNOSIS" , "Clinical Diagnosis");
	public static final Constant OTHER = new Constant("OTHER" , "Other");
	
	//hiv test types
	public static final Constant SCREENING = new Constant("SCREENING" , "Screening");
	public static final Constant CONFIRMATORY = new Constant("CONFIRMATORY" , "Confirmatory");
	
	//rnr
	public static final Integer AMC_NUMBER_OF_MONTHS = 3;
	public static final Constant SERVICE_SDP_REQUISITION = new Constant("sdp-requisitions" , "SDP Requisition");
	public static final Constant RNR_SUBMISSION_SUCCEEDED = new Constant("sdp-requisitions" , "SDP Requisition");
	public static final Constant RNR_SUBMISSION_FAILED = new Constant("sdp-requisitions" , "SDP Requisition");
	public static final Constant RNR_SUBMISSION_URL = new Constant("sdp-requisitions" , "SDP Requisition");
	
	//stock requisitions
	public static final Constant PENDING = new Constant("PENDING", "Pending");
	public static final Constant COMPLETED = new Constant("COMPLETED", "Completed");
	public static final Constant REJECTED = new Constant("REJECTED", "Rejected");
	
	public static final Constant PHYSICAL_COUNT = new Constant("PHYSICAL_COUNT", "Physical Count");
	public static final Constant TRANSACTION = new Constant("TRANSACTION", "Transaction");
	
	public static final Constant ARV = new Constant("ARV", "ARV");
	
	//action precendence
	public static final String BEFORE = "BEFORE";
	public static final String AFTER = "AFTER";

}
