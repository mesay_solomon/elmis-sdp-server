package org.jsi.elmis.configuration;

import java.io.Serializable;
import java.util.List;

import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.User;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Michael Mwebaze
 *
 */
public class AssignUserNodeRoleRequestObject implements Serializable{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private Node node;
	private User user;
	private List<Role> roleList;
	public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Role> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
}
