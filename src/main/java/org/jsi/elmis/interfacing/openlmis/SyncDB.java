/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.interfacing.openlmis;

import java.util.ArrayList;

import org.jsi.elmis.common.util.Connectivity;
import org.jsi.elmis.dao.FacilityDAO;
import org.jsi.elmis.dao.GeographicLevelDAO;
import org.jsi.elmis.dao.GeographicZoneDAO;
import org.jsi.elmis.dao.ProcessingPeriodDAO;
import org.jsi.elmis.dao.ProductDAO;
import org.jsi.elmis.dao.ProgramDAO;
import org.jsi.elmis.dao.ProgramProductDAO;
import org.jsi.elmis.dao.RegimenDAO;
import org.jsi.elmis.model.DosageFrequency;
import org.jsi.elmis.model.DosageUnit;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.FacilityApprovedProduct;
import org.jsi.elmis.model.FacilityType;
import org.jsi.elmis.model.GeographicLevel;
import org.jsi.elmis.model.GeographicZone;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProcessingSchedule;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductCategory;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenCombinationProduct;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.RegimenProductCombination;
import org.jsi.elmis.model.RegimenProductDosage;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SyncDB  implements Job{
	
	@Autowired
	ProgramDAO programDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	GeographicLevelDAO geoLevelDAO;
	@Autowired
	GeographicZoneDAO geoZoneDAO;
	@Autowired
	FacilityDAO facilityDAO;
	@Autowired
	ProcessingPeriodDAO periodDAO;
	@Autowired
	ProgramProductDAO progProdDAO;
	@Autowired
	RegimenDAO regimenDAO;
	
	@Value("${elmis.central.url}")
	private  String eLMISCentralURL;
	
	
	
	private ArrayList<GeographicZone> geographicZones = null;
	private ArrayList<GeographicLevel> geographicLevels = null;
	private ArrayList<FacilityType> facilityTypes = null;
	private ArrayList<Facility> facilities = null;
	private ArrayList<ProcessingSchedule> schedules = null;
	private ArrayList<Product> products = null;
	private ArrayList<ProgramProduct> programProducts = null;
	private ArrayList<FacilityApprovedProduct> faciltyApprovedProducts = null;
	private ArrayList<ProductCategory> categories = null;
	private ArrayList<Program> programs = null;
	private ArrayList<ProcessingPeriod> processingPeriods = null;
	
	private ArrayList<RegimenLine> regimenLines = null;
	private ArrayList<Regimen> regimens = null;
	private ArrayList<RegimenProductCombination> regimenProductCombinations = null;
	private ArrayList<RegimenCombinationProduct> regimenCombinationProducts = null;
	private ArrayList<RegimenProductDosage> regimenProductDosages = null;
	private ArrayList<DosageUnit> dosageUnits = null;
	private ArrayList<DosageFrequency> dosageFrequencies = null;
	
    @SuppressWarnings("unused")
	private ArrayList<LossAdjustmentType> lossesAndAdjustmentTypes= null;
    
    @Autowired
    LookupFactory lookupFactory;
    

	public SyncRequestResult syncFull() {
		// new ExampleApplication().insertProductCategory();

		// new ExampleApplication().testInsertAllProducts();

		if(Connectivity.isReachable(eLMISCentralURL)){
			this.fetchSyncData();			
			
			this.upsertPrograms();
			this.upsertProductCategory();
			this.upsertProducts();
			this.upsertFacilityTypes();
			this.deleteFacilityApprovedProducts();
			this.deleteProgramProducts();
			this.insertProgramProducts();
			
			
			this.upsertGeographicLevels();
			this.upsertGeographicZones();
			this.upsertFacilities();
			
			this.insertFacilityApprovedProducts();
			
//			this.upsertProcessingSchedule();
			this.upsertPeriodsByDateRange();			

            /*sync.deleteLossesAndAdjustmentTypes();
            sync.insertLossesAndAdjustmentTypes();*/

			this.upsertDosageUnits();
			this.upsertDosageFrequencies();
			this.upsertRegimenLines();
			this.upsertRegimens();
			this.upsertRegimenProductCombinations();
			this.upsertRegimenCombinationProducts();
			this.upsertRegimenProductDosages();
			
			
			System.out.println("Sync completed!");
			return new SyncRequestResult(true, "Synchronization completed!");
		} else {
			System.out.println("Sync not completed!");
			return new SyncRequestResult(false, "Unable to reach eLMIS server! Please check your connection and try again!");
		}
	}
	


	public Boolean fetchSyncData(){
		Boolean fetchIsSuccessful = false;
		
			geographicZones = lookupFactory.getGeographicZones();
			facilityTypes = lookupFactory.getFacilityTypes();
			facilities = lookupFactory.getFacilities();
			schedules = lookupFactory.getProcessingSchedules();
			products = lookupFactory.getProducts();
			programProducts = lookupFactory.getProgramProducts();
			faciltyApprovedProducts = lookupFactory.getFacilityApprovedProducts();
			categories = lookupFactory.getProductCategories();
			programs = lookupFactory.getPrograms();
			processingPeriods = lookupFactory.getProcessingPeriods();
			geographicLevels = lookupFactory.getGeographicLevels();
            lossesAndAdjustmentTypes = lookupFactory.getLossesAndAdjustmentTypes();
            
            
            dosageUnits = lookupFactory.getDosageUnits();
            dosageFrequencies = lookupFactory.getDosageFrequencies();
            regimenLines = lookupFactory.getRegimenLines();
            regimens = lookupFactory.getRegimens();
            regimenProductCombinations = lookupFactory.getRegimenProductCombinations();
            regimenCombinationProducts = lookupFactory.getRegimenCombinationProducts();
            regimenProductDosages = lookupFactory.getRegimenProductDosages();
            
            
			fetchIsSuccessful = true;
			
		return fetchIsSuccessful;
	}
	
	private void upsertPeriodsByDateRange() {
		periodDAO.upsertPeriodByDateRange(processingPeriods);
	}

	
	public void insertProgramProducts() {

		progProdDAO.insertProgramProducts(programProducts);
	}
	
	public void upsertGeographicZones() {
		
		geoZoneDAO.upsertGeographicZones(geographicZones);
	}

	public void upsertGeographicLevels() {

		geoLevelDAO.upsertGeographicLevels(geographicLevels);
	}


	public void upsertFacilityTypes() {

		facilityDAO.upsertFacilityTypes(facilityTypes);
	}

	public void upsertFacilities() {

		facilityDAO.upsertFacilities(facilities);

	}

	public void upsertProcessingSchedule() {

		periodDAO.upsertProcessingSchedules(schedules);

	}
	
	public void deleteProcessingSchedules(){
		periodDAO.deleteProcessingSchedules();
	}

	public void deleteProgramProducts() {
		progProdDAO.deleteProgramProducts();
	}

	public void deleteFacilityApprovedProducts() {
		progProdDAO.deleteFacilityApprovedProduct();
	}

	public void insertFacilityApprovedProducts() {

		progProdDAO.insertFacilityApprovedProduct(faciltyApprovedProducts);

	}


	public void insertPeriods() {
	}
	
	public void deletePeriods(){
	}



	private void upsertProducts() {
		productDAO.upsertProducts(products);
	}

	/**
	 * 
	 */
	
	private void upsertProductCategory() {
		
		productDAO.upsertProductCategories(categories);
		
	}
	
	public void upsertPrograms() {
		programDAO.upsertPrograms(programs );
	}
	
	
	private void upsertRegimenLines() {
		regimenDAO.upsertRegimenLines(regimenLines);
	}
	
	private void upsertRegimens() {
		regimenDAO.upsertRegimens(regimens);
	}
	
	private void upsertRegimenProductCombinations() {
		regimenDAO.upsertRegimenProductCombinations(regimenProductCombinations);
	}
	
	private void upsertRegimenCombinationProducts() {
		regimenDAO.upsertRegimenCombinationProducts(regimenCombinationProducts);
	}
	
	private void upsertRegimenProductDosages() {
		regimenDAO.upsertRegimenProductDosages(regimenProductDosages);
		
	}
	
	private void upsertDosageUnits() {
		regimenDAO.upsertDosageUnits(dosageUnits);
	}
	
	private void upsertDosageFrequencies(){
		regimenDAO.upsertDosageFrequencies(dosageFrequencies);
	}
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		
	}

}