package org.jsi.elmis.interfacing.openlmis;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.jsi.elmis.model.ProcessingPeriod;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

@Component
public class BaseFactory {
	@Value("${elmis.central.url}")
	private String BaseURL;
	@Value("${elmis.central.password}")
	private String Authorization;
	@Value("${elmis.central.approver.name}")
	private String UserName;
	
	@Value("${elmis.hub.url}")
	private String hubURL;
	@Value("${elmis.rnr.send.to.hub}")
	private Boolean sendRnRToHub;
	
	private static final String ARV = "ARV";

    public static Long FacilityID = 1L;

    private void parseHostAndPort(){
      // this assumes that the base URL contains the protocol and all
      HttpClient.PROTOCOL = BaseURL.substring(0, BaseURL.indexOf(':') );

      // set the port
      if(BaseURL.lastIndexOf(':')  != BaseURL.indexOf(':')){
         String port = BaseURL.substring(BaseURL.lastIndexOf(':') + 1);
         HttpClient.PORT = Integer.parseInt(port);
      }else{
        // default to http port 80
        HttpClient.PORT = 80;
      }

      String host = BaseURL.substring(BaseURL.lastIndexOf("//") + 2);
      if(host.indexOf(':') > 0){
        host = host.substring(0, host.indexOf(':'));
      }

      HttpClient.HOST = host;

    }

    public Object loadJSONLookup(String service, Class<?> type) throws Exception{

        parseHostAndPort();
        HttpClient client = new HttpClient();
        client.createContext();
        JSONObject array = null;
        JSONArray innerArray = null;
        
        ResponseEntity response;
        if(sendRnRToHub && type == ProcessingPeriod.class){
        	response = client.SendJSON("{}", hubURL + "/rest-api/lookup/" + service, HttpClient.GET, UserName , Authorization );
        	innerArray = new JSONArray(response.getResponse());
        } else {
        	response = client.SendJSON("{}", BaseURL + "/rest-api/lookup/" + service, HttpClient.GET, UserName , Authorization );
        	array = new JSONObject(response.getResponse());
        	innerArray = array.getJSONArray(service);
        }
        try{
            
            
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
            	   public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            		      return new Date(json.getAsJsonPrimitive().getAsLong()); 
            		   } 
            		}).create();

            List<Object> list = new ArrayList<>() ;
            for(int i = 0; i < innerArray.length(); i++){

                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(),type)) ;
            }
            return list;
        }

        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            throw new Exception ("Authentication Error");
        }
    }
    
    public Object loadJSONLookup(String service, Class<?> type , Map<String,String> parameters) throws Exception{

        parseHostAndPort();
        HttpClient client = new HttpClient();
        client.createContext();
        

        ResponseEntity response = client.SendJSON("{}", BaseURL + "/rest-api/lookup/" + service + (buildParametersString(parameters)), HttpClient.GET, UserName , Authorization );
        System.out.println(response.getResponse());
        try{
            JSONObject array = new JSONObject(response.getResponse());
            JSONArray innerArray = array.getJSONArray(service);
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
            	   public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            		      return new Date(json.getAsJsonPrimitive().getAsLong()); 
            		   } 
            		}).create();

            List<Object> list = new ArrayList<>() ;
            for(int i = 0; i < innerArray.length(); i++){

                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(),type)) ;
            }
            return list;
        }

        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            throw new Exception ("Authentication Error");
        }
    }
    
    @SuppressWarnings("rawtypes")
	public static String buildParametersString(Map<String, String> parameters){
    	String parametersString = "";

            Iterator it = parameters.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
//                System.out.println(pair.getKey() + " = " + pair.getValue());
                parametersString = parametersString.concat("?".concat(pair.getKey().toString().concat("=").concat(pair.getValue().toString())));
                it.remove(); // avoids a ConcurrentModificationException
            }
    	return  parametersString;
    }


    public Object uploadJSON(String service, String result , String json , Class<?> type , String programCode) throws JSONException , Exception{

    	parseHostAndPort();
        HttpClient client = new HttpClient();
        client.createContext();
        
        ResponseEntity response = null;

        if(sendRnRToHub && programCode.equals(ARV)){
        	response = client.SendJSON(json, hubURL + "/rest-api/" + service, HttpClient.POST, UserName , Authorization );
        } else {
        	response = client.SendJSON(json, BaseURL + "/rest-api/" + service, HttpClient.POST, UserName , Authorization );
        }
        JSONObject	array = new JSONObject(response.getResponse());


        String errorObject = null;
        try{
            errorObject = array.getString("error");
        }catch(Exception ex){

        }
        if(errorObject != null){
            throw new Exception(errorObject);
        }
        try{
            /*JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);*/
        	
        }
        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            // JSON was not being parsed,
            // this indicates that the authentication has failed.
            throw new Exception("Authentication Error");
        }
        return true;

    }

}

class TimestampDeserializer implements JsonDeserializer<Timestamp>
{
  @Override
  public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException
  {
    long time = Long.parseLong(json.getAsString());
    return new Timestamp(time);
  }
}

class DateDeserializer implements JsonDeserializer<Date> {

	@Override
	public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		String date = element.getAsString();
		
		SimpleDateFormat formatter = new SimpleDateFormat();
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			System.err.println("Failed to parse Date due to:");
			return null;
		}
	}
}
