package org.jsi.elmis.interfacing.openlmis;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jsi.elmis.model.DosageFrequency;
import org.jsi.elmis.model.DosageUnit;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.FacilityApprovedProduct;
import org.jsi.elmis.model.FacilityType;
import org.jsi.elmis.model.GeographicLevel;
import org.jsi.elmis.model.GeographicZone;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.ProcessingSchedule;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.ProductCategory;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.model.Regimen;
import org.jsi.elmis.model.RegimenCombinationProduct;
import org.jsi.elmis.model.RegimenLine;
import org.jsi.elmis.model.RegimenProductCombination;
import org.jsi.elmis.model.RegimenProductDosage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LookupFactory extends BaseFactory {

/*    public static ArrayList<DosageUnit> getDosageUnits() throws Exception{
        return (ArrayList<DosageUnit>) loadJSONLookup("dosage-units", DosageUnit.class);
    }

*/
	
	@Value("${paginate.sync.data}")
	private Boolean paginateSyncData;

    public ArrayList<Facility> getFacilities(){
    	ArrayList<Facility> facilities = null;
		try {
			if(!paginateSyncData){
				Map<String , String> parameters = new HashMap<String, String>();
				parameters.put("paging","false");
				facilities =(ArrayList<Facility>) loadJSONLookup("facilities",  Facility.class, parameters);
			} else {
				facilities =(ArrayList<Facility>) loadJSONLookup("facilities",  Facility.class);
			}
		} catch (Exception e) {
			e.printStackTrace(); // To change body of catch statement use File |
									// Settings | File Templates.
		}
        return facilities;
    }
    
    public ArrayList<GeographicLevel> getGeographicLevels(){
    	ArrayList<GeographicLevel> geographicLevels = null;
    	try{
    		geographicLevels =(ArrayList<GeographicLevel>) loadJSONLookup("geographic-levels", GeographicLevel.class); 
    	}catch(Exception ex){
    		
    	}
        return geographicLevels;
        
    }
    
    public ArrayList<GeographicZone> getGeographicZones(){
    	ArrayList<GeographicZone> geographicZones = null;
    	try{
    		geographicZones = (ArrayList<GeographicZone>) loadJSONLookup("geographic-zones", GeographicZone.class);
    	}catch(Exception ex){
    		
    	}
        return geographicZones;
    }

    public ArrayList<FacilityType> getFacilityTypes(){
    	ArrayList<FacilityType> facilityTypes = null;
    	try{
    		facilityTypes = (ArrayList<FacilityType>) loadJSONLookup("facility-types", FacilityType.class); 
    	}catch(Exception ex){
    		
    	}
        return facilityTypes;
    }


    public ArrayList<Product> getProducts(){
    	ArrayList<Product> products = null;
    	

		try {
			if(!paginateSyncData){
				Map<String , String> parameters = new HashMap<String, String>();
				parameters.put("paging","false");
				products = (ArrayList<Product>) loadJSONLookup("products", Product.class , parameters);
			} else {
				products = (ArrayList<Product>) loadJSONLookup("products", Product.class );
			}
		} catch (Exception e) {
			e.printStackTrace(); // To change body of catch statement use File |
									// Settings | File Templates.
		}
        return products;
    }
    
    public ArrayList<ProcessingPeriod> getProcessingPeriods() {
    	ArrayList<ProcessingPeriod> periods = null;
    	try{
    		periods = (ArrayList<ProcessingPeriod>) loadJSONLookup("processing-periods", ProcessingPeriod.class);
    	}catch(Exception ex){
    		
    	}
        return periods;
    }
    
    public ArrayList<ProcessingSchedule> getProcessingSchedules(){
    	ArrayList<ProcessingSchedule> processingSchedules = null;
    	try{
    		processingSchedules = (ArrayList<ProcessingSchedule>) loadJSONLookup("processing-schedules", ProcessingSchedule.class);;
    	}catch(Exception ex){
    		
    	}
        return processingSchedules;
    }
    
    public ArrayList<Program> getPrograms() {
    	ArrayList<Program> programs = null;
    	try{
    		programs = (ArrayList<Program>) loadJSONLookup("programs", Program.class); 
    	}catch(Exception ex){
    		
    	}
        return programs;
    }

    public ArrayList<ProductCategory> getProductCategories(){
    	ArrayList<ProductCategory> productCategories = null;
    	try{
    		productCategories = (ArrayList<ProductCategory>) loadJSONLookup("product-categories", ProductCategory.class);
    	}catch(Exception ex){
    		
    	}
        return productCategories;
    }
    
    public ArrayList<ProgramProduct> getProgramProducts(){
    	ArrayList<ProgramProduct> programProducts = null;
    	try{
    		programProducts = (ArrayList<ProgramProduct>) loadJSONLookup("program-products", ProgramProduct.class);
    		for (ProgramProduct programProduct : programProducts) {
				programProduct.setProgramid(programProduct.getProgram().getId());
				programProduct.setProductid(programProduct.getProduct().getId());
			}
    	}catch(Exception ex){
    		
    	}
        return programProducts;
    }
    
    public ArrayList<FacilityApprovedProduct> getFacilityApprovedProducts(){
    	ArrayList<FacilityApprovedProduct> approvedProducts = null;
    	try{
    		approvedProducts = (ArrayList<FacilityApprovedProduct>) loadJSONLookup("facility-approved-products", FacilityApprovedProduct.class); 
    		
    		for (FacilityApprovedProduct fap : approvedProducts) {
				fap.setFacilitytypeid(fap.getFacilityType().getId());
				fap.setProgramproductid(fap.getProgramProduct().getId());
			}
    		
    	}catch(Exception ex){
    		
    	}
        return approvedProducts;
    }
    


    public ArrayList<LossAdjustmentType> getLossesAndAdjustmentTypes(){
        ArrayList<LossAdjustmentType> lossesAndAdjustmentsTypes= null;
        try{
                   lossesAndAdjustmentsTypes = (ArrayList<LossAdjustmentType>) loadJSONLookup("losses-adjustments-types", LossAdjustmentType.class);
        } catch (Exception ex){

        }
        return   lossesAndAdjustmentsTypes;

    }

	public ArrayList<RegimenLine> getRegimenLines() {
		ArrayList<RegimenLine> regimenLines= null;
        try{
        	regimenLines = (ArrayList<RegimenLine>) loadJSONLookup("regimen-categories", RegimenLine.class);
        } catch (Exception ex){

        }
        return   regimenLines;
	}
	
	public ArrayList<Regimen> getRegimens() {
		ArrayList<Regimen> regimens= null;
        try{
        	regimens = (ArrayList<Regimen>) loadJSONLookup("regimens", Regimen.class);
        } catch (Exception ex){

        }
        return regimens;
	}
	
	public ArrayList<RegimenProductCombination> getRegimenProductCombinations() {
		ArrayList<RegimenProductCombination> regimenProductCombinations= null;
        try{
        	regimenProductCombinations = (ArrayList<RegimenProductCombination>) loadJSONLookup("regimen-product-combinations", RegimenProductCombination.class);
        } catch (Exception ex){

        }
        return regimenProductCombinations;
	}
	
	public ArrayList<RegimenCombinationProduct> getRegimenCombinationProducts() {
		ArrayList<RegimenCombinationProduct> regimenCombinationProducts= null;
        try{
        	regimenCombinationProducts = (ArrayList<RegimenCombinationProduct>) loadJSONLookup("regimen-combination-constituents", RegimenCombinationProduct.class);
        } catch (Exception ex){

        }
        return regimenCombinationProducts;
	}
	

	public ArrayList<RegimenProductDosage> getRegimenProductDosages() {
		ArrayList<RegimenProductDosage> regimenProductDosages= null;
        try{
        	regimenProductDosages = (ArrayList<RegimenProductDosage>) loadJSONLookup("regimen-constituent-dosages", RegimenProductDosage.class);
        } catch (Exception ex){

        }
        return regimenProductDosages;
	}
	
	public ArrayList<DosageUnit> getDosageUnits() {
		ArrayList<DosageUnit> dosageUnits= null;
        try{
        	dosageUnits = (ArrayList<DosageUnit>) loadJSONLookup("dosage-units", DosageUnit.class);
        } catch (Exception ex){

        }
        return dosageUnits;
	}
	
	public ArrayList<DosageFrequency> getDosageFrequencies() {
		ArrayList<DosageFrequency> dosageFrequencies= null;
        try{
        	dosageFrequencies = (ArrayList<DosageFrequency>) loadJSONLookup("dosage-frequencies", DosageFrequency.class);
        } catch (Exception ex){

        }
        return dosageFrequencies;
	}


}
