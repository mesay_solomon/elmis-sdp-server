-- Table: equipment_types

-- DROP TABLE equipment_types;

CREATE TABLE equipment_types
(
  id serial NOT NULL,
  code character varying(20) NOT NULL,
  name character varying(200),
  displayorder integer NOT NULL DEFAULT 0,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT equipment_types_pkey PRIMARY KEY (id),
  CONSTRAINT equipment_types_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE equipment_types
  OWNER TO postgres;

-- Index: unique_equipment_type_code_index

-- DROP INDEX unique_equipment_type_code_index;

CREATE UNIQUE INDEX unique_equipment_type_code_index
  ON equipment_types
  USING btree
  (code COLLATE pg_catalog."default");

