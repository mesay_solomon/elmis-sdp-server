ALTER TABLE facility_clients DROP CONSTRAINT fk_client_regimen_id;
ALTER TABLE facility_clients
  ADD CONSTRAINT fk_client_regimen_id FOREIGN KEY (regimen_id)
  REFERENCES regimens (id)
  ON UPDATE CASCADE
  ON DELETE NO ACTION;