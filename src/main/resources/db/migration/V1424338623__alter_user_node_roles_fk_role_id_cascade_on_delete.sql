ALTER TABLE user_node_roles
  DROP CONSTRAINT fk_role_id;

ALTER TABLE user_node_roles
  ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id)
  REFERENCES roles (id)
  ON UPDATE NO ACTION
  ON DELETE CASCADE;
