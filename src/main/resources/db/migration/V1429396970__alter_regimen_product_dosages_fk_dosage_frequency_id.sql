ALTER TABLE regimen_product_dosages DROP CONSTRAINT fk_dosage_frequency_id;
ALTER TABLE regimen_product_dosages
  ADD CONSTRAINT fk_dosage_frequency_id FOREIGN KEY (dosage_frequency_id)
  REFERENCES dosage_frequencies (id)
  ON UPDATE CASCADE
  ON DELETE NO ACTION;
