ALTER TABLE user_node_roles
  DROP CONSTRAINT fk_node_id;

ALTER TABLE user_node_roles
  ADD CONSTRAINT fk_node_id FOREIGN KEY (node_id)
  REFERENCES nodes (id)
  ON UPDATE NO ACTION
  ON DELETE CASCADE;