CREATE TABLE public.lab_tests
(
   id serial, 
   equipment_id integer, 
   reagent_product_id integer,
   period_id integer,  
   number_of_tests integer, 
   remark character varying(200), 
   CONSTRAINT pk_lab_test_id PRIMARY KEY (id), 
   CONSTRAINT fk_period_id FOREIGN KEY (period_id) REFERENCES processing_periods (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_equipment_id FOREIGN KEY (equipment_id) REFERENCES equipments (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_reagent_id FOREIGN KEY (reagent_product_id) REFERENCES products (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
