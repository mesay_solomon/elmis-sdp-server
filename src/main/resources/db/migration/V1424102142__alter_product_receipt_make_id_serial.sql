CREATE SEQUENCE product_receipt_sequence;
ALTER TABLE product_receipt
  ALTER COLUMN id SET DEFAULT nextval('product_receipt_sequence');
  
ALTER TABLE product_receipt
  ALTER COLUMN id SET NOT NULL;

