ALTER TABLE product_period_statuses
  DROP CONSTRAINT fk_period_id;
ALTER TABLE product_period_statuses
  ADD CONSTRAINT fk_period_id FOREIGN KEY (period_id) REFERENCES processing_periods (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE equipment_status
  DROP CONSTRAINT fk_period_id;
ALTER TABLE equipment_status
  ADD CONSTRAINT fk_period_id FOREIGN KEY (period_id) REFERENCES processing_periods (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE lab_tests
  DROP CONSTRAINT fk_period_id;
ALTER TABLE lab_tests
  ADD CONSTRAINT fk_period_id FOREIGN KEY (period_id) REFERENCES processing_periods (id) ON UPDATE CASCADE ON DELETE NO ACTION;