ALTER TABLE transactions
   ADD COLUMN action_id integer;
   
ALTER TABLE transactions
   ADD CONSTRAINT fk_action_id FOREIGN KEY (action_id) REFERENCES actions (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
   
   

ALTER TABLE physical_counts
   ADD COLUMN action_id integer;
   
ALTER TABLE physical_counts
   ADD CONSTRAINT fk_action_id FOREIGN KEY (action_id) REFERENCES actions (id) ON UPDATE NO ACTION ON DELETE NO ACTION;