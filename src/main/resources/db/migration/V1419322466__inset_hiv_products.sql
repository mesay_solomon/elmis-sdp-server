INSERT INTO hiv_test_products (product_id, test_type)
SELECT id, 'SCREENING' FROM products WHERE code = 'HTK0001'
AND NOT EXISTS(SELECT id 
                 FROM   hiv_test_products 
                 WHERE  product_id IN (
                   SELECT id 
                        FROM   PRODUCTS 
                        WHERE  code = 'HTK0001'));

INSERT INTO hiv_test_products (product_id, test_type)
SELECT id, 'CONFIRMATORY' FROM products WHERE code = 'HTK0002'
AND NOT EXISTS(SELECT id 
     FROM   hiv_test_products 
     WHERE  product_id IN (
       SELECT id 
      FROM   PRODUCTS 
      WHERE  code = 'HTK0002'));
   