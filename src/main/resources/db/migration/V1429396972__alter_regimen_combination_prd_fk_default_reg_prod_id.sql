ALTER TABLE regimen_combination_products
  DROP CONSTRAINT fk_defalut_regimen_product_id;
ALTER TABLE regimen_combination_products
  ADD CONSTRAINT fk_defalut_regimen_product_id FOREIGN KEY (default_dosage_id)
  REFERENCES regimen_product_dosages (id)
  ON UPDATE CASCADE
  ON DELETE NO ACTION;