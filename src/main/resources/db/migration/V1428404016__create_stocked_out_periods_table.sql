CREATE TABLE public.product_period_stocked_out_periods
(
   id serial, 
   product_period_id integer, 
   start_date timestamp without time zone, 
   end_date timestamp with time zone, 
   number_of_days integer, 
   CONSTRAINT product_period_stock_out_days_pkey PRIMARY KEY (id), 
   CONSTRAINT product_period_status_fkey FOREIGN KEY (product_period_id) REFERENCES product_period_statuses (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
