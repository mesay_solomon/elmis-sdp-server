CREATE TABLE public.stock_rquistion_items
(
   id serial, 
   product_id integer, 
   quantity_requested numeric, 
   CONSTRAINT pk_stock_requisition_item_id PRIMARY KEY (id), 
   CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;