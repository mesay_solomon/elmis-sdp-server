ALTER TABLE stock_rquistion_items
   ADD COLUMN stock_requisition_id integer;

ALTER TABLE stock_rquistion_items
  ADD CONSTRAINT fk_stock_requisition_id FOREIGN KEY (stock_requisition_id) REFERENCES stock_requisitions (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
