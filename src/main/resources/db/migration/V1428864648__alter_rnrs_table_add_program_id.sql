ALTER TABLE rnrs
  ADD COLUMN program_id integer;
ALTER TABLE rnrs
  ADD CONSTRAINT program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
