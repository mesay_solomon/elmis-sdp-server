ALTER TABLE users
DROP COLUMN deleted;

ALTER TABLE users
ADD COLUMN deleted boolean DEFAULT false;