DROP SEQUENCE IF EXISTS elmis_regimens_id_seq;
DROP SEQUENCE IF EXISTS elmis_regimen_products_id_seq;
DROP SEQUENCE IF EXISTS elmis_regimen_product_dosages_id_seq;
DROP SEQUENCE IF EXISTS elmis_regimen_categories_id_seq;
DROP SEQUENCE IF EXISTS regimen_product_combinations_id_seq;

