CREATE TABLE public.equipment_status
(
   id serial, 
   period_id integer, 
   functioning boolean, 
   CONSTRAINT pk_equipment_status_id PRIMARY KEY (id), 
   CONSTRAINT fk_period_id FOREIGN KEY (period_id) REFERENCES processing_periods (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;