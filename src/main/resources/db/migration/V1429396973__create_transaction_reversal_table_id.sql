CREATE TABLE reversal_transactions
(
   id serial, 
   reversed_transaction_id integer, 
   reversal_transaction_id integer, 
   CONSTRAINT reversal_transaction_pkey PRIMARY KEY (id), 
   CONSTRAINT reversed_transaction_fkey FOREIGN KEY (reversed_transaction_id) REFERENCES transactions (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT reversal_transaction_fkey FOREIGN KEY (reversal_transaction_id) REFERENCES transactions (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
