CREATE TABLE physical_count_adjustments
(
  id serial NOT NULL,
  physical_count_id integer,
  adjustment_id integer,
  CONSTRAINT pk_physical_count_adjusmtent_id PRIMARY KEY (id),
  CONSTRAINT fk_physical_count_id FOREIGN KEY (physical_count_id)
      REFERENCES physical_counts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_adjusment_id FOREIGN KEY (adjustment_id)
      REFERENCES adjustments (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE physical_count_adjustments
  OWNER TO postgres;