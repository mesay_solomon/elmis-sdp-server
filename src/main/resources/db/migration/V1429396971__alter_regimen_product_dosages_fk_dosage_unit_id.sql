
ALTER TABLE regimen_product_dosages DROP CONSTRAINT fk_dosage_unit_id;
ALTER TABLE regimen_product_dosages
  ADD CONSTRAINT fk_dosage_unit_id FOREIGN KEY (dosage_unit_id)
  REFERENCES dosage_units (id)
  ON UPDATE CASCADE
  ON DELETE NO ACTION;