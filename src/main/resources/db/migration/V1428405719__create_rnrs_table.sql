CREATE TABLE rnrs
(
   id serial, 
   period_id integer, 
   status character varying(50), 
   remark text, 
   CONSTRAINT rnrs_pkey PRIMARY KEY (id), 
   CONSTRAINT processing_period_fkey FOREIGN KEY (period_id) REFERENCES processing_periods (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
