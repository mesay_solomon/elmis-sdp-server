DROP TABLE IF EXISTS productsource CASCADE;
CREATE TABLE productsource
(
  id serial NOT NULL,
  sourcename character varying(100),
  createddate date,
  createdby character varying(100),
  facilityid integer not null,
CONSTRAINT productsource_pkey PRIMARY KEY (id),
CONSTRAINT productsource_facilityid_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
      CONSTRAINT productsource_sourcename_key UNIQUE (sourcename)
 
);
