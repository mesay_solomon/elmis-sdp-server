ALTER TABLE node_programs
DROp CONSTRAINT fk_node_kd;

ALTER TABLE node_programs
  ADD CONSTRAINT fk_node_kd FOREIGN KEY (node_id)
  REFERENCES nodes (id)
  ON UPDATE NO ACTION
  ON DELETE CASCADE;
