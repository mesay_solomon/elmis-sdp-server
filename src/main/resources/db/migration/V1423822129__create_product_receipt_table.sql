CREATE TABLE public.product_receipt
(
   id integer, 
   transaction_id integer, 
   dispatch_number character varying, 
   source_id integer, 
   CONSTRAINT pk_product_receipt_id PRIMARY KEY (id) , 
   CONSTRAINT fk_prouduct_source_id FOREIGN KEY (source_id)
      REFERENCES productsource(id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;

