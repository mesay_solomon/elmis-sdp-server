CREATE TABLE public.stock_requisition_transactions
(
   id serial, 
   stock_requisition_id integer, 
   transaction_id integer, 
   CONSTRAINT pk_stock_requisition_txn_id PRIMARY KEY (id), 
   CONSTRAINT fk_stock_requisition_id FOREIGN KEY (stock_requisition_id) REFERENCES stock_requisitions (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_transaction_id FOREIGN KEY (transaction_id) REFERENCES transactions (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
