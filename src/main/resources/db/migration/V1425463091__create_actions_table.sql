CREATE TABLE public.actions
(
   id serial, 
   type character varying(50), 
   "timestamp" timestamp without time zone, 
   ordinal_number integer, 
   user_id integer, 
   CONSTRAINT pk_actions PRIMARY KEY (id), 
   CONSTRAINT unique_ordinal_number UNIQUE (ordinal_number), 
   CONSTRAINT fk_user_id FOREIGN KEY (id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;



   