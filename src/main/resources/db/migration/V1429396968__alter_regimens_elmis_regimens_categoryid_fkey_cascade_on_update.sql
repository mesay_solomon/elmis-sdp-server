ALTER TABLE regimens DROP CONSTRAINT elmis_regimens_categoryid_fkey;
ALTER TABLE regimens ADD CONSTRAINT elmis_regimens_categoryid_fkey FOREIGN KEY (line_id) REFERENCES regimen_lines (id) ON UPDATE CASCADE ON DELETE NO ACTION;
