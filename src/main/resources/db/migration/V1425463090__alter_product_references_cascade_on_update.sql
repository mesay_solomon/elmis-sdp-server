ALTER TABLE equipment_reagents
  DROP CONSTRAINT fk_reagent_product_id;
ALTER TABLE equipment_reagents
  ADD CONSTRAINT fk_reagent_product_id FOREIGN KEY (reagent_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE hiv_test_products
  DROP CONSTRAINT fk_product_id;
ALTER TABLE hiv_test_products
  ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE lab_tests
  DROP CONSTRAINT fk_reagent_id;
ALTER TABLE lab_tests
  ADD CONSTRAINT fk_reagent_id FOREIGN KEY (reagent_product_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE node_products
  DROP CONSTRAINT fk_product_id;
ALTER TABLE node_products
  ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE product_period_statuses
  DROP CONSTRAINT fk_product_id;
ALTER TABLE product_period_statuses
  ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE stock_rquistion_items
  DROP CONSTRAINT fk_product_id;
ALTER TABLE stock_rquistion_items
  ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  
ALTER TABLE transaction_products
  DROP CONSTRAINT fk_product_id;
ALTER TABLE transaction_products
  ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (id) ON UPDATE CASCADE ON DELETE NO ACTION;
  