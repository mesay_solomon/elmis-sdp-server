INSERT INTO rights
(
  name,
  description,
  righttype,
  createddate
)
SELECT * FROM (SELECT 'LAB TEST NUMBERS','Labratory Test Numbers', '',  NOW()::TIMESTAMP) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM rights WHERE name = 'LAB TEST NUMBERS'
) LIMIT 1;

INSERT INTO rights
(
  name,
  description,
  righttype,
  createddate
)
SELECT * FROM (SELECT 'LAB EQUIPMENT STATUS', 'Labratory Equipment Status', '', NOW()::TIMESTAMP) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM rights WHERE name = 'LAB EQUIPMENT STATUS'
) LIMIT 1;