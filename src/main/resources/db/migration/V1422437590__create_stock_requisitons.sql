CREATE TABLE public.stock_requisitions
(
   id serial, 
   requester_id integer, 
   supplier_id integer, 
   "time" timestamp without time zone, 
   status character varying(50), 
   user_id integer, 
   CONSTRAINT pk_stock_requisition_id PRIMARY KEY (id), 
   CONSTRAINT fk_requester_node_id FOREIGN KEY (requester_id) REFERENCES nodes (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_supplier_node_id FOREIGN KEY (supplier_id) REFERENCES nodes (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
