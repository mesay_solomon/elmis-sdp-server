CREATE TABLE public.equipment_reagents
(
   id serial, 
   equipment_id integer, 
   reagent_id integer, 
   CONSTRAINT pk_equipment_reagent_id PRIMARY KEY (id), 
   CONSTRAINT fk_equipment_id FOREIGN KEY (equipment_id) REFERENCES equipments (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_reagent_product_id FOREIGN KEY (reagent_id) REFERENCES products (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;