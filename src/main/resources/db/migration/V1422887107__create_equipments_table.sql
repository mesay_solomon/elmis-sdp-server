-- Table: equipments

-- DROP TABLE equipments;

CREATE TABLE equipments
(
  id serial NOT NULL,
  code character varying(200) NOT NULL,
  name character varying(200) NOT NULL,
  equipmenttypeid integer NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT equipments_pkey PRIMARY KEY (id),
  CONSTRAINT equipments_equipmenttypeid_fkey FOREIGN KEY (equipmenttypeid)
      REFERENCES equipment_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT equipments_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE equipments
  OWNER TO postgres;

-- Index: unique_equipment_code

-- DROP INDEX unique_equipment_code;

CREATE UNIQUE INDEX unique_equipment_code
  ON equipments
  USING btree
  (code COLLATE pg_catalog."default");
