ALTER TABLE role_rights
  DROP CONSTRAINT role_rights_roleid_fkey;

ALTER TABLE role_rights
  ADD CONSTRAINT role_rights_roleid_fkey FOREIGN KEY (roleid)
  REFERENCES roles (id)
  ON UPDATE NO ACTION
  ON DELETE CASCADE;