ALTER TABLE equipment_status
   ADD COLUMN equipment_id integer;
   
ALTER TABLE equipment_status
  ADD CONSTRAINT fk_equipment_id FOREIGN KEY (equipment_id)
      REFERENCES equipments(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
   